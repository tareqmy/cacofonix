export class CallCenterLive {
    queueCount: number;
    queues: any[];

    /**
     * Constructor
     *
     * @param callCenterLive
     */
    constructor(callCenterLive) {
        this.queueCount = callCenterLive.queueCount;
        this.queues = callCenterLive.queues;
    }
}
