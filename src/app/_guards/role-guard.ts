import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import decode from 'jwt-decode';

import { AuthenticationService } from 'app/services/authentication.service';

@Injectable({ providedIn: 'root' })
export class RoleGuard implements CanActivate {

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {Router} _router
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        // this will be passed from the route config
        // on the data property
        const expectedRoles = route.data.expectedRoles;

        const jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken) {
            // decode the token to get its payload
            const tokenPayload = decode(jwtoken.token);
            if (!expectedRoles.includes(tokenPayload.auth)) {
                this._router.navigate(['/pages/auth/login']);
                return false;
            }
            // authorised so return true
            return true;
        }

        return false;
    }
}
