import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { UsersService } from 'app/services/users.service';
import { AgentReportsService } from 'app/services/agentreports.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'agentreports-list',
    templateUrl: './agentreports.component.html',
    styleUrls: ['./agentreports.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class AgentReportsComponent implements OnInit, OnDestroy {

    dataSource: AgentReportsDataSource | null;
    dataSourceCall: AgentReportsCallDataSource | null;
    displayedColumns = ['label1', 'value1', 'label2', 'value2'];

    selectedAgent: any;
    agents: any;

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AgentReportsService} _agentReportsService
     * @param {UsersService} _usersService
     */
    constructor(
        private _agentReportsService: AgentReportsService,
        private _usersService: UsersService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new AgentReportsDataSource(this._agentReportsService);
        this.dataSourceCall = new AgentReportsCallDataSource(this._agentReportsService);
        this.getAgents();
    }

    getAgents(): void {
        this._usersService.getUsers().then((agents) => {
            this.agents = agents;
            if (this.agents && this.agents.length > 0) {
                this.selectedAgent = agents[0].id;
                this._agentReportsService.getAgentReports(this.selectedAgent, this.startDate, this.endDate);
            }
        });
    }

    isAgentSelected(): boolean {
        return !_.isNil(this.selectedAgent);
    }

    refresh(): void {
        this._agentReportsService.getAgentReports(this.selectedAgent, this.startDate, this.endDate);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.agents = [];
        this.selectedAgent = null;
        this._agentReportsService.cleanup();
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._agentReportsService.getAgentReports(this.selectedAgent, this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._agentReportsService.getAgentReports(this.selectedAgent, this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }
}
export class AgentReportsDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {AgentReportsService} _agentReportsService
     */
    constructor(
        private _agentReportsService: AgentReportsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._agentReportsService.onAgentReportsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class AgentReportsCallDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {AgentReportsService} _agentReportsService
     */
    constructor(
        private _agentReportsService: AgentReportsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._agentReportsService.onAgentReportsCallChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
