export class AgentLog {
    id: number;
    agentEvent: string;
    eventDateTime: string;
    companyId: string;
    userId: number;
    userName: string;
    queueId: number;
    queueName: string;
    phoneLineId: number;
    sipUserName: string;

    /**
     * Constructor
     *
     * @param agentLog
     */
    constructor(agentLog) {
        this.id = agentLog.id || '';
        this.agentEvent = agentLog.agentEvent;
        this.eventDateTime = agentLog.eventDateTime;
        this.companyId = agentLog.companyId;
        this.userId = agentLog.userId;
        this.userName = agentLog.userName;
        this.queueId = agentLog.queueId;
        this.queueName = agentLog.queueName;
        this.phoneLineId = agentLog.phoneLineId;
        this.sipUserName = agentLog.sipUserName;
    }
}
