export class QueueLive {
    id: number;
    queueId: string;
    queueName: string;
    baseExtension: string;
    companyId: string;
    max: number;
    weight: number;
    serviceLevel: number;
    strategy: string;
    holdTime: number;
    talkTime: number;
    totalWait: number;
    avgWait: number;
    maxWait: number;
    completed: number;
    abandoned: number;
    agentStatusMap: any;
    agentsCount: number;
    agents: any[];
    queuedChannelsCount: number;
    queuedChannels: any[];
    channelsCount: number;
    channels: any[];
    messageCount: number;
    messages: any[];

    /**
     * Constructor
     *
     * @param queueLive
     */
    constructor(queueLive) {
        this.id = queueLive.id;
        this.queueId = queueLive.queueId;
        this.queueName = queueLive.queueName;
        this.baseExtension = queueLive.baseExtension;
        this.companyId = queueLive.companyId;
        this.max = queueLive.max;
        this.weight = queueLive.weight;
        this.serviceLevel = queueLive.serviceLevel;
        this.strategy = queueLive.strategy;
        this.holdTime = queueLive.holdTime;
        this.talkTime = queueLive.talkTime;
        this.totalWait = queueLive.totalWait;
        this.avgWait = queueLive.avgWait;
        this.maxWait = queueLive.maxWait;
        this.completed = queueLive.completed;
        this.abandoned = queueLive.abandoned;
        this.agentStatusMap = queueLive.agentStatusMap;
        this.agentsCount = queueLive.agentsCount;
        this.agents = queueLive.agents;
        this.queuedChannelsCount = queueLive.queuedChannelsCount;
        this.queuedChannels = queueLive.queuedChannels;
        this.channelsCount = queueLive.channelsCount;
        this.channels = queueLive.channels;
        this.messageCount = queueLive.messageCount;
        this.messages = queueLive.messages;
    }
}
