import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { PhoneLine } from 'app/models/phoneline.model';
import { PhoneLineService } from 'app/services/phoneline.service';
import { CommonService } from 'app/services/common.service';
import { PhonesService } from 'app/services/phones.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'phoneline',
    templateUrl: './phoneline.component.html',
    styleUrls: ['./phoneline.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PhoneLineComponent implements OnInit, OnDestroy {
    phoneLine: PhoneLine;
    pageType: string;
    isSuperAdmin: boolean;
    phoneLineForm: FormGroup;

    phoneId: number;
    extensions: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {PhoneLineService} _phoneLineService
     * @param {CommonService} _commonService
     * @param {PhonesService} _phonesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _phoneLineService: PhoneLineService,
        private _commonService: CommonService,
        private _phonesService: PhonesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router,
        private _route: ActivatedRoute
    ) {
        // Set the default
        this.phoneLine = new PhoneLine({});

        this.phoneId = this._phoneLineService.routeParams.id;

        this.extensions = [];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update phoneLine on changes
        this._phoneLineService.onPhoneLineChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(phoneLine => {

                if (phoneLine) {
                    this.phoneLine = new PhoneLine(phoneLine);
                    this.pageType = 'edit';
                    this.phoneLineForm = this.editPhoneLineForm();
                }
                else {
                    this.pageType = 'new';
                    this.phoneLine = new PhoneLine({});
                    this.phoneLine.phoneId = this.phoneId;
                    this.phoneLineForm = this.createPhoneLineForm();
                }
            });

        this._phonesService.getExtensions(this.phoneId).then((extensions) => {
            this.extensions = extensions;
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.phoneLineForm.controls; }

    /**
     * Create phoneLine form
     *
     * @returns {FormGroup}
     */
    createPhoneLineForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.phoneLine.id],
            description: [this.phoneLine.description, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
            phoneId: [{ value: this.phoneLine.phoneId, disabled: true }],
            phoneName: [{ value: this.phoneLine.phoneName, disabled: true }],
            callerName: [this.phoneLine.callerName, [Validators.required]],
            extensionId: [this.phoneLine.extensionId, [Validators.required]],
            baseExtension: [this.phoneLine.baseExtension],
            sipUserName: [{ value: this.phoneLine.sipUserName, disabled: true }],
            sipPassword: [this.phoneLine.sipPassword, [Validators.required, Validators.pattern('[A-Za-z0-9(),-.@^_{|}~]+')]],
            callWaitingEnabled: [this.phoneLine.callWaitingEnabled]
        });
    }

    /**
     * Create phoneLine form
     *
     * @returns {FormGroup}
     */
    editPhoneLineForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.phoneLine.id],
            description: [this.phoneLine.description, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
            phoneId: [{ value: this.phoneLine.phoneId, disabled: true }],
            phoneName: [{ value: this.phoneLine.phoneName, disabled: true }],
            callerName: [this.phoneLine.callerName, [Validators.required]],
            extensionId: [{ value: this.phoneLine.extensionId, disabled: true }],
            baseExtension: [{ value: this.phoneLine.baseExtension, disabled: true }],
            sipUserName: [{ value: this.phoneLine.sipUserName, disabled: true }],
            sipPassword: [this.phoneLine.sipPassword, [Validators.required, Validators.pattern('[A-Za-z0-9(),-.@^_{|}~]+')]],
            callWaitingEnabled: [this.phoneLine.callWaitingEnabled]
        });
    }

    /**
     * Save phoneLine
     */
    savePhoneLine(): void {
        const data = this.phoneLineForm.getRawValue();

        this._phoneLineService.savePhoneLine(data)
            .then(() => {

                // Trigger the subscription with new data
                this._phoneLineService.onPhoneLineChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Line saved');
            });
    }

    /**
     * Add phoneLine
     */
    addPhoneLine(): void {
        const data = this.phoneLineForm.getRawValue();

        this._phoneLineService.addPhoneLine(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._phoneLineService.onPhoneLineChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Line added');

                // Change the location with new one
                this._router.navigate(['/settings/phones/' + this.phoneId + '/lines/' + this.phoneLine.id]);
            });
    }


    /**
     * Delete phoneLine
     */
    deletePhoneLine(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.phoneLineForm.getRawValue();

                this._phoneLineService.deletePhoneLine(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._phoneLineService.onPhoneLineChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Line deleted');

                        // Change the location to phoneLines table
                        this._router.navigate(['/settings/phones/' + this.phoneId + '/lines']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
