import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { PhoneNumberComponent } from './phonenumber/phonenumber.component';
import { PhoneNumberService } from 'app/services/phonenumber.service';
import { PhoneNumbersComponent } from './phonenumbers/phonenumbers.component';
import { PhoneNumbersService } from 'app/services/phonenumbers.service';

const routes = [
    {
        path: ':id',
        component: PhoneNumberComponent,
        resolve: {
            data: PhoneNumberService
        }
    },
    {
        path: '',
        component: PhoneNumbersComponent,
        resolve: {
            data: PhoneNumbersService
        }
    },
];

@NgModule({
    declarations: [
        PhoneNumbersComponent,
        PhoneNumberComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        PhoneNumbersService,
        PhoneNumberService
    ],
    entryComponents: [
    ]
})
export class PhoneNumbersModule { }
