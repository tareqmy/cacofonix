export class QueueLog {
    id: number;
    callid: string;
    queueName: string;
    agent: string;
    event: number;
    data1: string;
    data2: string;
    data3: string;
    data4: string;
    data5: string;
    time: string;

    /**
     * Constructor
     *
     * @param queueLog
     */
    constructor(queueLog) {
        this.id = queueLog.id || '';
        this.callid = queueLog.callid || '';
        this.queueName = queueLog.queueName || '';
        this.agent = queueLog.agent;
        this.event = queueLog.event;
        this.data1 = queueLog.data1;
        this.data2 = queueLog.data2;
        this.data3 = queueLog.data3;
        this.data4 = queueLog.data4;
        this.data5 = queueLog.data5;
        this.time = queueLog.time;
    }
}
