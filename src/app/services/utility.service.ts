import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'environments/environment';

import { ApplicationInfo } from 'app/models/applicationinfo.model';

@Injectable({
    providedIn: 'root'
})
export class UtilityService {

    zonesSubject: BehaviorSubject<any[]>;
    zones: Observable<any[]>;

    rolesSubject: BehaviorSubject<any[]>;
    roles: Observable<any[]>;

    applicationInfoSubject: BehaviorSubject<any>;
    applicationInfo: Observable<any>;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(private _httpClient: HttpClient) {
        // Set the defaults
        this.zonesSubject = new BehaviorSubject<any[]>(JSON.parse(localStorage.getItem('zonesmap')));
        this.zones = this.zonesSubject.asObservable();

        this.rolesSubject = new BehaviorSubject<any[]>(JSON.parse(localStorage.getItem('rolesmap')));
        this.roles = this.rolesSubject.asObservable();

        let app = JSON.parse(localStorage.getItem('applicationInfo'));
        if (app === null) {
            app = new ApplicationInfo();
        }
        this.applicationInfoSubject = new BehaviorSubject<any>(app);
        this.applicationInfo = this.applicationInfoSubject.asObservable();

        this.loadAll();
    }

    public loadAll(): void {
        this.getTimeZones();
        this.getAllRolesMap();
        this.getApplicationInfo();
    }

    public get zonesValue(): any[] {
        return this.zonesSubject.value;
    }

    public get rolesValue(): any[] {
        return this.rolesSubject.value;
    }

    public get applicationInfoValue(): ApplicationInfo {
        return this.applicationInfoSubject.value;
    }

    getTimeZones(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/utils/zonesmap`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    localStorage.setItem('zonesmap', JSON.stringify(response));
                    this.zonesSubject.next(response);
                    resolve(response);
                }, reject);
        });
    }

    getAllRolesMap(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/utils/rolesmap`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    localStorage.setItem('rolesmap', JSON.stringify(response));
                    this.rolesSubject.next(response);
                    resolve(response);
                }, reject);
        });
    }

    getApplicationInfo(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/application/info`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    localStorage.setItem('applicationInfo', JSON.stringify(response));
                    this.applicationInfoSubject.next(response);
                    resolve(response);
                }, reject);
        });
    }
}
