import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from 'app/services/authentication.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {Router} _router
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken) {
            // authorised so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this._router.navigate(['/pages/auth/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
