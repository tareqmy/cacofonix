import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { AgentLive } from 'app/models/agentlive.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class AgentLiveService {

    eventSource: EventSourcePolyfill;
    agentLive: AgentLive;
    onChannelLiveChanged: BehaviorSubject<any>;
    interval: any;
    extension: string;
    sipUserName: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onChannelLiveChanged = new BehaviorSubject({});
    }

    /**
     * Get agentLive
     *
     */
    getAgentLive() {
        this.createInterval();

        let jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken && jwtoken.token) {
            this.eventSource = new EventSourcePolyfill(`${environment.settings.backend}/api/sse/agents/live`,
                {
                    headers: {
                        Authorization: `Bearer ${jwtoken.token}`
                    }
                });

            // this.eventSource.onopen = (event: any) => console.log('open', event);

            this.eventSource.onmessage = (event: any) => {
                this.agentLive = new AgentLive(JSON.parse(event.data));
                this.onChannelLiveChanged.next(this.agentLive.channels);
            };

            // this.eventSource.onerror = (event: any) => console.log('error', event);
        }
    }

    createInterval() {
        this.deleteInterval();
        this.interval = setInterval(() => {
            //note: checked duration is increasing
            if (this.agentLive) {
                this.agentLive.channels.forEach(channel => {
                    channel.duration = channel.duration + 1;
                });
            }
        }, 1000);
    }

    deleteInterval() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    closeEvents(stopInterval: boolean) {
        this.agentLive = null;
        this.onChannelLiveChanged.next(null);
        if (this.eventSource) {
            this.eventSource.close();
        }
        if (stopInterval) {
            this.deleteInterval();
        }
    }

    /**
     * Get Agent Sip Username
     *
     * @returns {Promise<any>}
     */
    getAgentSipUserName(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/agents/sipusername`;
            this._httpClient.get(requestUrl, { responseType: 'text' })
                .subscribe((response: any) => {
                    if (response) {
                        this.sipUserName = response;
                        this.extension = response.substr(-2);
                    }
                    resolve(response);
                }, reject);
        });
    }
}
