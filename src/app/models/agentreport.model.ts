export class AgentReport {
    userId: number;
    userName: string;
    zoneId: string;
    from: string;
    to: string;
    period: string;
    duration: string;

    totalSessionDuration: number;
    totalSessionTime: string;
    totalJoinedDuration: number;
    totalJoinedTime: string;
    totalUnJoinedDuration: number;
    totalUnJoinedTime: string;
    totalPausedDuration: number;
    totalPausedTime: string;

    totalCallDuration: number;
    totalCallTime: string;
    totalQueueCallRingDuration: number;
    totalQueueCallRingTime: string;
    totalQueueCallTalkDuration: number;
    totalQueueCallTalkTime: string;
    totalIdleDuration: number;
    totalIdleTime: string;

    percentSession: number;
    totalCalls: number;
    totalQueueCalls: number;
    totalQueueCallsInbound: number;
    totalQueueCallsOutbound: number;
    totalQueueCallsAnswered: number;
    totalQueueCallsUnanswered: number;
    averageHandleDuration: number;

    sessions: number;
    joined: number;
    pauses: number;

    averageSessionDuration: number;
    averageSessionTime: string;
    averageJoinedDuration: number;
    averageJoinedTime: string;
    averagePausedDuration: number;
    averagePausedTime: string;

    percentJoined: number;
    percentPaused: number;
    pausesPerSession: number;

    totalACDCallsAnswered: number;
    totalACDCallsFailed: number;
    totalACDCallsEndByCaller: number;
    totalACDCallsEndByAgent: number;
    totalACDCallsTransferred: number;

    /**
     * Constructor
     *
     * @param agentReport
     */
    constructor(agentReport) {
        this.userId = agentReport.userId;
        this.userName = agentReport.userName;
        this.zoneId = agentReport.zoneId;
        this.from = agentReport.from;
        this.to = agentReport.to;
        this.period = agentReport.period;
        this.duration = agentReport.duration;

        this.totalSessionDuration = agentReport.totalSessionDuration;
        this.totalSessionTime = agentReport.totalSessionTime;
        this.totalJoinedDuration = agentReport.totalJoinedDuration;
        this.totalJoinedTime = agentReport.totalJoinedTime;
        this.totalUnJoinedDuration = agentReport.totalUnJoinedDuration;
        this.totalUnJoinedTime = agentReport.totalUnJoinedTime;
        this.totalPausedDuration = agentReport.totalPausedDuration;
        this.totalPausedTime = agentReport.totalPausedTime;

        this.totalCallDuration = agentReport.totalCallDuration;
        this.totalCallTime = agentReport.totalCallTime;
        this.totalQueueCallRingDuration = agentReport.totalQueueCallRingDuration;
        this.totalQueueCallRingTime = agentReport.totalQueueCallRingTime;
        this.totalQueueCallTalkDuration = agentReport.totalQueueCallTalkDuration;
        this.totalQueueCallTalkTime = agentReport.totalQueueCallTalkTime;
        this.totalIdleDuration = agentReport.totalIdleDuration;
        this.totalIdleTime = agentReport.totalIdleTime;

        this.percentSession = agentReport.percentSession;
        this.totalCalls = agentReport.totalCalls;
        this.totalQueueCalls = agentReport.totalQueueCalls;
        this.totalQueueCallsAnswered = agentReport.totalQueueCallsAnswered;
        this.totalQueueCallsUnanswered = agentReport.totalQueueCallsUnanswered;
        this.averageHandleDuration = agentReport.averageHandleDuration;

        this.sessions = agentReport.sessions;
        this.joined = agentReport.joined;
        this.pauses = agentReport.pauses;

        this.averageSessionDuration = agentReport.averageSessionDuration;
        this.averageSessionTime = agentReport.averageSessionTime;
        this.averageJoinedDuration = agentReport.averageJoinedDuration;
        this.averageJoinedTime = agentReport.averageJoinedTime;
        this.averagePausedDuration = agentReport.averagePausedDuration;
        this.averagePausedTime = agentReport.averagePausedTime;

        this.percentJoined = agentReport.percentJoined;
        this.percentPaused = agentReport.percentPaused;
        this.pausesPerSession = agentReport.pausesPerSession;

        this.totalACDCallsAnswered = agentReport.totalACDCallsAnswered;
        this.totalACDCallsFailed = agentReport.totalACDCallsFailed;
        this.totalACDCallsEndByCaller = agentReport.totalACDCallsEndByCaller;
        this.totalACDCallsEndByAgent = agentReport.totalACDCallsEndByAgent;
        this.totalACDCallsTransferred = agentReport.totalACDCallsTransferred;
    }
}
