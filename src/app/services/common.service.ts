import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { interval } from 'rxjs';
import { environment } from 'environments/environment';
import { MatSnackBar } from '@angular/material';

import { AccountService } from 'app/services/account.service';
import { UtilityService } from 'app/services/utility.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class CommonService implements Resolve<any> {

    scheduledTask: Subscription;

    constructor(
        private _accountService: AccountService,
        private _utilityService: UtilityService,
        private _authenticationService: AuthenticationService,
        private _matSnackBar: MatSnackBar
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.startScheduler()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    startScheduler(): void {
        //call get account every 5 minutes. it will ensure that the token is still valid.
        this.scheduledTask = interval(6000000)
            .subscribe((val) => {
                this._utilityService.loadAll();
                if (this._accountService.currentAccount) {
                    this._authenticationService.isAuthenticated();
                }
            });
    }

    public showSnackBar(message: string): void {
        // Show the success message
        this._matSnackBar.open(message, 'OK', {
            verticalPosition: 'top',
            duration: 5000
        });
    }

    public showErrorSnackBar(message: string): void {
        // Show the success message
        this._matSnackBar.open(message, 'OK', {
            verticalPosition: 'top',
            duration: 10000
        });
    }
}
