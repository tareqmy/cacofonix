export class VoiceMailMessage {
    dir: number;
    msgNum: string;
    context: string;
    macroContext: number;
    callerId: string;
    origTime: number;
    duration: string;
    flag: any;
    mailboxUser: any;
    mailboxContext: any;
    msgId: any;

    /**
     * Constructor
     *
     * @param voiceMailMessage
     */
    constructor(voiceMailMessage) {
        this.dir = voiceMailMessage.dir || '';
        this.msgNum = voiceMailMessage.msgNum || '';
        this.context = voiceMailMessage.context || '';
        this.macroContext = voiceMailMessage.macroContext;
        this.callerId = voiceMailMessage.callerId;
        this.origTime = voiceMailMessage.origTime;
        this.duration = voiceMailMessage.duration;
        this.flag = voiceMailMessage.flag;
        this.mailboxUser = voiceMailMessage.mailboxUser;
        this.mailboxContext = voiceMailMessage.mailboxContext;
        this.msgId = voiceMailMessage.msgId;
    }
}
