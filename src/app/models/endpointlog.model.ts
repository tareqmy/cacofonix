export class EndpointLog {
    id: number;
    eventDateTime: string;
    resource: string;
    companyId: string;
    phoneLineId: number;
    userId: number;
    userName: string;
    endpointState: string;
    deviceState: string;

    /**
     * Constructor
     *
     * @param endpointLog
     */
    constructor(endpointLog) {
        this.id = endpointLog.id || '';
        this.eventDateTime = endpointLog.eventDateTime;
        this.resource = endpointLog.resource;
        this.companyId = endpointLog.companyId;
        this.phoneLineId = endpointLog.phoneLineId;
        this.userId = endpointLog.userId;
        this.userName = endpointLog.userName;
        this.endpointState = endpointLog.endpointState;
        this.deviceState = endpointLog.deviceState;
    }
}
