export class ApplicationInfo {
    applicationVersion: string;
    activeProfile: string;
    developerEmail: string;
    developerName: string;
    developerWeb: string;
}
