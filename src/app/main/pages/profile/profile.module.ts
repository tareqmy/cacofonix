import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatSelectModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatRippleModule,
    MatToolbarModule, MatDialogModule, MatTooltipModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { ProfileComponent } from './profile.component';
import { AccountPasswordChangeFormDialogComponent } from './password-change-form/password-change-form.component';
import { EmailConfirmFormDialogComponent } from './email-confirm-form/email-confirm-form.component';
import { AccountService } from 'app/services/account.service';

const routes = [
    {
        path: 'profile',
        component: ProfileComponent,
        resolve: {
            data: AccountService
        }
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        AccountPasswordChangeFormDialogComponent,
        EmailConfirmFormDialogComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatRippleModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,

        FuseSharedModule
    ],
    exports: [
        ProfileComponent
    ],
    entryComponents: [
        AccountPasswordChangeFormDialogComponent,
        EmailConfirmFormDialogComponent
    ]
})
export class ProfileModule { }
