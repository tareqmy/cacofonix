import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AlertService } from 'app/services/alert.service';

@Component({
    selector: 'alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    message: any;

    /**
     * Constructor
     *
     * @param {AlertService} _alertService
     */
    constructor(
        private _alertService: AlertService
    ) {
    }

    ngOnInit() {
        this.subscription = this._alertService.getMessage().subscribe(message => {
            this.message = message;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
