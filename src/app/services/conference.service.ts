import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class ConferenceService implements Resolve<any>
{
    routeParams: any;
    conference: any;
    onConferenceChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onConferenceChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getConference()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get conference
     *
     * @returns {Promise<any>}
     */
    getConference(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onConferenceChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/conferences/${this.routeParams.id}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.conference = response;
                        this.onConferenceChanged.next(this.conference);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save conference
     *
     * @param conference
     * @returns {Promise<any>}
     */
    saveConference(conference): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/conferences`;
            this._httpClient.put(requestUrl, conference, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add conference
     *
     * @param conference
     * @returns {Promise<any>}
     */
    addConference(conference): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/conferences`;
            this._httpClient.post(requestUrl, conference, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete conference
     *
     * @param conference
     */
    deleteConference(conference): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/conferences/${conference.id}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
