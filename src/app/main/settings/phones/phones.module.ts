import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { PhoneComponent } from './phone/phone.component';
import { PhoneService } from 'app/services/phone.service';
import { PhonesComponent } from './phones/phones.component';
import { PhonesService } from 'app/services/phones.service';

const routes = [
    {
        path: ':id',
        component: PhoneComponent,
        resolve: {
            data: PhoneService
        }
    },
    {
        path: ':id/lines',
        loadChildren: './phone/phonelines/phonelines.module#PhoneLinesModule',
    },
    {
        path: '',
        component: PhonesComponent,
        resolve: {
            data: PhonesService
        }
    },
];

@NgModule({
    declarations: [
        PhonesComponent,
        PhoneComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        PhonesService,
        PhoneService
    ],
    entryComponents: [
    ]
})
export class PhonesModule { }
