import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { Company } from 'app/models/company.model';
import { CompanyService } from 'app/services/company.service';
import { CommonService } from 'app/services/common.service';
import { UtilityService } from 'app/services/utility.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CompanyComponent implements OnInit, OnDestroy {
    company: Company;
    pageType: string;
    isSuperAdmin: boolean;
    companyForm: FormGroup;

    timeZones: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CompanyService} _companyService
     * @param {CommonService} _commonService
     * @param {UtilityService} _utilityService
          * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _companyService: CompanyService,
        private _commonService: CommonService,
        private _utilityService: UtilityService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.company = new Company({});

        this.timeZones = {};

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update company on changes
        this._companyService.onCompanyChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(company => {

                if (company) {
                    this.company = new Company(company);
                    this.pageType = 'edit';
                    this.companyForm = this.editCompanyForm();
                }
                else {
                    this.pageType = 'new';
                    this.company = new Company({});
                    this.companyForm = this.createCompanyForm();
                }
            });

        this.timeZones = this._utilityService.zonesValue;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.companyForm.controls; }

    /**
     * Create company form
     *
     * @returns {FormGroup}
     */
    createCompanyForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.company.id],
            companyId: [this.company.companyId, [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z][a-zA-Z0-9]+')]],
            companyName: [this.company.companyName, [Validators.required]],
            description: [this.company.description],
            enabled: [true],
            zoneId: [this.company.zoneId, [Validators.required]],
            email: [this.company.email, [Validators.required, Validators.email]],
            phone: [this.company.phone],
            notes: [this.company.notes]
        });
    }

    /**
     * Create company form
     *
     * @returns {FormGroup}
     */
    editCompanyForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.company.id],
            companyId: [{ value: this.company.companyId, disabled: true }, [Validators.required]],
            companyName: [this.company.companyName, [Validators.required]],
            description: [this.company.description],
            enabled: [true],
            zoneId: [this.company.zoneId, [Validators.required]],
            email: [this.company.email, [Validators.required, Validators.email]],
            phone: [this.company.phone],
            notes: [this.company.notes]
        });
    }

    /**
     * Save company
     */
    saveCompany(): void {
        const data = this.companyForm.getRawValue();

        this._companyService.saveCompany(data)
            .then(() => {

                // Trigger the subscription with new data
                this._companyService.onCompanyChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Company saved');
            });
    }

    /**
     * Add company
     */
    addCompany(): void {
        const data = this.companyForm.getRawValue();

        this._companyService.addCompany(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._companyService.onCompanyChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Company added');

                // Change the location with new one
                this._router.navigate(['/settings/companies/' + this.company.id]);
            });
    }


    /**
     * Delete company
     */
    deleteCompany(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.companyForm.getRawValue();

                this._companyService.deleteCompany(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._companyService.onCompanyChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Company deleted');

                        // Change the location to companys table
                        this._router.navigate(['/settings/companies']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
