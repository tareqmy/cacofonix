import { ChartDataSets } from 'chart.js';
export class ChartReport {
    labels: string[];
    dataSets: ChartDataSets[];

    /**
     * Constructor
     *
     * @param chartReport
     */
    constructor(chartReport) {
        this.labels = chartReport.labels;
        this.dataSets = chartReport.dataSets;
    }
}
