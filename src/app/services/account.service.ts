import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { MessagesService } from 'app/services/messages.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class AccountService implements Resolve<any> {

    currentAccount: any;
    onAccountChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param {MessagesService} _messagesService
     * @param {Router} _router
     */
    constructor(
        private _httpClient: HttpClient,
        private _messagesService: MessagesService,
        private _router: Router
    ) {
        // Set the defaults
        this.onAccountChanged = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentAccount')));
        this.currentAccount = this.onAccountChanged.asObservable();
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAccount()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getAccount(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.currentAccount = response;

                    if (this.currentAccount && this.currentAccount.email) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentAccount', JSON.stringify(this.currentAccount));
                        this.onAccountChanged.next(this.currentAccount);
                    }
                    resolve(response);
                }, reject);
        });
    }

    /** PUT: update the account on the server */
    updateAccount(account): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account`;
            this._httpClient.put(requestUrl, account, httpOptions)
                .subscribe((response: any) => {
                    this.currentAccount = response;

                    if (this.currentAccount && this.currentAccount.email) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentAccount', JSON.stringify(this.currentAccount));
                        this.onAccountChanged.next(this.currentAccount);
                    }
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Change user password
     *
     * @param changePassword
     */
    changePassword(changePassword): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account/change_password`;
            this._httpClient.put(requestUrl, changePassword, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Initiate email confirm
     *
     */
    initEmailConfirm(): Promise<any> {
        let headers = new HttpHeaders({ 'Accept': 'text/plain' });
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account/email/confirm/init`;
            this._httpClient.post(requestUrl, null, {
                headers: headers
            })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Finish email confirm
     *
     * @param emailConfirm
     */
    finishEmailConfirm(emailConfirm): Promise<any> {
        let httpParams = new HttpParams().append("token", emailConfirm.token);
        let headers = new HttpHeaders({ 'Accept': 'text/plain' });
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account/email/confirm/finish`;
            this._httpClient.post(requestUrl, emailConfirm, {
                headers: headers,
                params: httpParams
            })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get Extensions
     *
     * @returns {Promise<any>}
     */
    getExtensions(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account/extensions`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    loggingOut() {
        localStorage.removeItem('currentAccount');
        this.currentAccount = null;
        this.onAccountChanged.next(this.currentAccount);
    }

    /** Log a AuthenticationService message with the MessageService */
    private log(message: string) {
        this._messagesService.add(`AccountService: ${message}`);
    }
}
