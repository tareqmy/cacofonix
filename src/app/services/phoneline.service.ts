import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class PhoneLineService implements Resolve<any>
{
    routeParams: any;
    phoneLine: any;
    onPhoneLineChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onPhoneLineChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPhoneLine()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get phone
     *
     * @returns {Promise<any>}
     */
    getPhoneLine(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.lineId === 'new') {
                this.onPhoneLineChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/phonelines/${this.routeParams.lineId}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.phoneLine = response;
                        this.onPhoneLineChanged.next(this.phoneLine);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save phoneLine
     *
     * @param phoneLine
     * @returns {Promise<any>}
     */
    savePhoneLine(phoneLine): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phonelines`;
            this._httpClient.put(requestUrl, phoneLine, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add phoneLine
     *
     * @param phoneLine
     * @returns {Promise<any>}
     */
    addPhoneLine(phoneLine): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phonelines`;
            this._httpClient.post(requestUrl, phoneLine, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete phoneLine
     *
     * @param phoneLine
     */
    deletePhoneLine(phoneLine): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phonelines/${phoneLine.id}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
