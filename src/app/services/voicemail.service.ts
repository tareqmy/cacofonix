import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class VoiceMailService implements Resolve<any>
{
    routeParams: any;
    voiceMail: any;
    onVoiceMailChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onVoiceMailChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getVoicemails()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get voiceMail
     *
     * @returns {Promise<any>}
     */
    getVoicemails(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/voicemails/${this.routeParams.voiceMailId}`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.voiceMail = response;
                    this.onVoiceMailChanged.next(this.voiceMail);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Save voiceMail
     *
     * @param voiceMail
     * @returns {Promise<any>}
     */
    saveVoiceMail(voiceMail): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/voicemails`;
            this._httpClient.put(requestUrl, voiceMail, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Change user password
     *
     * @param voiceMail
     * @param changePassword
     */
    changePassword(voiceMail, changePassword): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/voicemails/${voiceMail.id}/change/${changePassword.newPassword}`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
