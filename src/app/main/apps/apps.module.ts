import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { RoleGuard } from 'app/_guards/role-guard';

const routes = [
    {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'callcenterlive',
        loadChildren: './callcenterlive/callcenterlive.module#CallCenterLiveModule',
    },
    {
        path: 'history',
        loadChildren: './history/history.module#HistoryModule',
    },
    {
        path: 'report',
        loadChildren: './report/report.module#ReportModule',
    },
    {
        path: 'voicemailmessages',
        loadChildren: './voicemailmessages/voicemailmessages.module#VoiceMailMessagesModule',
    },
    {
        path: 'queuelive',
        loadChildren: './queuelive/queuelive.module#QueueLiveModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: 'systemlive',
        loadChildren: './systemlive/systemlive.module#SystemLiveModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN']
        }
    },
    {
        path: '**',
        redirectTo: 'pages/errors/error-404'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class AppsModule {
}
