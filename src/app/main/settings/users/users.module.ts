import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faLock, faLockOpen, faThumbsUp, faThumbsDown, faChessPawn, faChessRook, faChessKnight, faChessKing } from '@fortawesome/free-solid-svg-icons';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { UserComponent } from './user/user.component';
import { UserService } from 'app/services/user.service';
import { UsersComponent } from './users/users.component';
import { UsersService } from 'app/services/users.service';

const routes = [
    {
        path: ':id',
        component: UserComponent,
        resolve: {
            data: UserService
        }
        //todo: admin user should be stopped from going to new user page but allowed view user page
    },
    {
        path: '',
        component: UsersComponent,
        resolve: {
            data: UsersService
        }
    },
];

@NgModule({
    declarations: [
        UserComponent,
        UsersComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        UserService,
        UsersService
    ],
    entryComponents: [
    ]
})
export class UsersModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faLock);
        library.addIcons(faLockOpen);
        library.addIcons(faThumbsUp);
        library.addIcons(faThumbsDown);
        library.addIcons(faChessPawn);
        library.addIcons(faChessRook);
        library.addIcons(faChessKnight);
        library.addIcons(faChessKing);
    }
}
