export const environment = {
    production: false,
    hmr: true,
    settings: {
        backend: 'http://localhost:8080'
    }
};
