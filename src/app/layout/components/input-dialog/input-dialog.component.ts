import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';


@Component({
    selector: 'input-dialog',
    templateUrl: './input-dialog.component.html',
    styleUrls: ['./input-dialog.component.scss']
})
export class InputDialogComponent {

    public message: string;
    public label: string;
    inputValue: string;

    constructor(
        public dialogRef: MatDialogRef<InputDialogComponent>
    ) {
        this.inputValue = "";
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
