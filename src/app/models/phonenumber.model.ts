export class PhoneNumber {
    number: string;
    password: string;
    providerId: number;
    providerName: string;
    companyId: number;
    companyName: string;
    extensionId: number;
    baseExtension: string;

    /**
     * Constructor
     *
     * @param phoneNumber
     */
    constructor(phoneNumber) {
        this.number = phoneNumber.number || '';
        this.password = phoneNumber.password || '';
        this.providerId = phoneNumber.providerId;
        this.providerName = phoneNumber.providerName;
        this.companyId = phoneNumber.companyId;
        this.companyName = phoneNumber.companyName;
        this.extensionId = phoneNumber.extensionId;
        this.baseExtension = phoneNumber.baseExtension;
    }
}
