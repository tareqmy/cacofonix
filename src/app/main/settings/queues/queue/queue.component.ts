import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AudioContext } from 'angular-audio-context';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { Queue } from 'app/models/queue.model';
import { QueueService } from 'app/services/queue.service';
import { CommonService } from 'app/services/common.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'queue',
    templateUrl: './queue.component.html',
    styleUrls: ['./queue.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class QueueComponent implements OnInit, OnDestroy {
    queue: Queue;
    pageType: string;
    isSuperAdmin: boolean;
    queueForm: FormGroup;
    strategies: any;
    fileToUpload: File;

    companies: any;
    extensions: any;

    audioSource: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {QueueService} _queueService
     * @param {CommonService} _commonService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     * @param {AudioContext} _audioContext
     */
    constructor(
        private _queueService: QueueService,
        private _commonService: CommonService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router,
        private _audioContext: AudioContext
    ) {
        // Set the default
        this.queue = new Queue({});

        this.companies = [];

        this.extensions = [];

        this.audioSource = null;

        this.strategies = ['ringall',
            'leastrecent',
            'fewestcalls',
            'random',
            'rrmemory',
            'rrordered',
            'linear',
            'wrandom'];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update queue on changes
        this._queueService.onQueueChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(queue => {

                if (queue) {
                    this.queue = new Queue(queue);
                    this.pageType = 'edit';
                    this.queueForm = this.editQueueForm();
                    this._companiesService.getExtensions(this.queue.companyId).then((extensions) => {
                        this.extensions = extensions;
                    });
                }
                else {
                    this.pageType = 'new';
                    this.queue = new Queue({});
                    this.queueForm = this.createQueueForm();
                }
            });

        if (this.isSuperAdmin) {
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.queueForm.controls; }

    /**
     * Create queue form
     *
     * @returns {FormGroup}
     */
    createQueueForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.queue.id],
            queueId: [this.queue.queueId, [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z][a-zA-Z0-9]+')]],
            queueName: [this.queue.queueName, [Validators.required]],
            email: [this.queue.email, [Validators.email]],
            companyId: [this.queue.companyId, [Validators.required]],
            companyName: [this.queue.companyName],
            extensionId: [this.queue.extensionId, [Validators.required]],
            baseExtension: [this.queue.baseExtension],
            strategy: [this.queue.strategy, [Validators.required]],
            weight: [this.queue.weight, [Validators.required]],
            serviceLevel: [this.queue.serviceLevel, [Validators.required]],
            maxLength: [this.queue.maxLength, [Validators.required]],
            zeroEscapeEnabled: [this.queue.zeroEscapeEnabled]
        });
    }

    /**
     * Create queue form
     *
     * @returns {FormGroup}
     */
    editQueueForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.queue.id],
            queueId: [{ value: this.queue.queueId, disabled: true }, [Validators.required]],
            queueName: [this.queue.queueName, [Validators.required]],
            email: [this.queue.email, [Validators.email]],
            companyId: [{ value: this.queue.companyId, disabled: true }],
            companyName: [{ value: this.queue.companyName, disabled: true }],
            extensionId: [this.queue.extensionId, [Validators.required]],
            baseExtension: [this.queue.baseExtension],
            strategy: [this.queue.strategy, [Validators.required]],
            weight: [this.queue.weight, [Validators.required]],
            serviceLevel: [this.queue.serviceLevel, [Validators.required]],
            maxLength: [this.queue.maxLength, [Validators.required]],
            playInitialMessage: [this.queue.playInitialMessage],
            playMusicOnHold: [this.queue.playMusicOnHold],
            playComfortMessage: [this.queue.playComfortMessage],
            comfortMessageInterval: [this.queue.comfortMessageInterval, [Validators.required]],
            zeroEscapeEnabled: [this.queue.zeroEscapeEnabled]
        });
    }

    getExtensions(event): void {
        this._companiesService.getExtensions(this.queue.companyId).then((extensions) => {
            this.extensions = extensions;
            this.f.extensionId.setValue(null);
        });
    }

    /**
     * Save queue
     */
    saveQueue(): void {
        const data = this.queueForm.getRawValue();

        this._queueService.saveQueue(data)
            .then(() => {

                // Trigger the subscription with new data
                this._queueService.onQueueChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Queue saved');
            });
    }

    /**
     * Add queue
     */
    addQueue(): void {
        const data = this.queueForm.getRawValue();

        this._queueService.addQueue(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._queueService.onQueueChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Queue added');

                // Change the location with new one
                this._router.navigate(['/settings/queues/' + this.queue.id]);
            });
    }


    /**
     * Delete queue
     */
    deleteQueue(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.queueForm.getRawValue();

                this._queueService.deleteQueue(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._queueService.onQueueChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Queue deleted');

                        // Change the location to queues table
                        this._router.navigate(['/settings/queues']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Upload queue initial file
     */
    uploadInitialMessage(event): void {
        // console.log(event.target.files[0]);
        this.fileToUpload = event.target.files[0];
        this._queueService.uploadInitialMessage(this.queue, this.fileToUpload)
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Initial message uploaded');
            });
    }

    /**
     * Download queue initial file
     */
    downloadInitialMessage(): void {
        this._queueService.downloadInitialMessage()
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Initial message downloaded');
            });
    }

    /**
     * Play queue initial file
     */
    playInitialMessage(): void {
        this._queueService.playInitialMessage()
            .then((result) => {
                this.playAudio(result);
            });
    }

    /**
     * Upload Music on hold file
     */
    uploadMusicOnHold(event): void {
        // console.log(event.target.files[0]);
        this.fileToUpload = event.target.files[0];
        this._queueService.uploadMusicOnHold(this.queue, this.fileToUpload)
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Music on hold uploaded');
            });
    }

    /**
     * Download Music on hold file
     */
    downloadMusicOnHold(): void {
        this._queueService.downloadMusicOnHold()
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Music on hold downloaded');
            });
    }

    /**
     * Play Music on hold file
     */
    playMusicOnHold(): void {
        this._queueService.playMusicOnHold()
            .then((result) => {
                this.playAudio(result);
            });
    }

    /**
     * Upload queue comfort file
     */
    uploadComfortMessage(event): void {
        // console.log(event.target.files[0]);
        this.fileToUpload = event.target.files[0];
        this._queueService.uploadComfortMessage(this.queue, this.fileToUpload)
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Comfort message uploaded');
            });
    }

    /**
     * Download queue comfort file
     */
    downloadComfortMessage(): void {
        this._queueService.downloadComfortMessage()
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Comfort message downloaded');
            });
    }

    /**
     * Play queue comfort file
     */
    playComfortMessage(): void {
        this._queueService.playComfortMessage()
            .then((result) => {
                this.playAudio(result);
            });
    }

    playAudio(arrayData): void {
        this.stopAudio();
        var soundBuffer;
        this.audioSource = this._audioContext.createBufferSource();
        let destination = this._audioContext.destination;
        let as = this.audioSource;
        this._audioContext.decodeAudioData(arrayData,
            function(buffer) {
                soundBuffer = buffer;
                as.buffer = soundBuffer;
                as.connect(destination);
                as.start(0);
            }
        );
    }

    stopAudio(): void {
        if (this.audioSource) {
            this.audioSource.stop();
        }
        this.audioSource = null;
    }

}
