import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { AccountService } from 'app/services/account.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class AutoAttendantsService implements Resolve<any> {

    autoAttendants: any[];
    onAutoAttendantsChanged: BehaviorSubject<any>;

    currentCompanyId: number;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService,
        private _accountService: AccountService
    ) {
        // Set the defaults
        this.onAutoAttendantsChanged = new BehaviorSubject({});

        this.currentCompanyId = null;
        this._accountService.onAccountChanged.subscribe(user => {
            if (user) {
                this.currentCompanyId = user.companyId;
            }
        });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAutoAttendants()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get AutoAttendants
     *
     * @returns {Promise<any>}
     */
    getAutoAttendants(): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestUrl = `${environment.settings.backend}/api/autoattendants`;
            if (!this._authenticationService.isSuperAdmin()) {
                requestUrl = `${environment.settings.backend}/api/companies/${this.currentCompanyId}/autoattendants`;
            }
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.autoAttendants = response;
                    this.onAutoAttendantsChanged.next(this.autoAttendants);
                    resolve(response);
                }, reject);
        });
    }
}
