import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { AccountService } from 'app/services/account.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class UsersService implements Resolve<any> {

    users: any[];
    onUsersChanged: BehaviorSubject<any>;

    currentCompanyId: number;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService,
        private _accountService: AccountService
    ) {
        // Set the defaults
        this.onUsersChanged = new BehaviorSubject({});

        this.currentCompanyId = null;
        this._accountService.onAccountChanged.subscribe(user => {
            if (user) {
                this.currentCompanyId = user.companyId;
            }
        });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getUsers()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get Users
     *
     * @returns {Promise<any>}
     */
    getUsers(): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestUrl = `${environment.settings.backend}/api/users`;
            if (!this._authenticationService.isSuperAdmin()) {
                requestUrl = `${environment.settings.backend}/api/companies/${this.currentCompanyId}/users`;
            }
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.users = response;
                    this.onUsersChanged.next(this.users);
                    resolve(response);
                }, reject);
        });
    }
}
