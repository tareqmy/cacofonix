import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { saveAs } from 'file-saver';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpOptionsUpload = {
    headers: new HttpHeaders({ 'Accept': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class QueueService implements Resolve<any>
{
    routeParams: any;
    queue: any;
    onQueueChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onQueueChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getQueue()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get queue
     *
     * @returns {Promise<any>}
     */
    getQueue(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onQueueChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.queue = response;
                        this.onQueueChanged.next(this.queue);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save queue
     *
     * @param queue
     * @returns {Promise<any>}
     */
    saveQueue(queue): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues`;
            this._httpClient.put(requestUrl, queue, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add queue
     *
     * @param queue
     * @returns {Promise<any>}
     */
    addQueue(queue): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues`;
            this._httpClient.post(requestUrl, queue, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete queue
     *
     * @param queue
     */
    deleteQueue(queue): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${queue.id}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Upload queue initial message
     *
     * @param queue
     * @param file
     */
    uploadInitialMessage(queue, promptFile): Promise<any> {
        const formData = new FormData();
        formData.append('file', promptFile, promptFile.name);
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${queue.id}/initialmessage`;
            this._httpClient.post(requestUrl, formData, httpOptionsUpload)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Upload queue comfort message
     *
     * @param queue
     * @param file
     */
    uploadMusicOnHold(queue, promptFile): Promise<any> {
        const formData = new FormData();
        formData.append('file', promptFile, promptFile.name);
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${queue.id}/musiconhold`;
            this._httpClient.post(requestUrl, formData, httpOptionsUpload)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Upload queue comfort message
     *
     * @param queue
     * @param file
     */
    uploadComfortMessage(queue, promptFile): Promise<any> {
        const formData = new FormData();
        formData.append('file', promptFile, promptFile.name);
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${queue.id}/comfortmessage`;
            this._httpClient.post(requestUrl, formData, httpOptionsUpload)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Download queue initialmessage
     */
    downloadInitialMessage(): Promise<any> {
        var FileSaver = require('file-saver');

        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}/initialmessage/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer", observe: "response" })
                .subscribe((response: any) => {
                    var filename = '';
                    var disposition = response.headers.get("Content-Disposition");
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }
                    filename = filename ? filename : 'download.wav';
                    var blob = new Blob([response.body], { type: "audio/wave" });
                    FileSaver.saveAs(blob, filename);
                    resolve(response.body);
                }, reject);
        });
    }

    /**
     * Play queue initialmessage
     */
    playInitialMessage(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}/initialmessage/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer" })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Download queue musiconhold
     */
    downloadMusicOnHold(): Promise<any> {
        var FileSaver = require('file-saver');

        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}/musiconhold/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer", observe: "response" })
                .subscribe((response: any) => {
                    var filename = '';
                    var disposition = response.headers.get("Content-Disposition");
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }
                    filename = filename ? filename : 'download.wav';
                    var blob = new Blob([response.body], { type: "audio/wave" });
                    FileSaver.saveAs(blob, filename);
                    resolve(response.body);
                }, reject);
        });
    }

    /**
     * Play queue musiconhold
     */
    playMusicOnHold(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}/musiconhold/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer" })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Download queue comfortmessage
     */
    downloadComfortMessage(): Promise<any> {
        var FileSaver = require('file-saver');

        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}/comfortmessage/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer", observe: "response" })
                .subscribe((response: any) => {
                    var filename = '';
                    var disposition = response.headers.get("Content-Disposition");
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }
                    filename = filename ? filename : 'download.wav';
                    var blob = new Blob([response.body], { type: "audio/wave" });
                    FileSaver.saveAs(blob, filename);
                    resolve(response.body);
                }, reject);
        });
    }

    /**
     * Play queue comfortmessage
     */
    playComfortMessage(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/queues/${this.routeParams.id}/comfortmessage/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer" })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
