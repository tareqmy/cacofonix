import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef } from '@angular/material';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

import { AudioContext } from 'angular-audio-context';

import { InputDialogComponent } from 'app/layout/components/input-dialog/input-dialog.component';

import { CommonService } from 'app/services/common.service';
import { QueueLiveService } from 'app/services/queuelive.service';
import { AgentLiveService } from 'app/services/agentlive.service';
import { QueueLive } from 'app/models/queuelive.model';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'queuelive-list',
    templateUrl: './queuelive.component.html',
    styleUrls: ['./queuelive.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class QueueLiveComponent implements OnInit, OnDestroy {

    channelDataSource: LiveChannelDataSource | null;
    displayedChannelColumns = ['callerIdName', 'callerIdNum', 'destination', 'startTime', 'duration', 'queueCallDirection', 'queued', 'wait', 'position'];

    messageDataSource: LiveMessageDataSource | null;
    displayedMessageColumns = ['dir', 'msgNum', 'callerId', 'origTime', 'duration', 'actions'];

    agentDataSource: LiveAgentDataSource | null;
    displayedAgentColumns = ['agentName', 'baseExtension', 'sipUserName', 'deviceStatus', 'endpointStatus', 'statusMap', 'channels', 'actions'];

    queueLiveSource: LiveQueueDataSource | null;
    displayedQueueLiveColumns = ['label1', 'value1', 'label2', 'value2'];

    queueLive: QueueLive;

    sipUserName: any;

    audioSource: any;
    msgId: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    /**
     * Constructor
     *
     * @param {QueueLiveService} _queueLiveService
     * @param {AgentLiveService} _agentLiveService
     * @param {CommonService} _commonService
     * @param {MatDialog} _matDialog
     * @param {AudioContext} _audioContext
     */
    constructor(
        private _queueLiveService: QueueLiveService,
        private _agentLiveService: AgentLiveService,
        private _commonService: CommonService,
        private _matDialog: MatDialog,
        private _audioContext: AudioContext
    ) {
        this.audioSource = null;
        this.msgId = null;
        this.queueLive = new QueueLive({});
        this.getAgentStatus();
        this._queueLiveService.onQueueLiveChanged.subscribe(queueLive => {
            if (queueLive) {
                this.queueLive = queueLive;
            }
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.messageDataSource = new LiveMessageDataSource(this._queueLiveService);
        this.channelDataSource = new LiveChannelDataSource(this._queueLiveService);
        this.agentDataSource = new LiveAgentDataSource(this._queueLiveService);
        this.queueLiveSource = new LiveQueueDataSource(this._queueLiveService);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        this._queueLiveService.closeEvents();
        this._agentLiveService.closeEvents(true);
        this.stopAudio();
    }

    getAgentStatus(): void {
        this._agentLiveService.getAgentSipUserName().then((sipUserName) => {
            if (sipUserName) {
                this.sipUserName = sipUserName;
                this._agentLiveService.getAgentLive();
            } else {
                this.sipUserName = null;
            }
        });
    }

    getAgentStatusInQueue(agentStatusMap): string {
        return agentStatusMap[this.queueLive.queueId];
    }

    isJoinedCallCenter(): boolean {
        return !_.isNil(this.sipUserName);
    }

    isPaused(): boolean {
        if (this.isJoinedCallCenter() && this._agentLiveService.agentLive) {
            return this._agentLiveService.agentLive.paused;
        }
        return false;
    }

    isJoinedQueue(): boolean {
        return !_.isNil(this.queueLive) && !_.isNil(this.queueLive.agentStatusMap) && !_.isNil(this.queueLive.agentStatusMap[this.sipUserName]);
    }

    /**
     * Join queue
     */
    joinQueue(id): void {
        this._queueLiveService.joinQueue(id)
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Joined queue');
            });
    }

    /**
     * Queue call
     */
    callQueue(id): void {
        const dialogRef = this._matDialog.open(InputDialogComponent, {
            width: '250px'
        });

        dialogRef.componentInstance.message = 'Please enter the destination.';
        dialogRef.componentInstance.label = 'Number';
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._queueLiveService.callQueue(id, result)
                    .then((result) => {
                        // Show the success message
                        this._commonService.showSnackBar('Calling...');
                    });
            }
        });
    }

    /**
     * Unjoin queue
     */
    leaveQueue(id): void {
        this._queueLiveService.leaveQueue(id)
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Left queue');
            });
    }

    /**
     * Unjoin queue
     */
    kickOutQueue(phoneLineId): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to force this agent out?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._queueLiveService.kickOutQueue(this.queueLive.id, phoneLineId)
                    .then((result) => {
                        // Show the success message
                        this._commonService.showSnackBar('Unjoined queue');
                    });
            }
            this.confirmDialogRef = null;
        });

    }

    play(msgId): void {
        this.stopAudio();
        this.msgId = msgId;
        this._queueLiveService.playVoicemailMessage(this.queueLive.id, msgId)
            .then((result) => {
                this.playAudio(result);
            });
    }

    isPlaying(msgId): boolean {
        return msgId === this.msgId;
    }

    playAudio(arrayData): void {
        this.audioSource = this._audioContext.createBufferSource();
        let destination = this._audioContext.destination;
        let as = this.audioSource;
        this._audioContext.decodeAudioData(arrayData,
            function(buffer) {
                as.buffer = buffer;
                as.connect(destination);
                as.start(0);
            }
        );
    }

    stopAudio(): void {
        if (this.audioSource) {
            this.audioSource.stop();
        }
        this.audioSource = null;
        this.msgId = null;
    }

    callback(msgId): void {
        this._queueLiveService.callbackVoicemailMessage(this.queueLive.id, msgId);
    }

    delete(msgId): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete this message?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {

                this._queueLiveService.deleteVoicemailMessage(this.queueLive.id, msgId)
                    .then(() => {

                        // Show the success message
                        this._commonService.showSnackBar('Message deleted');
                    });
            }
            this.confirmDialogRef = null;
        });

    }
}

export class LiveMessageDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueLiveService} _queueLiveService
     */
    constructor(
        private _queueLiveService: QueueLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueLiveService.onQueueMessageLiveChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class LiveChannelDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueLiveService} _queueLiveService
     */
    constructor(
        private _queueLiveService: QueueLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueLiveService.onQueueChannelLiveChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class LiveAgentDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueLiveService} _queueLiveService
     */
    constructor(
        private _queueLiveService: QueueLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueLiveService.onQueueAgentLiveChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class LiveQueueDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueLiveService} _queueLiveService
     */
    constructor(
        private _queueLiveService: QueueLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueLiveService.onQueueLiveSummaryChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
