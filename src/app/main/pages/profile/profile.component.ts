import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';
import { takeUntil } from 'rxjs/operators';

import { User } from 'app/models/user.model';
import { AccountService } from 'app/services/account.service';
import { CommonService } from 'app/services/common.service';
import { UtilityService } from 'app/services/utility.service';
import { AccountPasswordChangeFormDialogComponent } from './password-change-form/password-change-form.component';
import { EmailConfirmFormDialogComponent } from './email-confirm-form/email-confirm-form.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileComponent implements OnInit, OnDestroy {

    user: User;
    userForm: FormGroup;
    userRoles: any;
    currentAccountName: string;
    currentAccountEmail: string;
    dialogRef: any;
    timeZones: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _formBuilder: FormBuilder,
        private _accountService: AccountService,
        private _commonService: CommonService,
        private _utilityService: UtilityService,
        private _matDialog: MatDialog
    ) {

        // Set the private defaults
        this.user = new User({});

        this.userRoles = {};

        this.timeZones = {};

        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update user on changes
        this._accountService.onAccountChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                    this.currentAccountName = user.firstName + " " + user.lastName;
                    this.currentAccountEmail = user.email;
                }
                this.userForm = this.editAccountForm();
            });

        this.timeZones = this._utilityService.zonesValue;

        this.userRoles = this._utilityService.rolesValue;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Edit account form
     *
     * @returns {FormGroup}
     */
    editAccountForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.user.id],
            firstName: [this.user.firstName, [Validators.required]],
            lastName: [this.user.lastName],
            email: [{ value: this.user.email, disabled: true }],
            userRole: [{ value: this.user.userRole, disabled: true }],
            enabled: [this.user.enabled],
            locked: [this.user.locked],
            zoneId: [this.user.zoneId, [Validators.required]],
            emailConfirmed: [this.user.emailConfirmed]
        });
    }

    editProfile(userForm): void {
        this._accountService.updateAccount(userForm.getRawValue()).then(user => {
            if (user) {
                this.user = user;

                // Show the success message
                this._commonService.showSnackBar('Account updated');
            }
        });
    }

    confirmEmail(): void {
        this._accountService.initEmailConfirm().then(() => {

            // Show the success message
            this._commonService.showSnackBar('Confirm email sent');

            this.dialogRef = this._matDialog.open(EmailConfirmFormDialogComponent, {
                panelClass: 'email-confirm-form-dialog'
            });

            this.dialogRef.afterClosed()
                .subscribe((response: FormGroup) => {
                    if (!response) {
                        return;
                    }

                    this._accountService.finishEmailConfirm(response.getRawValue()).then(() => {

                        this.user.emailConfirmed = true;

                        // Show the success message
                        this._commonService.showSnackBar('Email Confirmed');
                    });
                });
        });
    }

    changePassword(): void {
        this.dialogRef = this._matDialog.open(AccountPasswordChangeFormDialogComponent, {
            panelClass: 'password-change-form-dialog'
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this._accountService.changePassword(response.getRawValue()).then(() => {
                    // Show the success message
                    this._commonService.showSnackBar('Password updated');
                });
            });
    }
}
