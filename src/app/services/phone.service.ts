import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class PhoneService implements Resolve<any>
{
    routeParams: any;
    phone: any;
    onPhoneChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onPhoneChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPhone()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get phone
     *
     * @returns {Promise<any>}
     */
    getPhone(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onPhoneChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/phones/${this.routeParams.id}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.phone = response;
                        this.onPhoneChanged.next(this.phone);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save phone
     *
     * @param phone
     * @returns {Promise<any>}
     */
    savePhone(phone): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phones`;
            this._httpClient.put(requestUrl, phone, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add phone
     *
     * @param phone
     * @returns {Promise<any>}
     */
    addPhone(phone): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phones`;
            this._httpClient.post(requestUrl, phone, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete phone
     *
     * @param phone
     */
    deletePhone(phone): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phones/${phone.id}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
