import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

import { saveAs } from 'file-saver';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class VoiceMailMessagesService
{
    voiceMailMessages: any[];
    onVoiceMailMessagesChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.voiceMailMessages = [];
        this.onVoiceMailMessagesChanged = new BehaviorSubject({});
    }

    /**
     * Get voiceMailMessage
     *
     * @returns {Promise<any>}
     */
    getVoicemailMessages(extensionId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/voicemails/messages`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.voiceMailMessages = response;
                    this.onVoiceMailMessagesChanged.next(this.voiceMailMessages);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Download voiceMailMessage
     *
     * @returns {Promise<any>}
     */
    downloadVoicemailMessage(extensionId, msgId): Promise<any> {
        var FileSaver = require('file-saver');

        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/voicemails/messages/${msgId}/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer", observe: "response" })
                .subscribe((response: any) => {
                    var filename = '';
                    var disposition = response.headers.get("Content-Disposition");
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }
                    filename = filename ? filename : 'download.wav';
                    var blob = new Blob([response.body], { type: "audio/wave" });
                    FileSaver.saveAs(blob, filename);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Play voiceMailMessage
     *
     * @returns {Promise<any>}
     */
    playVoicemailMessage(extensionId, msgId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/voicemails/messages/${msgId}/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer" })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Callback voiceMailMessage
     *
     * @returns {Promise<any>}
     */
    callbackVoicemailMessage(extensionId, msgId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/voicemails/messages/${msgId}/callback`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete voiceMailMessage
     *
     * @param extensionId
     * @param msgId
     */
    deleteVoicemailMessage(extensionId, msgId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/voicemails/messages/${msgId}`;
            this._httpClient.delete(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    cleanup() {
        this.voiceMailMessages = [];
        this.onVoiceMailMessagesChanged.next(null);
    }
}
