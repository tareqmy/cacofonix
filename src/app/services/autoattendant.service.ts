import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { saveAs } from 'file-saver';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpOptionsUpload = {
    headers: new HttpHeaders({ 'Accept': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class AutoAttendantService implements Resolve<any>
{
    routeParams: any;
    autoAttendant: any;
    onAutoAttendantChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onAutoAttendantChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAutoAttendant()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get autoAttendant
     *
     * @returns {Promise<any>}
     */
    getAutoAttendant(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onAutoAttendantChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/autoattendants/${this.routeParams.id}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.autoAttendant = response;
                        this.onAutoAttendantChanged.next(this.autoAttendant);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save autoAttendant
     *
     * @param autoAttendant
     * @returns {Promise<any>}
     */
    saveAutoAttendant(autoAttendant): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/autoattendants`;
            this._httpClient.put(requestUrl, autoAttendant, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add autoAttendant
     *
     * @param autoAttendant
     * @returns {Promise<any>}
     */
    addAutoAttendant(autoAttendant): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/autoattendants`;
            this._httpClient.post(requestUrl, autoAttendant, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete autoAttendant
     *
     * @param autoAttendant
     */
    deleteAutoAttendant(autoAttendant): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/autoattendants/${autoAttendant.id}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Upload autoAttendant prompt
     *
     * @param autoAttendant
     * @param file
     */
    uploadAutoAttendantPrompt(autoAttendant, promptFile): Promise<any> {
        const formData = new FormData();
        formData.append('file', promptFile, promptFile.name);
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/autoattendants/${autoAttendant.id}/prompts`;
            this._httpClient.post(requestUrl, formData, httpOptionsUpload)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Download autoAttendant prompt
     *
     * @param autoAttendant
     */
    downloadAutoAttendantPrompt(): Promise<any> {
        var FileSaver = require('file-saver');

        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/autoattendants/${this.routeParams.id}/prompts/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer", observe: "response" })
                .subscribe((response: any) => {
                    var filename = '';
                    var disposition = response.headers.get("Content-Disposition");
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');
                        }
                    }
                    filename = filename ? filename : 'download.wav';
                    var blob = new Blob([response.body], { type: "audio/wave" });
                    FileSaver.saveAs(blob, filename);
                    resolve(response.body);
                }, reject);
        });
    }

    /**
     * Play autoAttendant prompt
     *
     * @param autoAttendant
     */
    playAutoAttendantPrompt(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/autoattendants/${this.routeParams.id}/prompts/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer" })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
