export class User {
    id: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    userRole: string;
    enabled: boolean;
    locked: boolean;
    zoneId: string;
    emailConfirmed: boolean;
    companyId: number;
    companyName: string;

    /**
     * Constructor
     *
     * @param user
     */
    constructor(user) {
        this.id = user.id || '';
        this.email = user.email || '';
        this.password = user.password || '';
        this.firstName = user.firstName || '';
        this.lastName = user.lastName || '';
        this.userRole = user.userRole || 'ROLE_USER';
        this.enabled = user.enabled;
        this.locked = user.locked;
        this.zoneId = user.zoneId || 'UTC';
        this.emailConfirmed = user.emailConfirmed;
        this.companyId = user.companyId;
        this.companyName = user.companyName;
    }
}

export class ChangePassword {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
}
