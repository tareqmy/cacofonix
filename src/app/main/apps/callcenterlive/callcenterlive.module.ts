import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule,
    MatDividerModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCircle, faPhoneAlt, faChevronUp, faChevronDown, faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { SelectDialogModule } from 'app/layout/components/select-dialog/select-dialog.module';
import { InputDialogModule } from 'app/layout/components/input-dialog/input-dialog.module';

import { CallCenterLiveComponent } from './callcenterlive/callcenterlive.component';
import { CallCenterLiveService } from 'app/services/callcenterlive.service';

const routes = [
    {
        path: '',
        component: CallCenterLiveComponent,
        resolve: {
            data: CallCenterLiveService
        }
    },
];

@NgModule({
    declarations: [
        CallCenterLiveComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,
        MatDividerModule,

        SelectDialogModule,
        InputDialogModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        CallCenterLiveService
    ],
    entryComponents: [
    ]
})
export class CallCenterLiveModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faCircle);
        library.addIcons(faPhoneAlt);
        library.addIcons(faChevronUp);
        library.addIcons(faChevronDown);
        library.addIcons(faThumbsUp);
        library.addIcons(faThumbsDown);
    }
}
