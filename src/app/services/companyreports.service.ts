import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as moment from 'moment';

import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class CompanyReportsService {

    routeParams: any;
    queueReportsSummary: any[];
    queueReports: any[];
    agentReportsSummary: any[];
    agentReports: any[];
    onQueueReportsSummaryChanged: BehaviorSubject<any>;
    onQueueReportsChanged: BehaviorSubject<any>;
    onAgentReportsSummaryChanged: BehaviorSubject<any>;
    onAgentReportsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onQueueReportsChanged = new BehaviorSubject({});
        this.onQueueReportsSummaryChanged = new BehaviorSubject({});
        this.onAgentReportsChanged = new BehaviorSubject({});
        this.onAgentReportsSummaryChanged = new BehaviorSubject({});
        this.cleanup();
    }

    /**
     * Get CompanyReport
     *
     * @returns {Promise<any>}
     */
    getCompanyReports(from: string, to: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/companies/companyreports`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.populateData(response);
                    resolve(response);
                }, reject);
        });
    }

    //1. queue distribuition per queue
    //2. queue distribuition per week
    //3. queue distribuition per day
    populateData(response: any) {
        this.queueReports = [];
        this.queueReportsSummary = [];
        this.agentReports = [];
        this.agentReportsSummary = [];

        this.queueReportsSummary.push({ "label1": "Calls", "value1": response.totalCalls, "label2": "Overflowed", "value2": response.totalOverflowed });
        this.queueReportsSummary.push({ "label1": "Received", "value1": response.totalReceived, "label2": "Answered", "value2": response.totalAnswered });
        this.queueReportsSummary.push({ "label1": "Transferred", "value1": response.totalTransferred, "label2": "Unanswered", "value2": response.totalUnanswered });
        this.queueReportsSummary.push({ "label1": "Abandoned", "value1": response.totalAbandoned, "label2": "Abandoned", "value2": response.percentAbandoned + " %" });

        this.agentReportsSummary.push({ "label1": "Agents", "value1": response.totalAgents, "label2": "Sessions", "value2": response.totalSessions });
        this.agentReportsSummary.push({ "label1": "Shortest Session Time", "value1": response.shortestSessionTime, "label2": "Longest Session Time", "value2": response.longestSessionTime });
        this.agentReportsSummary.push({ "label1": "Average Session Time", "value1": response.averageSessionTime, "label2": "Total Session Time", "value2": response.totalSessionTime });

        response.queueReports.forEach(element => {
            this.queueReports.push({
                "queueId": element.queueId,
                "queueName": element.queueName,
                "total": element.total,
                "overflowed": element.overflowed,
                "received": element.received,
                "answered": element.answered,
                "transferred": element.transferred,
                "unanswered": element.unanswered,
                "abandoned": element.abandoned,
                "averageWaitTime": element.averageWaitTime,
                "averageWaitDuration": element.averageWaitDuration,
                "averageTalkTime": element.averageTalkTime,
                "averageTalkDuration": element.averageTalkDuration,
                "maxCallers": element.maxCallers,
                "percentAnswered": element.percentAnswered,
                "percentUnanswered": element.percentUnanswered,
                "percentSLA": element.percentSLA,
                "totalTalkTime": element.totalTalkTime,

                "totalOutbound": element.totalOutbound,
                "answeredOutbound": element.answeredOutbound,
                "unansweredOutbound": element.unansweredOutbound,
                "totalRingOutboundDuration": element.totalRingOutboundDuration,
                "totalRingOutboundTime": element.totalRingOutboundTime,
                "totalTalkOutboundDuration": element.totalTalkOutboundDuration,
                "totalTalkOutboundTime": element.totalTalkOutboundTime,
                "averageRingOutboundDuration": element.averageRingOutboundDuration,
                "averageRingOutboundTime": element.averageRingOutboundTime,
                "averageTalkOutboundDuration": element.averageTalkOutboundDuration,
                "averageTalkOutboundTime": element.averageTalkOutboundTime,
                "percentAnsweredOutbound": element.percentAnsweredOutbound,
                "percentUnansweredOutbound": element.percentUnansweredOutbound
            });
        });
        response.agentReports.forEach(element => {
            this.agentReports.push({
                "userId": element.userId,
                "userName": element.userName,

                "sessions": element.sessions,
                "totalSessionTime": element.totalSessionTime,
                "averageSessionTime": element.averageSessionTime,
                "percentSession": element.percentSession,
                "joined": element.joined,
                "totalJoinedDuration": element.totalJoinedDuration,
                "totalJoinedTime": element.totalJoinedTime,
                "averageJoinedTime": element.averageJoinedTime,
                "percentJoined": element.percentJoined,
                "pauses": element.pauses,
                "totalPausedDuration": element.totalPausedDuration,
                "totalPausedTime": element.totalPausedTime,
                "averagePausedTime": element.averagePausedTime,
                "percentPaused": element.percentPaused,
                "totalCallDuration": element.totalCallDuration,
                "totalCallTime": element.totalCallTime,
                "totalIdleTime": element.totalIdleTime,

                "totalCalls": element.totalCalls,
                "totalQueueCalls": element.totalQueueCalls,
                "totalQueueCallsInbound": element.totalQueueCallsInbound,
                "totalQueueCallsOutbound": element.totalQueueCallsOutbound,
                "totalQueueCallsAnswered": element.totalQueueCallsAnswered,
                "totalQueueCallsUnanswered": element.totalQueueCallsUnanswered,
                "averageHandleDuration": element.averageHandleDuration,
                "averageHandleTime": element.averageHandleTime,

                "totalACDCallsAnswered": element.totalACDCallsAnswered,
                "totalACDCallsEndByCaller": element.totalACDCallsEndByCaller,
                "totalACDCallsEndByAgent": element.totalACDCallsEndByAgent,
                "totalACDCallsTransferred": element.totalACDCallsTransferred,
                "totalACDCallsFailed": element.totalACDCallsFailed,

                "totalQueueCallRingDuration": element.totalQueueCallRingDuration,
                "totalQueueCallRingTime": element.totalQueueCallRingTime,
                "totalQueueCallTalkDuration": element.totalQueueCallTalkDuration,
                "totalQueueCallTalkTime": element.totalQueueCallTalkTime
            });
        });
        this.onQueueReportsSummaryChanged.next(this.queueReportsSummary);
        this.onQueueReportsChanged.next(this.queueReports);
        this.onAgentReportsSummaryChanged.next(this.agentReportsSummary);
        this.onAgentReportsChanged.next(this.agentReports);
    }

    //1. agent availability
    //2. agent pause detail
    //3. agent call disposition be agent

    cleanup() {
        this.queueReportsSummary = [];
        this.queueReports = [];
        this.agentReportsSummary = [];
        this.agentReports = [];
        this.onQueueReportsSummaryChanged.next(null);
        this.onQueueReportsChanged.next(null);
        this.onAgentReportsSummaryChanged.next(null);
        this.onAgentReportsChanged.next(null);
    }
}
