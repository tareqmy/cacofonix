export class Queue {
    id: number;
    queueId: string;
    queueName: string;
    email: string;
    extensionId: number;
    baseExtension: string;
    companyId: number;
    companyName: string;
    strategy: string;
    weight: number;
    serviceLevel: number;
    maxLength: number;
    playInitialMessage: boolean;
    initialMessageFileName: string;
    playMusicOnHold: boolean;
    mohFileName: string;
    playComfortMessage: boolean;
    comfortMessageFileName: string;
    comfortMessageInterval: number;
    zeroEscapeEnabled: boolean;

    /**
     * Constructor
     *
     * @param queue
     */
    constructor(queue) {
        this.id = queue.id || '';
        this.queueId = queue.queueId || '';
        this.queueName = queue.queueName || '';
        this.email = queue.email;
        this.extensionId = queue.extensionId;
        this.baseExtension = queue.baseExtension;
        this.companyId = queue.companyId;
        this.companyName = queue.companyName;
        this.strategy = queue.strategy;
        this.weight = queue.weight;
        this.serviceLevel = queue.serviceLevel;
        this.maxLength = queue.maxLength;
        this.playInitialMessage = queue.playInitialMessage;
        this.initialMessageFileName = queue.initialMessageFileName;
        this.playMusicOnHold = queue.playMusicOnHold;
        this.mohFileName = queue.mohFileName;
        this.playComfortMessage = queue.playComfortMessage;
        this.comfortMessageFileName = queue.comfortMessageFileName;
        this.comfortMessageInterval = queue.comfortMessageInterval;
        this.zeroEscapeEnabled = queue.zeroEscapeEnabled;
    }
}
