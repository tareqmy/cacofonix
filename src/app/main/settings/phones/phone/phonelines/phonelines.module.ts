import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { PhoneLineComponent } from './phoneline/phoneline.component';
import { PhoneLineService } from 'app/services/phoneline.service';
import { PhoneLinesComponent } from './phonelines/phonelines.component';
import { PhoneLinesService } from 'app/services/phonelines.service';

const routes = [
    {
        path: ':lineId',
        component: PhoneLineComponent,
        resolve: {
            data: PhoneLineService
        }
    },
    {
        path: '',
        component: PhoneLinesComponent,
        resolve: {
            data: PhoneLinesService
        }
    },
];

@NgModule({
    declarations: [
        PhoneLinesComponent,
        PhoneLineComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        PhoneLinesService,
        PhoneLineService
    ],
    entryComponents: [
    ]
})
export class PhoneLinesModule { }
