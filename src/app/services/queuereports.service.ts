import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as moment from 'moment';

import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class QueueReportsService {

    routeParams: any;
    queueReports: any[];
    queueReportsInbound: any[];
    queueReportsOutbound: any[];
    onQueueReportsChanged: BehaviorSubject<any>;
    onQueueReportsInboundChanged: BehaviorSubject<any>;
    onQueueReportsOutboundChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onQueueReportsChanged = new BehaviorSubject({});
        this.onQueueReportsInboundChanged = new BehaviorSubject({});
        this.onQueueReportsOutboundChanged = new BehaviorSubject({});
        this.cleanup();
    }

    /**
     * Get QueueReport
     *
     * @returns {Promise<any>}
     */
    getQueueReports(queueId: number, from: string, to: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/queues/${queueId}/queuereports`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.queueReports = [];
                    // this.queueReports.push({ "label1": "Name", "value1": response.queueName, "label2": "Zone", "value2": response.zoneId });
                    this.queueReports.push({ "label1": "Logins", "value1": response.logins, "label2": "Logoffs", "value2": response.logoffs });
                    this.queueReports.push({ "label1": "Pauses", "value1": response.pauses, "label2": "Resumes", "value2": response.resumes });
                    this.queueReports.push({ "label1": "Max Callers", "value1": response.maxCallers, "label2": "Max Joined Agents", "value2": response.maxJoinedAgents });
                    this.onQueueReportsChanged.next(this.queueReports);

                    this.queueReportsInbound = [];
                    this.queueReportsInbound.push({ "label1": "Received", "value1": response.received, "label2": "Overflowed", "value2": response.overflowed });
                    this.queueReportsInbound.push({ "label1": "Answered", "value1": response.answered, "label2": "Transferred", "value2": response.transferred });
                    this.queueReportsInbound.push({ "label1": "Unanswered", "value1": response.unanswered, "label2": "Abandoned", "value2": response.abandoned });
                    this.queueReportsInbound.push({ "label1": "Answered", "value1": response.percentAnswered + " %", "label2": "Transferred", "value2": response.percentTransferred + " %" });
                    this.queueReportsInbound.push({ "label1": "Unanswered", "value1": response.percentUnanswered + " %", "label2": "Abandoned", "value2": response.percentAbandoned + " %" });
                    this.queueReportsInbound.push({ "label1": "Within SLA", "value1": response.callsWithinSLA, "label2": "SLA", "value2": response.percentSLA + " %" });
                    this.queueReportsInbound.push({ "label1": "Wait Time", "value1": response.totalWaitTime, "label2": "Talk Time", "value2": response.totalTalkTime });
                    this.queueReportsInbound.push({ "label1": "Average Wait Time", "value1": response.averageWaitTime, "label2": "Average Talk Time", "value2": response.averageTalkTime });
                    this.onQueueReportsInboundChanged.next(this.queueReportsInbound);

                    this.queueReportsOutbound = [];
                    this.queueReportsOutbound.push({ "label1": "Answered", "value1": response.answeredOutbound, "label2": "Unanswered", "value2": response.unansweredOutbound });
                    this.queueReportsOutbound.push({ "label1": "Answered", "value1": response.percentAnsweredOutbound + " %", "label2": "Unanswered", "value2": response.percentUnansweredOutbound + " %" });
                    this.queueReportsOutbound.push({ "label1": "Ring Time", "value1": response.totalRingOutboundTime, "label2": "Talk Time", "value2": response.totalTalkOutboundTime });
                    this.queueReportsOutbound.push({ "label1": "Average Ring Time", "value1": response.averageRingOutboundTime, "label2": "Average Talk Time", "value2": response.averageTalkOutboundTime });
                    this.onQueueReportsOutboundChanged.next(this.queueReportsOutbound);
                    resolve(response);
                }, reject);
        });
    }

    cleanup() {
        this.queueReports = [];
        this.queueReportsInbound = [];
        this.queueReportsOutbound = [];
        this.onQueueReportsChanged.next(null);
        this.onQueueReportsInboundChanged.next(null);
        this.onQueueReportsOutboundChanged.next(null);
    }
}
