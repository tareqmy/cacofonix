export class Company {
    id: number;
    companyId: string;
    companyName: string;
    description: string;
    enabled: boolean;
    zoneId: string;
    email: string;
    phone: string;
    notes: string;

    /**
     * Constructor
     *
     * @param company
     */
    constructor(company) {
        this.id = company.id || '';
        this.companyId = company.companyId || '';
        this.companyName = company.companyName || '';
        this.description = company.description || '';
        this.enabled = company.enabled;
        this.zoneId = company.zoneId || 'Asia/Dhaka';
        this.email = company.email || '';
        this.phone = company.phone || '';
        this.notes = company.notes || '';
    }
}
