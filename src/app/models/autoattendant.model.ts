export class AutoAttendant {
    id: number;
    autoAttendantId: string;
    autoAttendantName: string;
    companyId: number;
    companyName: string;
    extensionId: number;
    baseExtension: string;
    promptUploaded: boolean;
    optionsMap: any;

    /**
     * Constructor
     *
     * @param autoAttendant
     */
    constructor(autoAttendant) {
        this.id = autoAttendant.id || '';
        this.autoAttendantId = autoAttendant.autoAttendantId || '';
        this.autoAttendantName = autoAttendant.autoAttendantName || '';
        this.companyId = autoAttendant.companyId;
        this.companyName = autoAttendant.companyName;
        this.extensionId = autoAttendant.extensionId;
        this.baseExtension = autoAttendant.baseExtension;
        this.promptUploaded = autoAttendant.promptUploaded;
        this.optionsMap = autoAttendant.optionsMap;
    }
}
