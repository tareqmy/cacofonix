import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { AudioContext } from 'angular-audio-context';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

import { VoiceMailMessagesService } from 'app/services/voicemailmessages.service';
import { AccountService } from 'app/services/account.service';
import { CommonService } from 'app/services/common.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'voicemailmessages-list',
    templateUrl: './voicemailmessages.component.html',
    styleUrls: ['./voicemailmessages.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class VoiceMailMessagesComponent implements OnInit, OnDestroy {

    dataSource: FilesDataSource | null;
    displayedColumns = ['dir', 'msgNum', 'callerId', 'origTime', 'duration', 'actions'];

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild('filter', { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    selectedExtension: any;
    extensions: any;

    audioSource: any;
    msgId: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {VoiceMailMessagesService} _voiceMailMessagesService
     * @param {AccountService} _accountService
     * @param {VoiceMailMessagesService} _commonService
     * @param {MatDialog} _matDialog
     * @param {AudioContext} _audioContext
     */
    constructor(
        private _voiceMailMessagesService: VoiceMailMessagesService,
        private _accountService: AccountService,
        private _commonService: CommonService,
        private _matDialog: MatDialog,
        private _audioContext: AudioContext
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this.audioSource = null;
        this.msgId = null;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.extensions = [];
        this.dataSource = new FilesDataSource(this._voiceMailMessagesService, this.paginator, this.sort);
        this.getExtensions();
        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    getExtensions(): void {
        this._accountService.getExtensions().then((extensions) => {
            this.extensions = extensions;
            if (this.extensions && this.extensions.length > 0) {
                this.selectedExtension = extensions[0].id;
                this._voiceMailMessagesService.getVoicemailMessages(this.selectedExtension);
            }
        });
    }

    isExtensionSelected(): boolean {
        return !_.isNil(this.selectedExtension);
    }

    refresh(): void {
        this._voiceMailMessagesService.getVoicemailMessages(this.selectedExtension);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.extensions = [];
        this.selectedExtension = null;
        this._voiceMailMessagesService.cleanup();
        this.stopAudio();
    }

    download(msgId): void {
        this._voiceMailMessagesService.downloadVoicemailMessage(this.selectedExtension, msgId);
    }

    play(msgId): void {
        this.stopAudio();
        this.msgId = msgId;
        this._voiceMailMessagesService.playVoicemailMessage(this.selectedExtension, msgId)
            .then((result) => {
                this.playAudio(result);
            });
    }

    isPlaying(msgId): boolean {
        return msgId === this.msgId;
    }

    playAudio(arrayData): void {
        this.audioSource = this._audioContext.createBufferSource();
        let destination = this._audioContext.destination;
        let as = this.audioSource;
        this._audioContext.decodeAudioData(arrayData,
            function(buffer) {
                as.buffer = buffer;
                as.connect(destination);
                as.start(0);
            }
        );
    }

    stopAudio(): void {
        if (this.audioSource) {
            this.audioSource.stop();
        }
        this.audioSource = null;
        this.msgId = null;
    }

    callback(msgId): void {
        this._voiceMailMessagesService.callbackVoicemailMessage(this.selectedExtension, msgId);
    }

    delete(msgId): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete this message?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {

                this._voiceMailMessagesService.deleteVoicemailMessage(this.selectedExtension, msgId)
                    .then(() => {

                        // Show the success message
                        this._commonService.showSnackBar('Message deleted');

                        this._voiceMailMessagesService.getVoicemailMessages(this.selectedExtension);
                    });
            }
            this.confirmDialogRef = null;
        });

    }
}

export class FilesDataSource extends DataSource<any>
{
    // Private
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {VoiceMailMessagesService} _voiceMailMessagesService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _voiceMailMessagesService: VoiceMailMessagesService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._voiceMailMessagesService.voiceMailMessages;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._voiceMailMessagesService.onVoiceMailMessagesChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(map(() => {

            let data = this._voiceMailMessagesService.voiceMailMessages.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
            return data.splice(startIndex, this._matPaginator.pageSize);
        })
        );

    }

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'dir':
                    [propertyA, propertyB] = [a.dir, b.dir];
                    break;
                case 'msgNum':
                    [propertyA, propertyB] = [a.msgNum, b.msgNum];
                    break;
                case 'callerId':
                    [propertyA, propertyB] = [a.callerId, b.callerId];
                    break;
                case 'origTime':
                    [propertyA, propertyB] = [a.origTime, b.origTime];
                    break;
                case 'duration':
                    [propertyA, propertyB] = [a.duration, b.duration];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
