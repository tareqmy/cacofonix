import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { PhoneNumber } from 'app/models/phonenumber.model';
import { PhoneNumberService } from 'app/services/phonenumber.service';
import { CommonService } from 'app/services/common.service';
import { ProvidersService } from 'app/services/providers.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'phonenumber',
    templateUrl: './phonenumber.component.html',
    styleUrls: ['./phonenumber.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PhoneNumberComponent implements OnInit, OnDestroy {
    phoneNumber: PhoneNumber;
    pageType: string;
    isSuperAdmin: boolean;
    phoneNumberForm: FormGroup;

    providers: any;
    companies: any;
    extensions: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {PhoneNumberService} _phoneNumberService
     * @param {CommonService} _commonService
     * @param {ProvidersService} _providersService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _phoneNumberService: PhoneNumberService,
        private _commonService: CommonService,
        private _providersService: ProvidersService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.phoneNumber = new PhoneNumber({});

        this.providers = [];

        this.companies = [];

        this.extensions = [];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update phoneNumber on changes
        this._phoneNumberService.onPhoneNumberChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(phoneNumber => {

                if (phoneNumber) {
                    this.phoneNumber = new PhoneNumber(phoneNumber);
                    this.pageType = 'edit';
                    this.phoneNumberForm = this.editPhoneNumberForm();
                    this._companiesService.getExtensions(this.phoneNumber.companyId).then((extensions) => {
                        this.extensions = extensions;
                    });
                }
                else {
                    this.pageType = 'new';
                    this.phoneNumber = new PhoneNumber({});
                    this.phoneNumberForm = this.createPhoneNumberForm();
                }
            });

        if (this.isSuperAdmin) {
          this._providersService.getProviders().then((providers) => {
              this.providers = providers;
          });
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.phoneNumberForm.controls; }

    /**
     * Create phoneNumber form
     *
     * @returns {FormGroup}
     */
    createPhoneNumberForm(): FormGroup {
        return this._formBuilder.group({
            number: [this.phoneNumber.number, [Validators.required]],
            password: [this.phoneNumber.password, [Validators.required, Validators.minLength(8)]],
            providerId: [this.phoneNumber.providerId, [Validators.required]],
            providerName: [this.phoneNumber.providerName],
            companyId: [this.phoneNumber.companyId, [Validators.required]],
            companyName: [this.phoneNumber.companyName],
            extensionId: [this.phoneNumber.extensionId, [Validators.required]],
            baseExtension: [this.phoneNumber.baseExtension]
        });
    }

    /**
     * Create phoneNumber form
     *
     * @returns {FormGroup}
     */
    editPhoneNumberForm(): FormGroup {
        return this._formBuilder.group({
          number: [{ value: this.phoneNumber.number, disabled: true }],
          password: [this.phoneNumber.password, [Validators.required, Validators.minLength(8)]],
          providerId: [{ value: this.phoneNumber.providerId, disabled: true }],
          providerName: [{ value: this.phoneNumber.providerName, disabled: true }],
          companyId: [{ value: this.phoneNumber.companyId, disabled: true }],
          companyName: [{ value: this.phoneNumber.companyName, disabled: true }],
          extensionId: [this.phoneNumber.extensionId, [Validators.required]],
          baseExtension: [this.phoneNumber.baseExtension]
        });
    }

    getExtensions(event): void {
        this._companiesService.getExtensions(this.phoneNumber.companyId).then((extensions) => {
            this.extensions = extensions;
            this.f.extensionId.setValue(null);
        });
    }

    /**
     * Save phoneNumber
     */
    savePhoneNumber(): void {
        const data = this.phoneNumberForm.getRawValue();

        this._phoneNumberService.savePhoneNumber(data)
            .then(() => {

                // Trigger the subscription with new data
                this._phoneNumberService.onPhoneNumberChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Phone Number saved');
            });
    }

    /**
     * Add phoneNumber
     */
    addPhoneNumber(): void {
        const data = this.phoneNumberForm.getRawValue();

        this._phoneNumberService.addPhoneNumber(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._phoneNumberService.onPhoneNumberChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Phone Number added');

                // Change the location with new one
                this._router.navigate(['/settings/phonenumbers/' + this.phoneNumber.number]);
            });
    }


    /**
     * Delete phoneNumber
     */
    deletePhoneNumber(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.phoneNumberForm.getRawValue();

                this._phoneNumberService.deletePhoneNumber(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._phoneNumberService.onPhoneNumberChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Phone Number deleted');

                        // Change the location to phoneNumbers table
                        this._router.navigate(['/settings/phonenumbers']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
