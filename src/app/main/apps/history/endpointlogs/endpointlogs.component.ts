import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { UsersService } from 'app/services/users.service';
import { EndpointLogsService } from 'app/services/endpointlogs.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'endpointlogs-list',
    templateUrl: './endpointlogs.component.html',
    styleUrls: ['./endpointlogs.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class EndpointLogsComponent implements OnInit, OnDestroy {

    dataSource: FilesDataSource | null;
    displayedColumns = ['resource', 'eventDateTime', 'endpointState', 'deviceState'];

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild('filter', { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    selectedAgent: any;
    agents: any;

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {EndpointLogsService} _endpointLogsService
     * @param {UsersService} _usersService
     */
    constructor(
        private _endpointLogsService: EndpointLogsService,
        private _usersService: UsersService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._endpointLogsService, this.paginator, this.sort);
        this.getAgents();
        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    getAgents(): void {
        this._usersService.getUsers().then((agents) => {
            this.agents = agents;
            if (this.agents && this.agents.length > 0) {
                this.selectedAgent = agents[0].id;
                this._endpointLogsService.getEndpointLogs(this.selectedAgent, this.startDate, this.endDate);
            }
        });
    }

    isAgentSelected(): boolean {
        return !_.isNil(this.selectedAgent);
    }

    refresh(): void {
        this._endpointLogsService.getEndpointLogs(this.selectedAgent, this.startDate, this.endDate);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.agents = [];
        this.selectedAgent = null;
        this._endpointLogsService.cleanup();
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._endpointLogsService.getEndpointLogs(this.selectedAgent, this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._endpointLogsService.getEndpointLogs(this.selectedAgent, this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }
}

export class FilesDataSource extends DataSource<any>
{
    // Private
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {EndpointLogsService} _endpointLogsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _endpointLogsService: EndpointLogsService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._endpointLogsService.endpointLogs;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._endpointLogsService.onEndpointLogsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(map(() => {

            let data = this._endpointLogsService.endpointLogs.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
            return data.splice(startIndex, this._matPaginator.pageSize);
        })
        );

    }

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'resource':
                    [propertyA, propertyB] = [a.resource, b.resource];
                    break;
                case 'eventDateTime':
                    [propertyA, propertyB] = [a.eventDateTime, b.eventDateTime];
                    break;
                case 'endpointState':
                    [propertyA, propertyB] = [a.endpointState, b.endpointState];
                    break;
                case 'deviceState':
                    [propertyA, propertyB] = [a.deviceState, b.deviceState];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
