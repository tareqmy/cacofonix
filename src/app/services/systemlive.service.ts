import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { SystemLive } from 'app/models/systemlive.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class SystemLiveService implements Resolve<any> {

    eventSource: EventSourcePolyfill;
    systemLive: SystemLive;
    onSystemLiveChanged: BehaviorSubject<any>;
    onSystemChannelLiveChanged: BehaviorSubject<any>;
    onSystemEndpointLiveChanged: BehaviorSubject<any>;
    interval: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onSystemLiveChanged = new BehaviorSubject({});
        this.onSystemChannelLiveChanged = new BehaviorSubject({});
        this.onSystemEndpointLiveChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getSystemLive()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get systemLive
     *
     */
    getSystemLive() {
        this.createInterval();

        let jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken && jwtoken.token) {
            this.eventSource = new EventSourcePolyfill(`${environment.settings.backend}/api/sse/system/live`,
                {
                    headers: {
                        Authorization: `Bearer ${jwtoken.token}`
                    }
                });

            // this.eventSource.onopen = (event: any) => console.log('open', event);

            this.eventSource.onmessage = (event: any) => {
                this.systemLive = new SystemLive(JSON.parse(event.data));
                this.onSystemLiveChanged.next(this.systemLive);
                this.onSystemChannelLiveChanged.next(this.systemLive.channels);
                this.onSystemEndpointLiveChanged.next(this.systemLive.endpoints);
            };

            // this.eventSource.onerror = (event: any) => console.log('error', event);
        }
    }

    createInterval() {
        this.deleteInterval();
        this.interval = setInterval(() => {
            //note: checked duration is increasing
            if (this.systemLive && this.systemLive.channels) {
                this.systemLive.channels.forEach(channel => {
                    channel.duration = channel.duration + 1;
                });
            }
        }, 1000);
    }

    deleteInterval() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    closeEvents() {
        this.systemLive = null;
        this.onSystemLiveChanged.next(null);
        this.onSystemChannelLiveChanged.next(null);
        this.onSystemEndpointLiveChanged.next(null);
        this.eventSource.close();
        this.deleteInterval();
    }
}
