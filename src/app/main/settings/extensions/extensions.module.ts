import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { ExtensionComponent } from './extension/extension.component';
import { ExtensionService } from 'app/services/extension.service';
import { ExtensionsComponent } from './extensions/extensions.component';
import { ExtensionsService } from 'app/services/extensions.service';
import { VoiceMailComponent } from './voicemail/voicemail.component';
import { VoiceMailService } from 'app/services/voicemail.service';

const routes = [
    {
        path: ':id',
        component: ExtensionComponent,
        resolve: {
            data: ExtensionService
        }
    },
    {
        path: ':id/voicemail/:voiceMailId',
        component: VoiceMailComponent,
        resolve: {
            data: VoiceMailService
        }
    },
    {
        path: '',
        component: ExtensionsComponent,
        resolve: {
            data: ExtensionsService
        }
    },
];

@NgModule({
    declarations: [
        ExtensionsComponent,
        ExtensionComponent,
        VoiceMailComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        ExtensionsService,
        ExtensionService,
        VoiceMailService
    ],
    entryComponents: [
    ]
})
export class ExtensionsModule { }
