import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

import { SystemLiveService } from 'app/services/systemlive.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'systemlive-list',
    templateUrl: './systemlive.component.html',
    styleUrls: ['./systemlive.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class SystemLiveComponent implements OnInit, OnDestroy {

    channelDataSource: ChannelLiveDataSource | null;
    endpointDataSource: EndpointlLiveDataSource | null;
    displayedChannelColumns = ['uniqueID', 'callerIdName', 'callerIdNum', 'destination', 'startTime', 'duration', 'direction', 'callType'];
    displayedEndpointColumns = ['callerId', 'resource', 'state', 'peerStatus'];

    /**
     * Constructor
     *
     * @param {SystemLiveService} _systemLiveService
     */
    constructor(
        private _systemLiveService: SystemLiveService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.channelDataSource = new ChannelLiveDataSource(this._systemLiveService);
        this.endpointDataSource = new EndpointlLiveDataSource(this._systemLiveService);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        this._systemLiveService.closeEvents();
    }
}

export class ChannelLiveDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {SystemLiveService} _systemLiveService
     */
    constructor(
        private _systemLiveService: SystemLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._systemLiveService.onSystemChannelLiveChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class EndpointlLiveDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {SystemLiveService} _systemLiveService
     */
    constructor(
        private _systemLiveService: SystemLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._systemLiveService.onSystemEndpointLiveChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
