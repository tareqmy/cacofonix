import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

import { MustMatch, MustNotMatch } from 'app/services/custom-validation';
import { ChangePassword } from 'app/models/user.model';

@Component({
    selector: 'account-password-change-form-dialog',
    templateUrl: './password-change-form.component.html',
    styleUrls: ['./password-change-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AccountPasswordChangeFormDialogComponent {

    changePassword: ChangePassword;
    changePasswordForm: FormGroup;
    dialogTitle: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<AccountPasswordChangeFormDialogComponent>} matDialogRef
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<AccountPasswordChangeFormDialogComponent>,
        private _formBuilder: FormBuilder
    ) {
        this.dialogTitle = 'Change Password';
        this.changePasswordForm = this.getChangePasswordForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.changePasswordForm.controls; }

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    getChangePasswordForm(): FormGroup {
        return this._formBuilder.group({
            oldPassword: ['', [Validators.required]],
            newPassword: ['', [Validators.required, Validators.minLength(8)]],
            confirmPassword: ['', [Validators.required]]
        },
            {
                validator: [MustMatch('newPassword', 'confirmPassword'), MustNotMatch('oldPassword', 'newPassword')]
            });
    }
}
