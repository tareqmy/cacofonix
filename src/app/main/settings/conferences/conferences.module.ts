import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { ConferenceComponent } from './conference/conference.component';
import { ConferenceService } from 'app/services/conference.service';
import { ConferencesComponent } from './conferences/conferences.component';
import { ConferencesService } from 'app/services/conferences.service';

const routes = [
    {
        path: ':id',
        component: ConferenceComponent,
        resolve: {
            data: ConferenceService
        }
    },
    {
        path: '',
        component: ConferencesComponent,
        resolve: {
            data: ConferencesService
        }
    },
];

@NgModule({
    declarations: [
        ConferencesComponent,
        ConferenceComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        ConferencesService,
        ConferenceService
    ],
    entryComponents: [
    ]
})
export class ConferencesModule { }
