export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SETTINGS': 'Settings',
            'DASHBOARD': {
                'TITLE': 'Dashboard'
            },
            'CHANNELLOGS': {
                'TITLE': 'Channel Logs'
            },
            'HISTORY': {
                'TITLE': 'History'
            },
            'REPORT': {
                'TITLE': 'Report'
            },
            'VOICEMAILMESSAGES': {
                'TITLE': 'Voice Mail Messages'
            },
            'CALLCENTERLIVE': {
                'TITLE': 'Call Center Live'
            },
            'SYSTEMLIVE': {
                'TITLE': 'System Live'
            },
            'PROVIDERS': {
                'TITLE': 'Providers'
            },
            'COMPANIES': {
                'TITLE': 'Companies'
            },
            'USERS': {
                'TITLE': 'Users'
            },
            'EXTENSIONS': {
                'TITLE': 'Extensions'
                //     'BADGE': '25'
            },
            'PHONES': {
                'TITLE': 'Phones'
            },
            'PHONENUMBERS': {
                'TITLE': 'Phone Numbers'
            },
            'QUEUES': {
                'TITLE': 'Queues'
            },
            'AUTO_ATTENDANTS': {
                'TITLE': 'Auto Attendants'
            },
            'CONFERENCES': {
                'TITLE': 'Conferences'
            }
        }
    }
};
