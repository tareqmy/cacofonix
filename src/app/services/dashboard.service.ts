import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'environments/environment';

import { PieReport } from 'app/models/piereport.model'
import { ChartReport } from 'app/models/chartreport.model'

@Injectable()
export class DashboardService
{
    countBarReport: ChartReport;
    onCountBarReportChanged: BehaviorSubject<any>;

    durationLineReport: ChartReport;
    onDurationLineReportChanged: BehaviorSubject<any>;

    countPieReport: PieReport;
    onCountPieReportChanged: BehaviorSubject<any>;

    durationPieReport: PieReport;
    onDurationPieReportChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.onCountBarReportChanged = new BehaviorSubject({});
        this.onCountPieReportChanged = new BehaviorSubject({});
        this.onDurationLineReportChanged = new BehaviorSubject({});
        this.onDurationPieReportChanged = new BehaviorSubject({});
    }

    /**
     * Get Reports
     *
     * @returns {Promise<any>}
     */
    getExtensionReports(extensionId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/accounts/reports/extensions/${extensionId}`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.calculatePieCount(response);
                    this.calculatePieDuration(response);
                    this.calculateLineDuration(response);
                    this.calculateBarCount(response);
                    resolve(response);
                }, reject);
        });
    }

    calculateBarCount(response) {
        this.countBarReport = new ChartReport({});
        var incomingData = [];
        var outgoingData = [];
        var labels = [];
        response.forEach(element => {
            labels.push(element.dayString);
            incomingData.push(element.incomingCallCount);
            outgoingData.push(element.outgoingCallCount);
        });
        this.countBarReport.labels = labels;
        this.countBarReport.dataSets = [
            { data: incomingData, label: 'Incoming Count' },
            { data: outgoingData, label: 'Outgoing Count' }
        ];
        this.onCountBarReportChanged.next(this.countBarReport);
    }

    calculateLineDuration(response) {
        this.durationLineReport = new ChartReport({});
        var incomingData = [];
        var outgoingData = [];
        var labels = [];
        response.forEach(element => {
            labels.push(element.dayString);
            incomingData.push(element.incomingCallDuration);
            outgoingData.push(element.outgoingCallDuration);
        });
        this.durationLineReport.labels = labels;
        this.durationLineReport.dataSets = [
            { data: incomingData, label: 'Incoming Duration' },
            { data: outgoingData, label: 'Outgoing Duration' }
        ];
        this.onDurationLineReportChanged.next(this.durationLineReport);
    }

    calculatePieCount(response) {
        this.countPieReport = new PieReport({});
        var totalIncomingCallCount = 0;
        var totalOutgoingCallCount = 0;
        response.forEach(element => {
            totalIncomingCallCount = totalIncomingCallCount + element.incomingCallCount;
            totalOutgoingCallCount = totalOutgoingCallCount + element.outgoingCallCount;
        });
        this.countPieReport.labels = ['Incoming Count', 'Outgoing Count'];
        this.countPieReport.data = [totalIncomingCallCount, totalOutgoingCallCount];
        this.onCountPieReportChanged.next(this.countPieReport);
    }

    calculatePieDuration(response) {
        this.durationPieReport = new PieReport({});
        var totalIncomingCallDuration = 0;
        var totalOutgoingCallDuration = 0;
        response.forEach(element => {
            totalIncomingCallDuration = totalIncomingCallDuration + element.incomingCallDuration;
            totalOutgoingCallDuration = totalOutgoingCallDuration + element.outgoingCallDuration;
        });
        this.durationPieReport.labels = ['Incoming Duration', 'Outgoing Duration'];
        this.durationPieReport.data = [totalIncomingCallDuration, totalOutgoingCallDuration];
        this.onDurationPieReportChanged.next(this.durationPieReport);
    }

    close() {
        this.countBarReport = null;
        this.countPieReport = null;
        this.durationLineReport = null;
        this.durationPieReport = null;
        this.onCountBarReportChanged.next(null);
        this.onCountPieReportChanged.next(null);
        this.onDurationLineReportChanged.next(null);
        this.onDurationPieReportChanged.next(null);
    }
}
