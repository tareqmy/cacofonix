FROM node:lts-alpine as development
LABEL creator="tareqmohammadyousuf"
LABEL email="tareq.y@gmail.com"

# creates the folder if doesnt exist
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm install

COPY . /app

EXPOSE 4200

# start app --- --disable-host-check if development needs to be accessed from another machine
CMD ng serve --host 0.0.0.0 --disable-host-check

#############
### build ###
#############

FROM node:lts-alpine as builder
# creates the folder if doesnt exist
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# copy installed application from development
COPY --from=development /app /app

# generate build
RUN ng build --prod --output-path=dist

############
### prod ###
############

# base image
FROM nginx:latest as production

# copy artifact build from the 'build environment'
COPY --from=builder /app/dist /usr/share/nginx/html

COPY entrypoint.sh /opt/
RUN chmod +x /opt/entrypoint.sh
COPY nginx.conf /etc/nginx/conf.d/default.conf

# expose port 80
EXPOSE 80

# run nginx
ENTRYPOINT ["/opt/entrypoint.sh"]
