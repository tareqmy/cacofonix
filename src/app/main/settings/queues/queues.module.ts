import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule,
    MatDividerModule
} from '@angular/material';

import { AudioContextModule } from 'angular-audio-context';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { QueueComponent } from './queue/queue.component';
import { QueueService } from 'app/services/queue.service';
import { QueuesComponent } from './queues/queues.component';
import { QueuesService } from 'app/services/queues.service';

const routes = [
    {
        path: ':id',
        component: QueueComponent,
        resolve: {
            data: QueueService
        }
    },
    {
        path: '',
        component: QueuesComponent,
        resolve: {
            data: QueuesService
        }
    },
];

@NgModule({
    declarations: [
        QueuesComponent,
        QueueComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,
        MatDividerModule,

        AudioContextModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        QueuesService,
        QueueService
    ],
    entryComponents: [
    ]
})
export class QueuesModule { }
