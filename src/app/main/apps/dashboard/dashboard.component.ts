import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { environment } from 'environments/environment';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as _ from 'lodash';

import { DashboardService } from 'app/services/dashboard.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { AccountService } from 'app/services/account.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DashboardComponent implements OnInit, OnDestroy {

    selected: any;
    chartPlugins = [pluginDataLabels];

    barChartOptions: ChartOptions = {
        responsive: true,
        // We use these empty structures as placeholders for dynamic theming.
        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };
    barChartLabels: Label[] = ['NA'];
    barChartData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    lineChartLabels: Label[] = ['NA'];
    lineChartData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    extensions: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {DashboardService} _dashboardService
     * @param {AuthenticationService} _authenticationService
     * @param {AccountService} _accountService
     */
    constructor(
        private _dashboardService: DashboardService,
        private _authenticationService: AuthenticationService,
        private _accountService: AccountService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.extensions = [];
        this.getExtensions();
        this._dashboardService.onCountBarReportChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(report => {
                if (report && JSON.stringify(report) !== '{}') {
                    this.barChartData = report.dataSets;
                    this.barChartLabels = report.labels;
                }
            });

        this._dashboardService.onDurationLineReportChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(report => {
                if (report && JSON.stringify(report) !== '{}') {
                    this.lineChartLabels = report.labels;
                    this.lineChartData = report.dataSets;
                }
            });
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._dashboardService.close();
    }

    refresh(): void {
        this._dashboardService.getExtensionReports(this.selected);
    }

    isExtensionSelected(): boolean {
        return !_.isNil(this.selected);
    }

    getExtensions(): void {
        this._accountService.getExtensions().then((extensions) => {
            if (extensions && extensions.length > 0) {
                this.selected = extensions[0].id;
                this._dashboardService.getExtensionReports(this.selected);
            }
            this.extensions = extensions;
        });
    }

    getReports(event): void {
        this._dashboardService.getExtensionReports(event.source.value);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
}
