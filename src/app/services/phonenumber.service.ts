import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class PhoneNumberService implements Resolve<any>
{
    routeParams: any;
    phoneNumber: any;
    onPhoneNumberChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onPhoneNumberChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPhoneNumber()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get phoneNumber
     *
     * @returns {Promise<any>}
     */
    getPhoneNumber(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onPhoneNumberChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/phonenumbers/${this.routeParams.id}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.phoneNumber = response;
                        this.onPhoneNumberChanged.next(this.phoneNumber);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save phoneNumber
     *
     * @param phoneNumber
     * @returns {Promise<any>}
     */
    savePhoneNumber(phoneNumber): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phonenumbers`;
            this._httpClient.put(requestUrl, phoneNumber, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add phoneNumber
     *
     * @param phoneNumber
     * @returns {Promise<any>}
     */
    addPhoneNumber(phoneNumber): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phonenumbers`;
            this._httpClient.post(requestUrl, phoneNumber, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete phoneNumber
     *
     * @param phoneNumber
     */
    deletePhoneNumber(phoneNumber): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phonenumbers/${phoneNumber.number}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
