import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { AgentLive } from 'app/models/agentlive.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class ChannelLiveService {

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService
    ) {
    }

    /**
     * transfer Channel
     *
     * @param channelId
     * @param userId
     * @returns {Promise<any>}
     */
    transferChannel(channelId, userId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/calls/transfer/${channelId}/${userId}/`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * escalate Channel
     *
     * @param channelId
     * @param userId
     * @returns {Promise<any>}
     */
    escalateChannel(channelId, userId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/calls/escalate/${channelId}/${userId}/`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * M2Q Channel
     *
     * @param channelId
     * @param queueId
     * @returns {Promise<any>}
     */
    m2qChannel(channelId, queueId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/calls/m2q/${channelId}/${queueId}/`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Hangup Channel
     *
     * @param channelId
     * @returns {Promise<any>}
     */
    hangupChannel(channelId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/calls/hangup/${channelId}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
