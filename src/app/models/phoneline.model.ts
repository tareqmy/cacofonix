export class PhoneLine {
    id: number;
    description: string;
    phoneId: number;
    phoneName: string;
    callerName: string;
    extensionId: number;
    baseExtension: string;
    sipUserName: string;
    sipPassword: string;
    callWaitingEnabled: boolean;

    /**
     * Constructor
     *
     * @param phoneLine
     */
    constructor(phoneLine) {
        this.id = phoneLine.id || '';
        this.description = phoneLine.description || '';
        this.phoneId = phoneLine.phoneId;
        this.phoneName = phoneLine.phoneName;
        this.callerName = phoneLine.callerName;
        this.extensionId = phoneLine.extensionId;
        this.baseExtension = phoneLine.baseExtension;
        this.sipUserName = phoneLine.sipUserName;
        this.sipPassword = phoneLine.sipPassword;
        this.callWaitingEnabled = phoneLine.callWaitingEnabled;
    }
}
