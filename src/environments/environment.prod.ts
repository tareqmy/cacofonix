export const environment = {
    production: true,
    hmr: false,
    settings: {
        backend: 'http://localhost:8080'
    }
};
