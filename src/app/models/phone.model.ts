export class Phone {
    id: number;
    phoneName: string;
    companyId: number;
    companyName: string;

    /**
     * Constructor
     *
     * @param phone
     */
    constructor(phone) {
        this.id = phone.id || '';
        this.phoneName = phone.phoneName || '';
        this.companyId = phone.companyId;
        this.companyName = phone.companyName;
    }
}
