import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';
import * as moment from 'moment';

import { ChannelLogsService } from 'app/services/channellogs.service';
import { AccountService } from 'app/services/account.service';
import { CommonService } from 'app/services/common.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'channellogs-list',
    templateUrl: './channellogs.component.html',
    styleUrls: ['./channellogs.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class ChannelLogsComponent implements OnInit, OnDestroy {

    dataSource: FilesDataSource | null;
    dataSummarySource: SummaryDataSource | null;
    displayedColumns = ['date', 'time', 'from', 'to', 'callerName', 'calledNumber', 'callType', 'status', 'duration'];
    displayedSummaryColumns = ['label1', 'value1', 'label2', 'value2'];

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild('filter', { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    selectedExtension: any;
    extensions: any;

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ChannelLogsService} _channelLogsService
     * @param {AccountService} _accountService
     * @param {ChannelLogsService} _commonService
     */
    constructor(
        private _channelLogsService: ChannelLogsService,
        private _accountService: AccountService,
        private _commonService: CommonService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.extensions = [];
        this.dataSource = new FilesDataSource(this._channelLogsService, this.paginator, this.sort);
        this.dataSummarySource = new SummaryDataSource(this._channelLogsService);
        this.getExtensions();
        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    getExtensions(): void {
        this._accountService.getExtensions().then((extensions) => {
            this.extensions = extensions;
            if (this.extensions && this.extensions.length > 0) {
                this.selectedExtension = extensions[0].id;
                this._channelLogsService.getChannelLogsForExtension(this.selectedExtension, this.startDate, this.endDate);
                this._channelLogsService.getChannelLogsSummaryForExtension(this.selectedExtension, this.startDate, this.endDate);
            }
        });
    }

    isExtensionSelected(): boolean {
        return !_.isNil(this.selectedExtension);
    }

    refresh(): void {
        this._channelLogsService.getChannelLogsForExtension(this.selectedExtension, this.startDate, this.endDate);
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._channelLogsService.getChannelLogsForExtension(this.selectedExtension, this.startDate, this.endDate);
        this._channelLogsService.getChannelLogsSummaryForExtension(this.selectedExtension, this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._channelLogsService.getChannelLogsForExtension(this.selectedExtension, this.startDate, this.endDate);
        this._channelLogsService.getChannelLogsSummaryForExtension(this.selectedExtension, this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }

    isInProgress(status): boolean {
        return status === "IN_PROGRESS";
    }

    isCallMade(status): boolean {
        return status === "CALL_MADE";
    }

    isCallReceived(status): boolean {
        return status === "CALL_RECEIVED";
    }

    isCallMissed(status, caller): boolean {
        return status === "CALL_MISSED" && !caller;
    }

    isCallMissedOutgoing(status, caller): boolean {
        return status === "CALL_MISSED" && caller;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.extensions = [];
        this.selectedExtension = null;
        this._channelLogsService.cleanup();
    }
}

export class FilesDataSource extends DataSource<any>
{
    // Private
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {ChannelLogsService} _channelLogsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _channelLogsService: ChannelLogsService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._channelLogsService.channelLogs;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._channelLogsService.onChannelLogsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(map(() => {

            let data = this._channelLogsService.channelLogs.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
            return data.splice(startIndex, this._matPaginator.pageSize);
        })
        );

    }

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'date':
                    [propertyA, propertyB] = [a.date, b.date];
                    break;
                case 'time':
                    [propertyA, propertyB] = [a.time, b.time];
                    break;
                case 'from':
                    [propertyA, propertyB] = [a.from, b.from];
                    break;
                case 'to':
                    [propertyA, propertyB] = [a.to, b.to];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class SummaryDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {ChannelLogsService} _channelLogsService
     */
    constructor(
        private _channelLogsService: ChannelLogsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._channelLogsService.onChannelLogsSummaryChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
