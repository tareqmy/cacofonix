export class VoiceMail {
    id: number;
    extensionId: number;
    baseExtension: string;
    enabled: boolean;
    fullName: string;
    email: string;

    /**
     * Constructor
     *
     * @param voicemail
     */
    constructor(voicemail) {
        this.id = voicemail.id;
        this.extensionId = voicemail.extensionId;
        this.baseExtension = voicemail.baseExtension;
        this.enabled = voicemail.enabled;
        this.fullName = voicemail.fullName;
        this.email = voicemail.email;
    }
}
