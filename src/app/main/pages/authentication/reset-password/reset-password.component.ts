import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

import { MustMatch } from 'app/services/custom-validation';
import { UserService } from 'app/services/user.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { CommonService } from 'app/services/common.service';

@Component({
    selector: 'reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    resetPasswordForm: FormGroup;
    email: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {AuthenticationService} _authenticationService
     * @param {UserService} _userService
     * @param {CommonService} _commonService
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _authenticationService: AuthenticationService,
        private _userService: UserService,
        private _commonService: CommonService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: false
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // redirect to home if already logged in
        if (this._authenticationService.currentTokenValue) {
            this._router.navigate(['/apps/dashboard']);
        }

        this._route.params.subscribe(params => {
            this.email = params['email'];
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.resetPasswordForm = this._formBuilder.group({
            email: [{ value: this.email, disabled: true }, [Validators.required, Validators.email]],
            token: ['', Validators.required],
            newPassword: ['', [Validators.required, Validators.minLength(8)]],
            confirmPassword: ['', Validators.required]
        },
            {
                validator: [MustMatch('newPassword', 'confirmPassword')]
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // convenience getter for easy access to form fields
    get f() { return this.resetPasswordForm.controls; }

    /**
     * Initiate password reset
     */
    initResetPassword(): void {
        this._userService.initResetPassword(this.email)
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Password Reset Email sent');
            });
    }

    /**
     * Initiate password reset
     */
    finishResetPassword(): void {
        const data = this.resetPasswordForm.getRawValue();

        this._userService.finishResetPassword(data)
            .then(() => {

                // Show the success message
                this._commonService.showSnackBar('Password Reset Successful');

                // Change the location with new one
                this._router.navigate(['/pages/auth/login/']);
            });
    }
}
