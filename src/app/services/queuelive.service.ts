import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { QueueLive } from 'app/models/queuelive.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class QueueLiveService implements Resolve<any> {

    routeParams: any;
    eventSource: EventSourcePolyfill;
    queueLive: QueueLive;

    queueLiveSummary: any[];
    onQueueLiveChanged: BehaviorSubject<any>;

    onQueueMessageLiveChanged: BehaviorSubject<any>;
    onQueueChannelLiveChanged: BehaviorSubject<any>;
    onQueueAgentLiveChanged: BehaviorSubject<any>;
    onQueueLiveSummaryChanged: BehaviorSubject<any>;
    interval: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onQueueLiveChanged = new BehaviorSubject({});
        this.onQueueMessageLiveChanged = new BehaviorSubject({});
        this.onQueueChannelLiveChanged = new BehaviorSubject({});
        this.onQueueAgentLiveChanged = new BehaviorSubject({});
        this.onQueueLiveSummaryChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getQueueLive()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get queueLive
     *
     */
    getQueueLive() {
        this.createInterval();

        let jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken && jwtoken.token) {
            this.eventSource = new EventSourcePolyfill(`${environment.settings.backend}/api/sse/queues/${this.routeParams.id}/live`,
                {
                    headers: {
                        Authorization: `Bearer ${jwtoken.token}`
                    }
                });

            // this.eventSource.onopen = (event: any) => console.log('open', event);

            this.eventSource.onmessage = (event: any) => {
                this.queueLive = new QueueLive(JSON.parse(event.data));
                this.onQueueLiveChanged.next(this.queueLive);
                this.onQueueMessageLiveChanged.next(this.queueLive.messages);
                this.onQueueChannelLiveChanged.next(this.queueLive.channels);
                this.onQueueAgentLiveChanged.next(this.queueLive.agents);
                this.queueLiveSummary = [];
                this.queueLiveSummary.push({ "label1": "Max", "value1": this.queueLive.max, "label2": "Weight", "value2": this.queueLive.weight });
                this.queueLiveSummary.push({ "label1": "Average Wait", "value1": this.queueLive.avgWait, "label2": "Max Wait", "value2": this.queueLive.maxWait });
                this.queueLiveSummary.push({ "label1": "Strategy", "value1": this.queueLive.strategy, "label2": "Service Level", "value2": this.queueLive.serviceLevel });
                this.queueLiveSummary.push({ "label1": "Talk Time", "value1": this.queueLive.talkTime, "label2": "Hold Time", "value2": this.queueLive.holdTime });
                this.queueLiveSummary.push({ "label1": "Completed", "value1": this.queueLive.completed, "label2": "Abandoned", "value2": this.queueLive.abandoned });
                this.queueLiveSummary.push({ "label1": "Agents", "value1": this.queueLive.agentsCount, "label2": "Channels", "value2": this.queueLive.channelsCount });
                this.onQueueLiveSummaryChanged.next(this.queueLiveSummary);
            };

            // this.eventSource.onerror = (event: any) => console.log('error', event);
        }
    }

    createInterval() {
        this.deleteInterval();
        this.interval = setInterval(() => {
            //note: checked duration is increasing
            if (this.queueLive) {
                if (this.queueLive.queuedChannelsCount > 0) {
                    var aWait = this.queueLive.avgWait;
                    var mWait = this.queueLive.maxWait;
                    var qCount = this.queueLive.queuedChannelsCount;
                    this.queueLive.avgWait = ((aWait * qCount) + qCount) / qCount;
                    this.queueLive.maxWait = mWait + 1;
                    this.queueLiveSummary[1].value1 = this.queueLive.avgWait;
                    this.queueLiveSummary[1].value2 = this.queueLive.maxWait;
                }

                if (this.queueLive.channels) {
                    this.queueLive.channels.forEach(channel => {
                        channel.duration = channel.duration + 1;
                        if (channel.queued) {
                            channel.wait = channel.wait + 1;
                        }
                    });
                }
            }
        }, 1000);
    }

    deleteInterval() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    closeEvents() {
        this.queueLive = null;
        this.onQueueLiveChanged.next(null);
        this.onQueueMessageLiveChanged.next(null);
        this.onQueueChannelLiveChanged.next(null);
        this.onQueueAgentLiveChanged.next(null);
        this.eventSource.close();
        this.deleteInterval();
    }

    /**
     * Join queue
     *
     * @param id
     * @returns {Promise<any>}
     */
    joinQueue(id): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/join`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Queue call
     *
     * @param id
     * @param destinationNumber
     * @returns {Promise<any>}
     */
    callQueue(id, destinationNumber): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/call/${destinationNumber}`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Leave queue
     *
     * @param id
     * @returns {Promise<any>}
     */
    leaveQueue(id): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/leave`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * kickout queue
     *
     * @param id
     * @param phoneLineId
     * @returns {Promise<any>}
     */
    kickOutQueue(id, phoneLineId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/kickout/${phoneLineId}`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Play voiceMailMessage
     *
     * @param id
     * @param msgId
     * @returns {Promise<any>}
     */
    playVoicemailMessage(id, msgId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/voicemails/messages/${msgId}/download`;
            this._httpClient.get(requestUrl, { responseType: "arraybuffer" })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Callback voiceMailMessage
     *
     * @returns {Promise<any>}
     */
    callbackVoicemailMessage(id, msgId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/voicemails/messages/${msgId}/callback`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete voiceMailMessage
     *
     * @param id
     * @param msgId
     */
    deleteVoicemailMessage(id, msgId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues/${id}/voicemails/messages/${msgId}`;
            this._httpClient.delete(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
