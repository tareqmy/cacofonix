import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { MessagesService } from 'app/services/messages.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {MessagesService} _messagesService
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _messagesService: MessagesService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken && jwtoken.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${jwtoken.token}`
                }
            });
        }

        return next.handle(request);
    }

    /** Log a JwtInterceptor message with the MessageService */
    private log(message: string) {
        this._messagesService.add(`JwtInterceptor: ${message}`);
    }
}
