import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'app/services/utility.service';

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    applicationInfo: any;

    /**
     * Constructor
     */
    constructor(private _utilityService: UtilityService) {
        this.applicationInfo = {};
    }

    /**
    * On init
    */
    ngOnInit(): void {
        this.applicationInfo = this._utilityService.applicationInfoValue;
    }
}
