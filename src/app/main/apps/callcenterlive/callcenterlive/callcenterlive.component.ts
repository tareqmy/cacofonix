import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef } from '@angular/material';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

import { SelectDialogComponent } from 'app/layout/components/select-dialog/select-dialog.component';
import { InputDialogComponent } from 'app/layout/components/input-dialog/input-dialog.component';

import { CommonService } from 'app/services/common.service';
import { AccountService } from 'app/services/account.service';
import { ExtensionService } from 'app/services/extension.service';
import { AgentLiveService } from 'app/services/agentlive.service';
import { QueueLiveService } from 'app/services/queuelive.service';
import { ChannelLiveService } from 'app/services/channellive.service';
import { CallCenterLiveService } from 'app/services/callcenterlive.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'callcenterlive-list',
    templateUrl: './callcenterlive.component.html',
    styleUrls: ['./callcenterlive.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class CallCenterLiveComponent implements OnInit, OnDestroy {

    queueDataSource: LiveQueueDataSource | null;
    displayedQueueColumns = ['queueName', 'baseExtension', 'agentStatusMap', 'max', 'weight',
        'queuedChannelsCount', 'channelsCount', 'messageCount', 'avgWait', 'maxWait', 'actions'];

    channelDataSource: LiveChannelDataSource | null;
    displayedChannelColumns = ['callerIdName', 'callerIdNum', 'destination', 'startTime', 'duration', 'direction', 'callType', 'answered', 'actions'];

    selectedExtension: any;
    selectedPhoneLine: any;
    selectedSipUserName: any;

    extensions: any;
    phoneLines: any;

    extension: any;
    sipUserName: any;

    isUser: boolean;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    /**
     * Constructor
     *
     * @param {CallCenterLiveService} _callCenterLiveService
     * @param {QueueLiveService} _queueLiveService
     * @param {AgentLiveService} _agentLiveService
     * @param {ChannelLiveService} _channelLiveService
     * @param {CommonService} _commonService
     * @param {AccountService} _accountService
     * @param {ExtensionService} _extensionService
     * @param {AuthenticationService} _authenticationService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _callCenterLiveService: CallCenterLiveService,
        private _queueLiveService: QueueLiveService,
        private _agentLiveService: AgentLiveService,
        private _channelLiveService: ChannelLiveService,
        private _commonService: CommonService,
        private _accountService: AccountService,
        private _extensionService: ExtensionService,
        private _authenticationService: AuthenticationService,
        private _matDialog: MatDialog
    ) {
        this.isUser = this._authenticationService.isUser();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.queueDataSource = new LiveQueueDataSource(this._callCenterLiveService);
        this.channelDataSource = new LiveChannelDataSource(this._agentLiveService);
        this.extensions = [];
        this.phoneLines = [];
        this.sipUserName = null;
        this.extension = null;
        this.getAgentStatus();
    }

    getAgentStatus(): void {
        this._agentLiveService.getAgentSipUserName().then((sipUserName) => {
            if (sipUserName) {
                this.sipUserName = sipUserName;
                this.extension = sipUserName.substr(-2);
                this._agentLiveService.getAgentLive();
            } else {
                this.sipUserName = null;
                this.extension = null;
            }
            this.getExtensions();
        });
    }

    getExtensions(): void {
        this._accountService.getExtensions().then((extensions) => {
            this.extensions = extensions;
            if (this.extensions && this.extensions.length > 0) {
                //todo: needs to be something fetched from server
                if (this.extension) {
                    var index = this.extensions.map(function(e) { return e.baseExtension; }).indexOf(this.extension);
                    this.selectedExtension = extensions[index].id;
                    this.selectedPhoneLine = extensions[index].phoneLineId;
                    this.selectedSipUserName = extensions[index].sipUserName;
                } else {
                    this.selectedExtension = extensions[0].id;
                    this.selectedPhoneLine = extensions[0].phoneLineId;
                    this.selectedSipUserName = extensions[0].sipUserName;
                }
            }
        });
    }

    extensionChanged(): void {
        var index = this.extensions.map(function(e) { return e.id; }).indexOf(this.selectedExtension);
        this.selectedPhoneLine = this.extensions[index].phoneLineId;
        this.selectedSipUserName = this.extensions[index].sipUserName;
    }

    isPhoneLineSelected(): boolean {
        return !_.isNil(this.selectedPhoneLine);
    }

    isJoinedCallCenter(): boolean {
        return !_.isNil(this.sipUserName);
    }

    isPaused(): boolean {
        if (this.isPhoneLineSelected() && this._agentLiveService.agentLive) {
            return this._agentLiveService.agentLive.paused;
        }
        return false;
    }

    getDeviceStatus(): string {
        if (this.isPhoneLineSelected() && this._agentLiveService.agentLive) {
            return this._agentLiveService.agentLive.deviceStatus;
        }
        return "UNKNOWN";
    }

    getEndpointStatus(): string {
        if (this.isPhoneLineSelected() && this._agentLiveService.agentLive) {
            return this._agentLiveService.agentLive.endpointStatus;
        }
        return "unknown";
    }

    isPersonalCall(callType): boolean {
        return callType === "Personal";
    }

    isJoinedAnyQueue(): boolean {
        if (this._agentLiveService.agentLive) {
            return _.keys(this._agentLiveService.agentLive.statusMap).length > 0;
        }
        return false;
    }

    isJoinedQueue(id): boolean {
        return !_.isNil(this.getQueueAgentStatus(id));
    }

    getQueueAgentStatus(id): string {
        return this._callCenterLiveService.queueLives[id].agentStatusMap[this.sipUserName];
    }

    getAgentCount(agentStatusMap): number {
        return _.keys(agentStatusMap).length;
    }

    /**
     * Join callcenter
     */
    joinCallCenter(): void {
        this._callCenterLiveService.joinCallCenter(this.selectedPhoneLine)
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Joined callcenter');

                this.getAgentStatus();
            });
    }

    /**
     * UnJoin callcenter
     */
    leaveCallCenter(): void {
        this._callCenterLiveService.leaveCallCenter()
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Left callcenter');
                this.sipUserName = null;
                this.extension = null;
                this._agentLiveService.closeEvents(true);
            });
    }

    /**
     * Pause agent
     */
    pauseAgent(): void {
        this._callCenterLiveService.pauseAgent()
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Agent paused');
            });
    }

    /**
     * Resume agent
     */
    resumeAgent(): void {
        this._callCenterLiveService.resumeAgent()
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Agent resumed');
            });
    }

    transferChannel(channelId): void {

        this._callCenterLiveService.getAgentsMap()
            .then((result) => {
                const dialogRef = this._matDialog.open(SelectDialogComponent, {
                    width: '250px',
                    data: result
                });
                dialogRef.componentInstance.message = 'Please select the agent';
                dialogRef.componentInstance.label = 'Agents';
                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        this._channelLiveService.transferChannel(channelId, result)
                            .then((result) => {
                                // Show the success message
                                this._commonService.showSnackBar('Channel transferred to agent!');
                            });
                    }
                });
            });

    }

    escalateChannel(channelId): void {

        this._callCenterLiveService.getSupervisorsMap()
            .then((result) => {
                const dialogRef = this._matDialog.open(SelectDialogComponent, {
                    width: '250px',
                    data: result
                });
                dialogRef.componentInstance.message = 'Please select the supervisor';
                dialogRef.componentInstance.label = 'Supervisors';
                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        this._channelLiveService.escalateChannel(channelId, result)
                            .then((result) => {
                                // Show the success message
                                this._commonService.showSnackBar('Channel escalated!');
                            });
                    }
                });
            });

    }

    m2qChannel(channelId): void {

        this._callCenterLiveService.getQueuesMap()
            .then((result) => {
                const dialogRef = this._matDialog.open(SelectDialogComponent, {
                    width: '250px',
                    data: result
                });

                dialogRef.componentInstance.message = 'Please select the queue';
                dialogRef.componentInstance.label = 'Queues';
                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        this._channelLiveService.m2qChannel(channelId, result)
                            .then((result) => {
                                // Show the success message
                                this._commonService.showSnackBar('Channel transferred to queue!');
                            });
                    }
                });
            });

    }

    hangupChannel(channelId): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to hangup this call?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._channelLiveService.hangupChannel(channelId)
                    .then((result) => {
                        // Show the success message
                        this._commonService.showSnackBar('Channel Hung Up!');
                    });
            }
            this.confirmDialogRef = null;
        });
    }

    /**
     * Join queue
     */
    joinQueue(id): void {
        this._queueLiveService.joinQueue(id)
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Joined queue');
            });
    }

    /**
     * Queue call
     */
    callQueue(id): void {
        const dialogRef = this._matDialog.open(InputDialogComponent, {
            width: '250px'
        });

        dialogRef.componentInstance.message = 'Please enter the destination.';
        dialogRef.componentInstance.label = 'Number';
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._queueLiveService.callQueue(id, result)
                    .then((result) => {
                        // Show the success message
                        this._commonService.showSnackBar('Calling...');
                    });
            }
        });
    }

    /**
     * Unjoin queue
     */
    leaveQueue(id): void {
        this._queueLiveService.leaveQueue(id)
            .then((result) => {
                // Show the success message
                this._commonService.showSnackBar('Left queue');
            });
    }

    getRouterLink(id): string {
        if (this.isUser) {
            return "/apps/callcenterlive";
        }
        return "/apps/queuelive/" + id;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        this._callCenterLiveService.closeEvents();
        this._agentLiveService.closeEvents(true);
    }
}

export class LiveQueueDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {CallCenterLiveService} _callCenterLiveService
     */
    constructor(
        private _callCenterLiveService: CallCenterLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._callCenterLiveService.onQueueLivesChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class LiveChannelDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {AgentLiveService} _agentLiveService
     */
    constructor(
        private _agentLiveService: AgentLiveService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._agentLiveService.onChannelLiveChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
