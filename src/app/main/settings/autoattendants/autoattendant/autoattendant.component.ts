import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AudioContext } from 'angular-audio-context';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { AutoAttendant } from 'app/models/autoattendant.model';
import { AutoAttendantService } from 'app/services/autoattendant.service';
import { CommonService } from 'app/services/common.service';
import { ProvidersService } from 'app/services/providers.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'autoattendant',
    templateUrl: './autoattendant.component.html',
    styleUrls: ['./autoattendant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AutoAttendantComponent implements OnInit, OnDestroy {
    autoAttendant: AutoAttendant;
    pageType: string;
    isSuperAdmin: boolean;
    autoAttendantForm: FormGroup;
    fileToUpload: File;

    providers: any;
    companies: any;
    extensions: any;

    audioSource: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AutoAttendantService} _autoAttendantService
     * @param {CommonService} _commonService
     * @param {ProvidersService} _providersService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     * @param {AudioContext} _audioContext
     */
    constructor(
        private _autoAttendantService: AutoAttendantService,
        private _commonService: CommonService,
        private _providersService: ProvidersService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router,
        private _audioContext: AudioContext
    ) {
        // Set the default
        this.autoAttendant = new AutoAttendant({});

        this.providers = [];

        this.companies = [];

        this.extensions = [];

        this.audioSource = null;

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update autoAttendant on changes
        this._autoAttendantService.onAutoAttendantChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(autoAttendant => {

                if (autoAttendant) {
                    this.autoAttendant = new AutoAttendant(autoAttendant);
                    this.pageType = 'edit';
                    this.autoAttendantForm = this.editAutoAttendantForm();
                    this._companiesService.getExtensions(this.autoAttendant.companyId).then((extensions) => {
                        this.extensions = extensions;
                    });
                }
                else {
                    this.pageType = 'new';
                    this.autoAttendant = new AutoAttendant({});
                    this.autoAttendantForm = this.createAutoAttendantForm();
                }
            });

        if (this.isSuperAdmin) {
            this._providersService.getProviders().then((providers) => {
                this.providers = providers;
            });
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.stopAudio();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.autoAttendantForm.controls; }

    /**
     * Create autoAttendant form
     *
     * @returns {FormGroup}
     */
    createAutoAttendantForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.autoAttendant.id],
            autoAttendantId: [this.autoAttendant.autoAttendantId, [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z][a-zA-Z0-9]+')]],
            autoAttendantName: [this.autoAttendant.autoAttendantName, [Validators.required]],
            companyId: [this.autoAttendant.companyId, [Validators.required]],
            companyName: [this.autoAttendant.companyName],
            extensionId: [this.autoAttendant.extensionId, [Validators.required]],
            baseExtension: [this.autoAttendant.baseExtension],
            optionsMap: this._formBuilder.group({
                1: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                2: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                3: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                4: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                5: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                6: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                7: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                8: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                9: ["", [Validators.pattern('\\*?[0-9]{2}')]],
                0: ["", [Validators.pattern('\\*?[0-9]{2}')]]
            })
        });
    }

    /**
     * Create autoAttendant form
     *
     * @returns {FormGroup}
     */
    editAutoAttendantForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.autoAttendant.id],
            autoAttendantId: [{ value: this.autoAttendant.autoAttendantId, disabled: true }, [Validators.required]],
            autoAttendantName: [this.autoAttendant.autoAttendantName, [Validators.required]],
            companyId: [{ value: this.autoAttendant.companyId, disabled: true }],
            companyName: [{ value: this.autoAttendant.companyName, disabled: true }],
            extensionId: [this.autoAttendant.extensionId, [Validators.required]],
            baseExtension: [this.autoAttendant.baseExtension],
            optionsMap: this._formBuilder.group({
                1: [this.autoAttendant.optionsMap[1], [Validators.pattern('\\*?[0-9]{2}')]],
                2: [this.autoAttendant.optionsMap[2], [Validators.pattern('\\*?[0-9]{2}')]],
                3: [this.autoAttendant.optionsMap[3], [Validators.pattern('\\*?[0-9]{2}')]],
                4: [this.autoAttendant.optionsMap[4], [Validators.pattern('\\*?[0-9]{2}')]],
                5: [this.autoAttendant.optionsMap[5], [Validators.pattern('\\*?[0-9]{2}')]],
                6: [this.autoAttendant.optionsMap[6], [Validators.pattern('\\*?[0-9]{2}')]],
                7: [this.autoAttendant.optionsMap[7], [Validators.pattern('\\*?[0-9]{2}')]],
                8: [this.autoAttendant.optionsMap[8], [Validators.pattern('\\*?[0-9]{2}')]],
                9: [this.autoAttendant.optionsMap[9], [Validators.pattern('\\*?[0-9]{2}')]],
                0: [this.autoAttendant.optionsMap[0], [Validators.pattern('\\*?[0-9]{2}')]]
            })
        });
    }

    getExtensions(event): void {
        this._companiesService.getExtensions(this.autoAttendant.companyId).then((extensions) => {
            this.extensions = extensions;
            this.f.extensionId.setValue(null);
        });
    }

    /**
     * Save autoAttendant
     */
    saveAutoAttendant(): void {
        const data = this.autoAttendantForm.getRawValue();

        this._autoAttendantService.saveAutoAttendant(data)
            .then(() => {

                // Trigger the subscription with new data
                this._autoAttendantService.onAutoAttendantChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Auto Attendant saved');
            });
    }

    /**
     * Add autoAttendant
     */
    addAutoAttendant(): void {
        const data = this.autoAttendantForm.getRawValue();

        this._autoAttendantService.addAutoAttendant(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._autoAttendantService.onAutoAttendantChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Auto Attendant added');

                // Change the location with new one
                this._router.navigate(['/settings/autoattendants/' + this.autoAttendant.id]);
            });
    }


    /**
     * Delete autoAttendant
     */
    deleteAutoAttendant(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.autoAttendantForm.getRawValue();

                this._autoAttendantService.deleteAutoAttendant(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._autoAttendantService.onAutoAttendantChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Auto Attendant deleted');

                        // Change the location to autoAttendants table
                        this._router.navigate(['/settings/autoattendants']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Upload autoAttendant prompt file
     */
    uploadPrompt(event): void {
        // console.log(event.target.files[0]);
        this.fileToUpload = event.target.files[0];
        this._autoAttendantService.uploadAutoAttendantPrompt(this.autoAttendant, this.fileToUpload)
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Prompt uploaded');
            });
    }

    /**
     * Download autoAttendant prompt file
     */
    downloadPrompt(): void {
        this._autoAttendantService.downloadAutoAttendantPrompt()
            .then(() => {
                // Show the success message
                this._commonService.showSnackBar('Prompt downloaded');
            });
    }

    /**
     * Play autoAttendant prompt file
     */
    playPrompt(): void {
        this._autoAttendantService.playAutoAttendantPrompt()
            .then((result) => {
                this.playAudio(result);
            });
    }

    playAudio(arrayData): void {
        this.stopAudio();
        var soundBuffer;
        this.audioSource = this._audioContext.createBufferSource();
        let destination = this._audioContext.destination;
        let as = this.audioSource;
        this._audioContext.decodeAudioData(arrayData,
            function(buffer) {
                soundBuffer = buffer;
                as.buffer = soundBuffer;
                as.connect(destination);
                as.start(0);
            }
        );
    }

    stopAudio(): void {
        if (this.audioSource) {
            this.audioSource.stop();
        }
        this.audioSource = null;
    }
}
