import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { CompanyReportsService } from 'app/services/companyreports.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'companyreports-list',
    templateUrl: './companyreports.component.html',
    styleUrls: ['./companyreports.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class CompanyReportsComponent implements OnInit, OnDestroy {

    dataSourceSummaryQueue: GenericDataSource | null;
    dataSourceQueue: GenericDataSource | null;
    dataSourceSummaryAgent: GenericDataSource | null;
    dataSourceAgent: GenericDataSource | null;
    displayedColumns = ['label1', 'value1', 'label2', 'value2'];
    displayedQueueColumns = ['queueName', 'received', 'answered', 'transferred', 'unanswered', 'abandoned',
        'averageWaitTime', 'averageTalkTime', 'maxCallers', 'percentAnswered', 'percentUnanswered', 'percentSLA', 'totalTalkTime'];
    displayedQueueOutboundColumns = ['queueName', 'totalOutbound', 'answeredOutbound', 'unansweredOutbound',
        'totalRingOutboundTime', 'totalTalkOutboundTime', 'averageRingOutboundTime', 'averageTalkOutboundTime', 'percentAnsweredOutbound', 'percentUnansweredOutbound']
    displayedAgentColumns = ['userName', 'sessions', 'totalSessionTime', 'averageSessionTime', 'joined', 'totalJoinedTime', 'percentJoined',
        'pauses', 'totalPausedTime', 'percentPaused', 'totalCallTime', 'totalIdleTime'];
    displayedAgentCallColumns = ['userName', 'totalQueueCalls', 'totalQueueCallsInbound', 'totalQueueCallsOutbound', 'totalQueueCallsAnswered', 'totalQueueCallsUnanswered',
        'totalQueueCallRingTime', 'totalQueueCallTalkTime', 'averageHandleTime'];

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    chartPlugins = [pluginDataLabels];

    barChartOptions: ChartOptions = {
        responsive: true,
        // We use these empty structures as placeholders for dynamic theming.
        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };

    chartQueueLabels: Label[] = ['NA'];
    chartAgentLabels: Label[] = ['NA'];

    barChartData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    barChartTimeData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    barChartOutboundData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    barChartOutboundTimeData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    barChartAgentSessionData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    barChartAgentData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    barChartTimeAgentData: ChartDataSets[] = [
        { data: [0], label: 'NA' }
    ];

    /**
     * Constructor
     *
     * @param {CompanyReportsService} _companyReportsService
     */
    constructor(
        private _companyReportsService: CompanyReportsService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSourceSummaryQueue = new GenericDataSource(this._companyReportsService.onQueueReportsSummaryChanged);
        this.dataSourceQueue = new GenericDataSource(this._companyReportsService.onQueueReportsChanged);
        this.dataSourceSummaryAgent = new GenericDataSource(this._companyReportsService.onAgentReportsSummaryChanged);
        this.dataSourceAgent = new GenericDataSource(this._companyReportsService.onAgentReportsChanged);

        this._companyReportsService.onQueueReportsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(report => {
                if (report && JSON.stringify(report) !== '{}') {
                    var answeredData = [];
                    var abandonedData = [];
                    var labels = [];
                    report.forEach(element => {
                        answeredData.push(element.answered);
                        abandonedData.push(element.abandoned);
                        labels.push(element.queueName);
                    });
                    this.barChartData = [
                        { data: answeredData, label: 'Answered' },
                        { data: abandonedData, label: 'Abandoned' }
                    ];
                    this.chartQueueLabels = labels;

                    var avgWaitData = [];
                    var avgTalkData = [];
                    report.forEach(element => {
                        avgWaitData.push(element.averageWaitDuration);
                        avgTalkData.push(element.averageTalkDuration);
                    });
                    this.barChartTimeData = [
                        { data: avgWaitData, label: 'Average Wait' },
                        { data: avgTalkData, label: 'Average Talk' }
                    ];

                    var answeredOutboundData = [];
                    var abandonedOutboundData = [];
                    report.forEach(element => {
                        answeredOutboundData.push(element.answeredOutbound);
                        abandonedOutboundData.push(element.unansweredOutbound);
                    });
                    this.barChartOutboundData = [
                        { data: answeredOutboundData, label: 'Answered' },
                        { data: abandonedOutboundData, label: 'Abandoned' }
                    ];

                    var avgWaitOutboundData = [];
                    var avgTalkOutboundData = [];
                    report.forEach(element => {
                        avgWaitOutboundData.push(element.averageRingOutboundDuration);
                        avgTalkOutboundData.push(element.averageTalkOutboundDuration);
                    });
                    this.barChartOutboundTimeData = [
                        { data: avgWaitOutboundData, label: 'Average Wait' },
                        { data: avgTalkOutboundData, label: 'Average Talk' }
                    ];

                }
            });

        this._companyReportsService.onAgentReportsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(report => {
                if (report && JSON.stringify(report) !== '{}') {
                    var answeredData = [];
                    var abandonedData = [];
                    var totalJoinedData = [];
                    var totalPausedData = [];
                    var totalTalkData = [];
                    var labels = [];
                    report.forEach(element => {
                        answeredData.push(element.totalQueueCallsAnswered);
                        abandonedData.push(element.totalQueueCallsUnanswered);
                        totalJoinedData.push(element.totalJoinedDuration);
                        totalPausedData.push(element.totalPausedDuration);
                        totalTalkData.push(element.totalCallDuration);
                        labels.push(element.userName);
                    });
                    this.barChartAgentData = [
                        { data: answeredData, label: 'Answered' },
                        { data: abandonedData, label: 'Abandoned' }
                    ];

                    this.barChartAgentSessionData = [
                        { data: totalJoinedData, label: 'Joined' },
                        { data: totalPausedData, label: 'Paused' },
                        { data: totalTalkData, label: 'Talk' }
                    ];

                    this.chartAgentLabels = labels;

                    var avgWaitData = [];
                    var avgTalkData = [];
                    report.forEach(element => {
                        avgWaitData.push(element.totalQueueCallRingDuration);
                        avgTalkData.push(element.totalQueueCallTalkDuration);
                    });
                    this.barChartTimeAgentData = [
                        { data: avgWaitData, label: 'Average Wait' },
                        { data: avgTalkData, label: 'Average Talk' }
                    ];

                }
            });

        this.refresh();
    }

    refresh(): void {
        this._companyReportsService.getCompanyReports(this.startDate, this.endDate);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._companyReportsService.cleanup();
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._companyReportsService.getCompanyReports(this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._companyReportsService.getCompanyReports(this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }
}
export class GenericDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {BehaviorSubject<any>} genericBehaviour
     */
    constructor(
        private genericBehaviour: BehaviorSubject<any>
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this.genericBehaviour;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
