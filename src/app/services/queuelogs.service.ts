import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as moment from 'moment';

import { environment } from 'environments/environment';

import { PhoneService } from 'app/services/phone.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class QueueLogsService {

    routeParams: any;
    queueLogs: any[];
    onQueueLogsChanged: BehaviorSubject<any>;
    queueChannelLogs: any[];
    onQueueChannelLogsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _phoneService: PhoneService
    ) {
        // Set the defaults
        this.onQueueLogsChanged = new BehaviorSubject({});
        this.onQueueChannelLogsChanged = new BehaviorSubject({});
        this.cleanup();
    }

    /**
     * Get QueueLogs
     *
     * @returns {Promise<any>}
     */
    getQueueLogs(queueId: number, from: string, to: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/queues/${queueId}/queuelogs`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.queueLogs = response;
                    this.onQueueLogsChanged.next(this.queueLogs);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get QueueChannelLogs
     *
     * @returns {Promise<any>}
     */
    getQueueChannelLogs(queueId: number, from: string, to: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/queues/${queueId}/queuechannellogs`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.queueChannelLogs = response;
                    this.onQueueChannelLogsChanged.next(this.queueChannelLogs);
                    resolve(response);
                }, reject);
        });
    }

    cleanup() {
        this.queueLogs = [];
        this.onQueueLogsChanged.next(null);
        this.queueChannelLogs = [];
        this.onQueueChannelLogsChanged.next(null);
    }
}
