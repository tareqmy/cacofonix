import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
    selector: 'select-dialog',
    templateUrl: './select-dialog.component.html',
    styleUrls: ['./select-dialog.component.scss']
})
export class SelectDialogComponent {

    public message: string;
    public label: string;
    elementId: number;
    elements: any;

    constructor(
        public dialogRef: MatDialogRef<SelectDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        let keys = Object.keys(data);
        let values = Object.values(data);
        this.elements = [];
        for (let i = 0; i < keys.length; i++) {
            this.elements.push({ "key": keys[i], "value": values[i] });
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
