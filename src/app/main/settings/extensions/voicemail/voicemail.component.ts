import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { VoiceMail } from 'app/models/voicemail.model';
import { VoiceMailService } from 'app/services/voicemail.service';
import { CommonService } from 'app/services/common.service';

@Component({
    selector: 'voicemail',
    templateUrl: './voicemail.component.html',
    styleUrls: ['./voicemail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class VoiceMailComponent implements OnInit, OnDestroy {
    voiceMail: VoiceMail;
    voiceMailForm: FormGroup;

    passwordForm: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {VoiceMailService} _voiceMailService
     * @param {CommonService} _commonService
     * @param {FormBuilder} _formBuilder
     * @param {Router} _router
     */
    constructor(
        private _voiceMailService: VoiceMailService,
        private _commonService: CommonService,
        private _formBuilder: FormBuilder,
        private _router: Router
    ) {
        // Set the default
        this.voiceMail = new VoiceMail({});

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update voiceMail on changes
        this._voiceMailService.onVoiceMailChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(voiceMail => {
                this.voiceMail = new VoiceMail(voiceMail);
                this.voiceMailForm = this.editVoiceMailForm();
                this.passwordForm = this.changePasswordForm();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.voiceMailForm.controls; }

    // convenience getter for easy access to form fields
    get p() { return this.passwordForm.controls; }

    /**
     * Create voiceMail form
     *
     * @returns {FormGroup}
     */
    editVoiceMailForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.voiceMail.id],
            extensionId: [{ value: this.voiceMail.extensionId, disabled: true }],
            baseExtension: [{ value: this.voiceMail.baseExtension, disabled: true }],
            enabled: [this.voiceMail.enabled],
            fullName: [this.voiceMail.fullName],
            email: [this.voiceMail.email, [Validators.email]]
        });
    }

    changePasswordForm(): FormGroup {
        return this._formBuilder.group({
            newPassword: ['', [Validators.required, Validators.minLength(5)]]
        });
    }

    /**
     * Save voiceMail
     */
    saveVoiceMail(): void {
        const data = this.voiceMailForm.getRawValue();

        this._voiceMailService.saveVoiceMail(data)
            .then(() => {

                // Trigger the subscription with new data
                this._voiceMailService.onVoiceMailChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('VoiceMail saved');
            });
    }

    updatePassword(): void {
        const password = this.passwordForm.getRawValue();

        this._voiceMailService.changePassword(this.voiceMail, password)
            .then(() => {

                // Show the success message
                this._commonService.showSnackBar('Password updated');
            });
    }

}
