import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { AccountService } from 'app/services/account.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class PhonesService implements Resolve<any> {

    phones: any[];
    onPhonesChanged: BehaviorSubject<any>;

    currentCompanyId: number;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService,
        private _accountService: AccountService
    ) {
        // Set the defaults
        this.onPhonesChanged = new BehaviorSubject({});

        this.currentCompanyId = null;
        this._accountService.onAccountChanged.subscribe(user => {
            if (user) {
                this.currentCompanyId = user.companyId;
            }
        });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPhones()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get Phones
     *
     * @returns {Promise<any>}
     */
    getPhones(): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestUrl = `${environment.settings.backend}/api/account/phones`;
            if (this._authenticationService.isAdmin()) {
                requestUrl = `${environment.settings.backend}/api/companies/${this.currentCompanyId}/phones`;
            }
            if (this._authenticationService.isSuperAdmin()) {
                requestUrl = `${environment.settings.backend}/api/phones`;
            }
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.phones = response;
                    this.onPhonesChanged.next(this.phones);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get Extensions
     *
     * @param phoneId
     * @returns {Promise<any>}
     */
    getExtensions(phoneId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/phones/${phoneId}/extensions`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
