#!/bin/bash

echo "updating nginx.conf"
sed "s#servername#$DNS_NAME#" -i /etc/nginx/conf.d/default.conf

if [ -n "$SERVERURL" ]; then
  echo "setting SERVERURL in environment"
  sed "s#\"backend\".*#\"backend\": \"$SERVERURL\"#" -i /usr/share/nginx/html/environments/environment.json
fi

# service nginx start
nginx -g "daemon off;"
