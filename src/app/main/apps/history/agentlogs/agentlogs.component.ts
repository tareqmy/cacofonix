import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { UsersService } from 'app/services/users.service';
import { AgentLogsService } from 'app/services/agentlogs.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'agentlogs-list',
    templateUrl: './agentlogs.component.html',
    styleUrls: ['./agentlogs.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class AgentLogsComponent implements OnInit, OnDestroy {

    dataSource: FilesDataSource | null;
    displayedColumns = ['agentEvent', 'eventDateTime', 'queueName', 'sipUserName'];

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild('filter', { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    selectedAgent: any;
    agents: any;

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AgentLogsService} _agentLogsService
     * @param {UsersService} _usersService
     */
    constructor(
        private _agentLogsService: AgentLogsService,
        private _usersService: UsersService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._agentLogsService, this.paginator, this.sort);
        this.getAgents();
        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    getAgents(): void {
        this._usersService.getUsers().then((agents) => {
            this.agents = agents;
            if (this.agents && this.agents.length > 0) {
                this.selectedAgent = agents[0].id;
                this._agentLogsService.getAgentLogs(this.selectedAgent, this.startDate, this.endDate);
            }
        });
    }

    isAgentSelected(): boolean {
        return !_.isNil(this.selectedAgent);
    }

    refresh(): void {
        this._agentLogsService.getAgentLogs(this.selectedAgent, this.startDate, this.endDate);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.agents = [];
        this.selectedAgent = null;
        this._agentLogsService.cleanup();
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._agentLogsService.getAgentLogs(this.selectedAgent, this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._agentLogsService.getAgentLogs(this.selectedAgent, this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }
}

export class FilesDataSource extends DataSource<any>
{
    // Private
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {AgentLogsService} _agentLogsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _agentLogsService: AgentLogsService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._agentLogsService.agentLogs;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._agentLogsService.onAgentLogsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(map(() => {

            let data = this._agentLogsService.agentLogs.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
            return data.splice(startIndex, this._matPaginator.pageSize);
        })
        );

    }

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'agentEvent':
                    [propertyA, propertyB] = [a.agentEvent, b.agentEvent];
                    break;
                case 'eventDateTime':
                    [propertyA, propertyB] = [a.eventDateTime, b.eventDateTime];
                    break;
                case 'queueName':
                    [propertyA, propertyB] = [a.queueName, b.queueName];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
