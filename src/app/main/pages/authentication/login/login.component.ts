import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { first } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

import { AlertService } from 'app/services/alert.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    returnUrl: string;
    loading = false;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {Router} _router
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _authenticationService: AuthenticationService,
        private _alertService: AlertService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _route: ActivatedRoute
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: false
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // redirect to home if already logged in
        if (this._authenticationService.currentTokenValue) {
            this._router.navigate(['/apps/dashboard']);
        }

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // get return url from route parameters or default to '/app/dashboard'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/apps/dashboard';

        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            rememberMe: false
        });
    }

    login(loginForm: FormGroup): void {

        if (loginForm.invalid) {
            return;
        }

        this.loading = true;
        this._authenticationService.login(loginForm.getRawValue())
            .pipe(first())
            .subscribe(
                data => {
                    this._router.navigate([this.returnUrl]);
                },
                error => {
                    this._alertService.error(error);
                    this.loading = false;
                });
    }
}
