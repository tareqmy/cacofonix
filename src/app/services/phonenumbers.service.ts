import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { AccountService } from 'app/services/account.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class PhoneNumbersService implements Resolve<any> {

    phoneNumbers: any[];
    onPhoneNumbersChanged: BehaviorSubject<any>;

    currentCompanyId: number;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService,
        private _accountService: AccountService
    ) {
        // Set the defaults
        this.onPhoneNumbersChanged = new BehaviorSubject({});

        this.currentCompanyId = null;
        this._accountService.onAccountChanged.subscribe(user => {
            if (user) {
                this.currentCompanyId = user.companyId;
            }
        });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPhoneNumbers()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get PhoneNumbers
     *
     * @returns {Promise<any>}
     */
    getPhoneNumbers(): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestUrl = `${environment.settings.backend}/api/phonenumbers`;
            if (!this._authenticationService.isSuperAdmin()) {
                requestUrl = `${environment.settings.backend}/api/companies/${this.currentCompanyId}/phonenumbers`;
            }
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.phoneNumbers = response;
                    this.onPhoneNumbersChanged.next(this.phoneNumbers);
                    resolve(response);
                }, reject);
        });
    }
}
