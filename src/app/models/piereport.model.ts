export class PieReport {
    labels: string[];
    data: number[];

    /**
     * Constructor
     *
     * @param pieReport
     */
    constructor(pieReport) {
        this.labels = pieReport.labels;
        this.data = pieReport.data;
    }
}
