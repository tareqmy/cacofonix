export class Provider {
    id: number;
    providerId: string;
    host: string;

    /**
     * Constructor
     *
     * @param provider
     */
    constructor(provider) {
        this.id = provider.id || '';
        this.providerId = provider.providerId || '';
        this.host = provider.host || '';
    }
}
