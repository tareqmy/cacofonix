import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { Phone } from 'app/models/phone.model';
import { PhoneService } from 'app/services/phone.service';
import { CommonService } from 'app/services/common.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'phone',
    templateUrl: './phone.component.html',
    styleUrls: ['./phone.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PhoneComponent implements OnInit, OnDestroy {
    phone: Phone;
    pageType: string;
    isSuperAdmin: boolean;
    phoneForm: FormGroup;

    companies: any;
    extensions: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {PhoneService} _phoneService
     * @param {CommonService} _commonService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _phoneService: PhoneService,
        private _commonService: CommonService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.phone = new Phone({});

        this.companies = [];

        this.extensions = [];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update phone on changes
        this._phoneService.onPhoneChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(phone => {

                if (phone) {
                    this.phone = new Phone(phone);
                    this.pageType = 'edit';
                    this.phoneForm = this.editPhoneForm();
                }
                else {
                    this.pageType = 'new';
                    this.phone = new Phone({});
                    this.phoneForm = this.createPhoneForm();
                }
            });

        if (this.isSuperAdmin) {
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.phoneForm.controls; }

    /**
     * Create phone form
     *
     * @returns {FormGroup}
     */
    createPhoneForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.phone.id],
            phoneName: [this.phone.phoneName, [Validators.required]],
            companyId: [this.phone.companyId, [Validators.required]],
            companyName: [this.phone.companyName]
        });
    }

    /**
     * Create phone form
     *
     * @returns {FormGroup}
     */
    editPhoneForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.phone.id],
            phoneName: [this.phone.phoneName, [Validators.required]],
            companyId: [{ value: this.phone.companyId, disabled: true }],
            companyName: [{ value: this.phone.companyName, disabled: true }]
        });
    }

    /**
     * Save phone
     */
    savePhone(): void {
        const data = this.phoneForm.getRawValue();

        this._phoneService.savePhone(data)
            .then(() => {

                // Trigger the subscription with new data
                this._phoneService.onPhoneChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Phone saved');
            });
    }

    /**
     * Add phone
     */
    addPhone(): void {
        const data = this.phoneForm.getRawValue();

        this._phoneService.addPhone(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._phoneService.onPhoneChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Phone added');

                // Change the location with new one
                this._router.navigate(['/settings/phones/' + this.phone.id]);
            });
    }


    /**
     * Delete phone
     */
    deletePhone(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.phoneForm.getRawValue();

                this._phoneService.deletePhone(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._phoneService.onPhoneChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Phone deleted');

                        // Change the location to phones table
                        this._router.navigate(['/settings/phones']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
