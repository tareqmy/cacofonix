export class ChannelLog {
    id: number;
    uniqueId: string;
    accountCode: string;
    companyId: string;
    extensionId: number;
    baseExtension: string;
    phoneName: string;
    linkedId: string;
    from: string;
    to: string;
    callerName: string;
    callerNumber: string;
    calledNumber: string;
    caller: boolean;
    callInitiator: boolean;
    external: boolean;
    dateTime: string;
    date: string;
    time: string;
    duration: string;
    status: string;
    callType: string;

    /**
     * Constructor
     *
     * @param channelLog
     */
    constructor(channelLog) {
        this.id = channelLog.id || '';
        this.uniqueId = channelLog.uniqueId || '';
        this.accountCode = channelLog.accountCode || '';
        this.companyId = channelLog.companyId;
        this.extensionId = channelLog.extensionId;
        this.baseExtension = channelLog.baseExtension;
        this.phoneName = channelLog.phoneName;
        this.linkedId = channelLog.linkedId;
        this.from = channelLog.from;
        this.to = channelLog.to;
        this.callerName = channelLog.callerName;
        this.callerNumber = channelLog.callerNumber;
        this.calledNumber = channelLog.calledNumber;
        this.caller = channelLog.caller;
        this.callInitiator = channelLog.callInitiator;
        this.external = channelLog.external;
        this.dateTime = channelLog.dateTime;
        this.date = channelLog.date;
        this.time = channelLog.time;
        this.duration = channelLog.duration;
        this.status = channelLog.status;
        this.callType = channelLog.callType;
    }
}
