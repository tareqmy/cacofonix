import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import decode from 'jwt-decode';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { environment } from 'environments/environment';

import { User } from 'app/models/user.model';
import { Login, JWToken } from 'app/models/login.model';
import { MessagesService } from 'app/services/messages.service';
import { AccountService } from 'app/services/account.service';


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private currentTokenSubject: BehaviorSubject<JWToken>;
    public currentToken: Observable<JWToken>;
    public currentEmail: Observable<string>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param {AccountService} _accountService
     * @param {MessagesService} _messagesService
     * @param {Router} _router
     */
    constructor(
        private _httpClient: HttpClient,
        private _accountService: AccountService,
        private _messagesService: MessagesService,
        private _fuseNavigationService: FuseNavigationService,
        private _router: Router
    ) {
        this.currentTokenSubject = new BehaviorSubject<JWToken>(JSON.parse(localStorage.getItem('jwtoken')));
        this.currentToken = this.currentTokenSubject.asObservable();
    }

    public get currentTokenValue(): JWToken {
        return this.currentTokenSubject.value;
    }

    login(login: Login) {
        const requestUrl = `${environment.settings.backend}/api/account/authenticate`;
        return this._httpClient.post<any>(requestUrl, login, httpOptions)
            .pipe(map(jwtoken => {
                // login successful if there's a jwt token in the response
                if (jwtoken && jwtoken.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('jwtoken', JSON.stringify(jwtoken));
                    this.currentTokenSubject.next(jwtoken);
                    this._accountService.getAccount();

                    const tokenPayload = decode(jwtoken.token);
                    if (tokenPayload.auth === "ROLE_USER") {
                        this._fuseNavigationService.setCurrentNavigation('user');
                    } else if (tokenPayload.auth === "ROLE_ADMIN") {
                        this._fuseNavigationService.setCurrentNavigation('admin');
                    } else {
                        this._fuseNavigationService.setCurrentNavigation('super');
                    }
                }

                return jwtoken;
            }));
    }

    isAuthenticated(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/account/authenticate`;
            this._httpClient.get(requestUrl, { responseType: 'text' })
                .subscribe((response: any) => {
                    this.currentEmail = response;
                    resolve(response);
                }, reject);
        });
    }

    isSuperAdmin(): boolean {
        return this.getCurrentUserType() === "ROLE_SYSTEM_ADMIN" || this.getCurrentUserType() === "ROLE_GETAFIX_ADMIN";
    }

    isAdmin(): boolean {
        return this.getCurrentUserType() === "ROLE_ADMIN";
    }

    isUser(): boolean {
        return this.getCurrentUserType() === "ROLE_USER";
    }

    getCurrentUserType(): string {
        const jwtoken = this.currentTokenValue;
        if (jwtoken) {
            const tokenPayload = decode(jwtoken.token);
            return tokenPayload.auth;
        }
        return null;
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('jwtoken');
        this._accountService.loggingOut();
        this.currentTokenSubject.next(null);
        // localStorage.clear(); //causes problem with app info when sign in sign out and refresh
        this._router.navigate(['/pages/auth/login']);
    }

    /** Log a AuthenticationService message with the MessageService */
    private log(message: string) {
        this._messagesService.add(`AuthenticationService: ${message}`);
    }
}
