import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ChannelLogsService {

    routeParams: any;
    channelLogs: any[];
    channelLogsSummary: any[];
    onChannelLogsChanged: BehaviorSubject<any>;
    onChannelLogsSummaryChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onChannelLogsChanged = new BehaviorSubject({});
        this.onChannelLogsSummaryChanged = new BehaviorSubject({});
        this.cleanup();
    }

    /**
     * Get ChannelLogs
     *
     * @returns {Promise<any>}
     */
    getChannelLogsForExtension(extensionId, from, to): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/channellogs`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.channelLogs = response;
                    this.onChannelLogsChanged.next(this.channelLogs);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get ChannelLogsSummary
     *
     * @returns {Promise<any>}
     */
    getChannelLogsSummaryForExtension(extensionId, from, to): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/extensions/${extensionId}/channellogs/summary`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.channelLogsSummary = [];
                    this.channelLogsSummary.push({ "label1": "Extension", "value1": response.extension, "label2": "User Name", "value2": response.userName });
                    this.channelLogsSummary.push({ "label1": "From", "value1": response.from, "label2": "To", "value2": response.to });
                    this.channelLogsSummary.push({ "label1": "Calls", "value1": response.calls, "label2": "Calls Time", "value2": response.callsTime });
                    this.channelLogsSummary.push({ "label1": "Incoming Calls", "value1": response.incomingCalls, "label2": "Incoming Calls Time", "value2": response.incomingCallsTime });
                    this.channelLogsSummary.push({ "label1": "Outgoing Calls", "value1": response.outgoingCalls, "label2": "Outgoing Calls Time", "value2": response.outgoingCallsTime });
                    this.onChannelLogsSummaryChanged.next(this.channelLogsSummary);
                    resolve(response);
                }, reject);
        });
    }

    cleanup() {
        this.channelLogs = [];
        this.channelLogsSummary = [];
        this.onChannelLogsChanged.next(null);
        this.onChannelLogsSummaryChanged.next(null);
    }
}
