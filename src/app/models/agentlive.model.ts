export class AgentLive {
    phoneLineId: number;
    baseExtension: string;
    agentName: string;
    sipUserName: string;
    paused: boolean;
    deviceStatus: string;
    endpointStatus: string;
    statusMap: any;
    channels: any;

    /**
     * Constructor
     *
     * @param agentLive
     */
    constructor(agentLive) {
        this.phoneLineId = agentLive.phoneLineId;
        this.baseExtension = agentLive.baseExtension;
        this.agentName = agentLive.agentName;
        this.sipUserName = agentLive.sipUserName;
        this.paused = agentLive.paused;
        this.deviceStatus = agentLive.deviceStatus;
        this.endpointStatus = agentLive.endpointStatus;
        this.statusMap = agentLive.statusMap;
        this.channels = agentLive.channels;
    }
}
