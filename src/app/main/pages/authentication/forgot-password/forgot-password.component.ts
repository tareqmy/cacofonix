import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

import { UserService } from 'app/services/user.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { CommonService } from 'app/services/common.service';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {AuthenticationService} _authenticationService
     * @param {UserService} _userService
     * @param {CommonService} _commonService
     * @param {Router} _router
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _authenticationService: AuthenticationService,
        private _userService: UserService,
        private _commonService: CommonService,
        private _router: Router
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: false
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // redirect to home if already logged in
        if (this._authenticationService.currentTokenValue) {
            this._router.navigate(['/apps/dashboard']);
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    /**
     * Initiate password reset
     */
    initResetPassword(): void {
        const email = this.forgotPasswordForm.getRawValue().email;

        this._userService.initResetPassword(email)
            .then(() => {

                // Show the success message
                this._commonService.showSnackBar('Password Reset Email sent');

                // Change the location with new one
                this._router.navigate(['/pages/auth/reset-password/' + email]);
            });
    }
}
