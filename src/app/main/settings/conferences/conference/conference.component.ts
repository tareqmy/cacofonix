import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { Conference } from 'app/models/conference.model';
import { ConferenceService } from 'app/services/conference.service';
import { CommonService } from 'app/services/common.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'conference',
    templateUrl: './conference.component.html',
    styleUrls: ['./conference.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ConferenceComponent implements OnInit, OnDestroy {
    conference: Conference;
    pageType: string;
    isSuperAdmin: boolean;
    conferenceForm: FormGroup;

    companies: any;
    extensions: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ConferenceService} _conferenceService
     * @param {CommonService} _commonService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _conferenceService: ConferenceService,
        private _commonService: CommonService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.conference = new Conference({});

        this.companies = [];

        this.extensions = [];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update conference on changes
        this._conferenceService.onConferenceChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(conference => {

                if (conference) {
                    this.conference = new Conference(conference);
                    this.pageType = 'edit';
                    this.conferenceForm = this.editConferenceForm();
                    this._companiesService.getExtensions(this.conference.companyId).then((extensions) => {
                        this.extensions = extensions;
                    });
                }
                else {
                    this.pageType = 'new';
                    this.conference = new Conference({});
                    this.conferenceForm = this.createConferenceForm();
                }
            });

        if (this.isSuperAdmin) {
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.conferenceForm.controls; }

    /**
     * Create conference form
     *
     * @returns {FormGroup}
     */
    createConferenceForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.conference.id],
            name: [this.conference.name, [Validators.required]],
            companyId: [this.conference.companyId, [Validators.required]],
            companyName: [this.conference.companyName],
            leaderCode: [this.conference.leaderCode],
            participantCode: [this.conference.participantCode]
        });
    }

    /**
     * Create conference form
     *
     * @returns {FormGroup}
     */
    editConferenceForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.conference.id],
            name: [this.conference.name, [Validators.required]],
            companyId: [{ value: this.conference.companyId, disabled: true }],
            companyName: [{ value: this.conference.companyName, disabled: true }],
            leaderCode: [{ value: this.conference.leaderCode, disabled: true }],
            participantCode: [{ value: this.conference.participantCode, disabled: true }]
        });
    }

    getExtensions(event): void {
        this._companiesService.getExtensions(this.conference.companyId).then((extensions) => {
            this.extensions = extensions;
            this.f.extensionId.setValue(null);
        });
    }

    /**
     * Save conference
     */
    saveConference(): void {
        const data = this.conferenceForm.getRawValue();

        this._conferenceService.saveConference(data)
            .then(() => {

                // Trigger the subscription with new data
                this._conferenceService.onConferenceChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Conference saved');
            });
    }

    /**
     * Add conference
     */
    addConference(): void {
        const data = this.conferenceForm.getRawValue();

        this._conferenceService.addConference(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._conferenceService.onConferenceChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Conference added');

                // Change the location with new one
                this._router.navigate(['/settings/conferences/' + this.conference.id]);
            });
    }


    /**
     * Delete conference
     */
    deleteConference(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.conferenceForm.getRawValue();

                this._conferenceService.deleteConference(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._conferenceService.onConferenceChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Conference deleted');

                        // Change the location to conferences table
                        this._router.navigate(['/settings/conferences']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
