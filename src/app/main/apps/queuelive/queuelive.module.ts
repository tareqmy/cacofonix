import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule,
    MatDividerModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCircle, faPhoneAlt, faThumbsUp, faPause, faChevronUp, faChevronDown, faLaugh, faFrownOpen, faPlay, faStop, faPhone, faTrash } from '@fortawesome/free-solid-svg-icons';

import { AudioContextModule } from 'angular-audio-context';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { InputDialogModule } from 'app/layout/components/input-dialog/input-dialog.module';

import { QueueLiveComponent } from './queuelive/queuelive.component';
import { QueueLiveService } from 'app/services/queuelive.service';

const routes = [
    {
        path: ':id',
        component: QueueLiveComponent,
        resolve: {
            data: QueueLiveService
        }
    },
];

@NgModule({
    declarations: [
        QueueLiveComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,
        MatDividerModule,

        AudioContextModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        InputDialogModule,

        FontAwesomeModule
    ],
    providers: [
        QueueLiveService
    ],
    entryComponents: [
    ]
})
export class QueueLiveModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faCircle);
        library.addIcons(faPhoneAlt);
        library.addIcons(faThumbsUp);
        library.addIcons(faPause);
        library.addIcons(faChevronUp);
        library.addIcons(faChevronDown);
        library.addIcons(faLaugh);
        library.addIcons(faFrownOpen);
        library.addIcons(faPlay);
        library.addIcons(faStop);
        library.addIcons(faPhone);
        library.addIcons(faTrash);
    }
}
