import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { QueuesService } from 'app/services/queues.service';
import { QueueReportsService } from 'app/services/queuereports.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'queuereports-list',
    templateUrl: './queuereports.component.html',
    styleUrls: ['./queuereports.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class QueueReportsComponent implements OnInit, OnDestroy {

    dataSource: QueueReportsDataSource | null;
    dataSourceInbound: QueueReportsInboundDataSource | null;
    dataSourceOutbound: QueueReportsOutboundDataSource | null;
    displayedColumns = ['label1', 'value1', 'label2', 'value2'];

    selectedQueue: any;
    queues: any;

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {QueueReportsService} _queueReportsService
     * @param {QueuesService} _queuesService
     */
    constructor(
        private _queueReportsService: QueueReportsService,
        private _queuesService: QueuesService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new QueueReportsDataSource(this._queueReportsService);
        this.dataSourceInbound = new QueueReportsInboundDataSource(this._queueReportsService);
        this.dataSourceOutbound = new QueueReportsOutboundDataSource(this._queueReportsService);
        this.getQueues();
    }

    getQueues(): void {
        this._queuesService.getQueues().then((queues) => {
            this.queues = queues;
            if (this.queues && this.queues.length > 0) {
                this.selectedQueue = queues[0].id;
                this._queueReportsService.getQueueReports(this.selectedQueue, this.startDate, this.endDate);
            }
        });
    }

    isQueueSelected(): boolean {
        return !_.isNil(this.selectedQueue);
    }

    refresh(): void {
        this._queueReportsService.getQueueReports(this.selectedQueue, this.startDate, this.endDate);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.queues = [];
        this.selectedQueue = null;
        this._queueReportsService.cleanup();
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._queueReportsService.getQueueReports(this.selectedQueue, this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._queueReportsService.getQueueReports(this.selectedQueue, this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }
}
export class QueueReportsDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueReportsService} _queueReportsService
     */
    constructor(
        private _queueReportsService: QueueReportsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueReportsService.onQueueReportsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class QueueReportsInboundDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueReportsService} _queueReportsService
     */
    constructor(
        private _queueReportsService: QueueReportsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueReportsService.onQueueReportsInboundChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class QueueReportsOutboundDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueReportsService} _queueReportsService
     */
    constructor(
        private _queueReportsService: QueueReportsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueReportsService.onQueueReportsOutboundChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
