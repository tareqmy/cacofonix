import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from 'app/services/authentication.service';
import { MessagesService } from 'app/services/messages.service';
import { CommonService } from 'app/services/common.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {MessagesService} _messagesService
     * @param {CommonService} _commonService
     * @param {Router} _router
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _messagesService: MessagesService,
        private _commonService: CommonService,
        private _router: Router
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            this.log(`${err.status} ${err.message} ${err.error}`);
            let error = err.status + " " + err.statusText;
            if (err.status === 0 || err.status === 401 || err.status === 406 || err.status === 500) {
                // auto logout if 401 response returned from api
                this._authenticationService.logout();
                if (err.status === 0) {
                    error = "Error: Server Unreachable";
                } else if (err.status === 401) {
                    error = "Error: Unauthorized";
                } else if (err.status === 406) {
                    error = "Error: Not Acceptable";
                } else if (err.status === 500) {
                    error = "Error: Server Error";
                }
            } else {
                if (err.error && err.error.apierror) {
                    if (err.error.apierror.subErrors) {
                        error = err.error.apierror.subErrors[0].field + ": " + err.error.apierror.subErrors[0].message;
                    } else {
                        if (err.error.apierror.debugMessage) {
                            error = err.error.apierror.debugMessage;
                        } else if (err.error.apierror.message) {
                            error = err.error.apierror.message;
                        } else {
                            error = err.status + ": " + err.error.apierror.status;
                        }
                    }
                }
            }

            // Show the error message
            this._commonService.showErrorSnackBar(error);

            return throwError(error);
        }))
    }

    /** Log a ErrorInterceptor message with the MessageService */
    private log(message: string) {
        this._messagesService.add(`ErrorInterceptor: ${message}`);
    }
}
