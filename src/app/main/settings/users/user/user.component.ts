import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { User } from 'app/models/user.model';
import { UserService } from 'app/services/user.service';
import { CommonService } from 'app/services/common.service';
import { UtilityService } from 'app/services/utility.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class UserComponent implements OnInit, OnDestroy {
    user: User;
    pageType: string;
    isSuperAdmin: boolean;
    userForm: FormGroup;
    passwordForm: FormGroup;

    userRoles: any;
    timeZones: any;
    companies: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {UserService} _userService
     * @param {CommonService} _commonService
     * @param {UtilityService} _utilityService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _userService: UserService,
        private _commonService: CommonService,
        private _utilityService: UtilityService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.user = new User({});

        this.userRoles = {};

        this.timeZones = {};

        this.companies = [];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update user on changes
        this._userService.onUserChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {

                if (user) {
                    this.user = new User(user);
                    this.pageType = 'edit';
                    this.userForm = this.editUserForm();
                }
                else {
                    this.pageType = 'new';
                    this.user = new User({});
                    this.userForm = this.createUserForm();
                }

                this.passwordForm = this.changePasswordForm();
            });

        if (this.isSuperAdmin) {
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }

        this.timeZones = this._utilityService.zonesValue;

        this.userRoles = this._utilityService.rolesValue;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.userForm.controls; }

    // convenience getter for easy access to form fields
    get p() { return this.passwordForm.controls; }

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    createUserForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.user.id],
            firstName: [this.user.firstName, [Validators.required]],
            lastName: [this.user.lastName],
            email: [this.user.email, [Validators.required, Validators.email]],
            password: [this.user.password, [Validators.required, Validators.minLength(8)]],
            userRole: [this.user.userRole, [Validators.required]],
            enabled: [true],
            locked: [false],
            zoneId: [this.user.zoneId, [Validators.required]],
            companyId: [this.user.companyId, [Validators.required]],
            companyName: [this.user.companyName]
        });
    }

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    editUserForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.user.id],
            firstName: [this.user.firstName, [Validators.required]],
            lastName: [this.user.lastName],
            email: [{ value: this.user.email, disabled: true }, [Validators.required, Validators.email]],
            password: [this.user.password],
            userRole: [{ value: this.user.userRole, disabled: true }, [Validators.required]],
            enabled: [this.user.enabled],
            locked: [this.user.locked],
            zoneId: [this.user.zoneId, [Validators.required]],
            companyId: [{ value: this.user.companyId, disabled: true }],
            companyName: [{ value: this.user.companyName, disabled: true }]
        });
    }

    changePasswordForm(): FormGroup {
        return this._formBuilder.group({
            newPassword: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    /**
     * Save user
     */
    saveUser(): void {
        const data = this.userForm.getRawValue();

        this._userService.saveUser(data)
            .then(() => {

                // Trigger the subscription with new data
                this._userService.onUserChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('User saved');
            });
    }

    /**
     * Add user
     */
    addUser(): void {
        const data = this.userForm.getRawValue();

        this._userService.addUser(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._userService.onUserChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('User added');

                // Change the location with new one
                this._router.navigate(['/settings/users/' + this.user.id]);
            });
    }

    updatePassword(): void {
        const user = this.userForm.getRawValue();
        const password = this.passwordForm.getRawValue();

        this._userService.changePassword(user, password)
            .then(() => {

                // Trigger the subscription with new data
                this._userService.onUserChanged.next(user);

                // Show the success message
                this._commonService.showSnackBar('Password updated');
            });
    }

    /**
     * Delete user
     */
    deleteUser(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.userForm.getRawValue();

                this._userService.deleteUser(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._userService.onUserChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('User deleted');

                        // Change the location to users table
                        this._router.navigate(['/settings/users']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }
}
