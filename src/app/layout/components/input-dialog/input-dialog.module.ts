import { NgModule } from '@angular/core';

import {
    MatButtonModule, MatFormFieldModule, MatInputModule, MatDialogModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';

import { InputDialogComponent } from 'app/layout/components/input-dialog/input-dialog.component';

@NgModule({
    declarations: [
        InputDialogComponent
    ],
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        FormsModule
    ],
    entryComponents: [
        InputDialogComponent
    ],
})
export class InputDialogModule {
}
