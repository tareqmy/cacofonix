import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class UserService implements Resolve<any>
{
    routeParams: any;
    user: any;
    onUserChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onUserChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getUser()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get user
     *
     * @returns {Promise<any>}
     */
    getUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onUserChanged.next(false);
                resolve(false);
            }
            else {
                const requestUrl = `${environment.settings.backend}/api/users/${this.routeParams.id}`;
                this._httpClient.get(requestUrl)
                    .subscribe((response: any) => {
                        this.user = response;
                        this.onUserChanged.next(this.user);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save user
     *
     * @param user
     * @returns {Promise<any>}
     */
    saveUser(user): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/users`;
            this._httpClient.put(requestUrl, user, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add user
     *
     * @param user
     * @returns {Promise<any>}
     */
    addUser(user): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/users`;
            this._httpClient.post(requestUrl, user, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete user
     *
     * @param user
     */
    deleteUser(user): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/users/${user.id}`;
            this._httpClient.delete(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Change user password
     *
     * @param user
     * @param changePassword
     */
    changePassword(user, changePassword): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/users/${user.id}/change_password`;
            this._httpClient.put(requestUrl, changePassword, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Initiate reset user password
     *
     * @param resetPassword
     */
    initResetPassword(email): Promise<any> {
        let httpParams = new HttpParams().append("email", email);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'text/plain' });
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/users/password/reset/init`;
            this._httpClient.post(requestUrl, email, {
                headers: headers,
                params: httpParams
            })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Finish reset user password
     *
     * @param resetPassword
     */
    finishResetPassword(resetPassword): Promise<any> {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'text/plain' });
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/users/password/reset/finish`;
            this._httpClient.post(requestUrl, resetPassword, {
                headers: headers
            })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
