import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { CompanyComponent } from './company/company.component';
import { CompanyService } from 'app/services/company.service';
import { CompaniesComponent } from './companies/companies.component';
import { CompaniesService } from 'app/services/companies.service';

const routes = [
    {
        path: ':id',
        component: CompanyComponent,
        resolve: {
            data: CompanyService
        }
    },
    {
        path: '',
        component: CompaniesComponent,
        resolve: {
            data: CompaniesService
        }
    },
];

@NgModule({
    declarations: [
        CompanyComponent,
        CompaniesComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        CompanyService,
        CompaniesService
    ],
    entryComponents: [
    ]
})
export class CompaniesModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faThumbsUp);
        library.addIcons(faThumbsDown);
    }
}
