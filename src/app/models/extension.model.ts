export class Extension {
    id: number;
    baseExtension: string;
    ringDuration: number;
    companyId: number;
    companyName: string;
    userId: number;
    userName: string;
    voiceMailId: number;
    phoneLineId: number;
    sipUserName: string;

    /**
     * Constructor
     *
     * @param extension
     */
    constructor(extension) {
        this.id = extension.id || '';
        this.baseExtension = extension.baseExtension || '';
        this.ringDuration = extension.ringDuration || 40;
        this.companyId = extension.companyId;
        this.companyName = extension.companyName;
        this.userId = extension.userId;
        this.userName = extension.userName;
        this.voiceMailId = extension.voiceMailId;
        this.phoneLineId = extension.phoneLineId;
        this.sipUserName = extension.sipUserName;
    }
}
