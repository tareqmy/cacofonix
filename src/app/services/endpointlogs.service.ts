import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as moment from 'moment';

import { environment } from 'environments/environment';

import { PhoneService } from 'app/services/phone.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class EndpointLogsService {

    routeParams: any;
    endpointLogs: any[];
    onEndpointLogsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _phoneService: PhoneService
    ) {
        // Set the defaults
        this.onEndpointLogsChanged = new BehaviorSubject({});
        this.cleanup();
    }

    /**
     * Get EndpointLogs
     *
     * @returns {Promise<any>}
     */
    getEndpointLogs(agentId: number, from: string, to: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/users/${agentId}/endpointlogs`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.endpointLogs = response;
                    this.onEndpointLogsChanged.next(this.endpointLogs);
                    resolve(response);
                }, reject);
        });
    }

    cleanup() {
        this.endpointLogs = [];
        this.onEndpointLogsChanged.next(null);
    }
}
