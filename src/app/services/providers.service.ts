import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class ProvidersService implements Resolve<any> {

    providers: any[];
    onProvidersChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onProvidersChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProviders()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get Providers
     *
     * @returns {Promise<any>}
     */
    getProviders(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/providers`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.providers = response;
                    this.onProvidersChanged.next(this.providers);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get Users
     *
     * @param providerId
     * @returns {Promise<any>}
     */
    getUsers(providerId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/providers/${providerId}/users`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get Extensions
     *
     * @param providerId
     * @returns {Promise<any>}
     */
    getExtensions(providerId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/providers/${providerId}/extensions`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
