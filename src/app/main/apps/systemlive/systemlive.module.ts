import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule,
    MatDividerModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCircle, faPhoneAlt, faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { SystemLiveComponent } from './systemlive/systemlive.component';
import { SystemLiveService } from 'app/services/systemlive.service';

const routes = [
    {
        path: '',
        component: SystemLiveComponent,
        resolve: {
            data: SystemLiveService
        }
    },
];

@NgModule({
    declarations: [
        SystemLiveComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,
        MatDividerModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        SystemLiveService
    ],
    entryComponents: [
    ]
})
export class SystemLiveModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faCircle);
        library.addIcons(faPhoneAlt);
        library.addIcons(faChevronUp);
        library.addIcons(faChevronDown);
    }
}
