import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFileDownload, faTrash, faPlay, faStop, faPhone } from '@fortawesome/free-solid-svg-icons';

import { AudioContextModule } from 'angular-audio-context';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { VoiceMailMessagesComponent } from './voicemailmessages/voicemailmessages.component';
import { VoiceMailMessagesService } from 'app/services/voicemailmessages.service';

const routes = [
    {
        path: '',
        component: VoiceMailMessagesComponent
    },
];

@NgModule({
    declarations: [
        VoiceMailMessagesComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        AudioContextModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        VoiceMailMessagesService
    ],
    entryComponents: [
    ]
})
export class VoiceMailMessagesModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faFileDownload);
        library.addIcons(faTrash);
        library.addIcons(faPlay);
        library.addIcons(faStop);
        library.addIcons(faPhone);
    }
}
