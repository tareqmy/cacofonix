export class Conference {
    id: number;
    name: string;
    companyId: number;
    companyName: string;
    leaderCode: string;
    participantCode: string;

    /**
     * Constructor
     *
     * @param conference
     */
    constructor(conference) {
        this.id = conference.id || '';
        this.name = conference.name || '';
        this.companyId = conference.companyId;
        this.companyName = conference.companyName;
        this.leaderCode = conference.leaderCode;
        this.participantCode = conference.participantCode;
    }
}
