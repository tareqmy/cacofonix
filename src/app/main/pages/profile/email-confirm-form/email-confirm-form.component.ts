import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

@Component({
    selector: 'account-email-confirm-form-dialog',
    templateUrl: './email-confirm-form.component.html',
    styleUrls: ['./email-confirm-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class EmailConfirmFormDialogComponent {

    emailConfirmForm: FormGroup;
    dialogTitle: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<EmailConfirmFormDialogComponent>} matDialogRef
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<EmailConfirmFormDialogComponent>,
        private _formBuilder: FormBuilder
    ) {
        this.dialogTitle = 'Confirm Email';
        this.emailConfirmForm = this.getEmailConfirmForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.emailConfirmForm.controls; }

    getEmailConfirmForm(): FormGroup {
        return this._formBuilder.group({
            token: ['', [Validators.required]]
        });
    }
}
