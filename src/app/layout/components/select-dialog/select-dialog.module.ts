import { NgModule } from '@angular/core';

import {
    MatButtonModule, MatFormFieldModule, MatSelectModule, MatDialogModule
} from '@angular/material';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';

import { SelectDialogComponent } from 'app/layout/components/select-dialog/select-dialog.component';

@NgModule({
    declarations: [
        SelectDialogComponent
    ],
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        MatDialogModule,
        FormsModule,
        CommonModule
    ],
    entryComponents: [
        SelectDialogComponent
    ],
})
export class SelectDialogModule {
}
