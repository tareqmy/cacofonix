import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class CompaniesService implements Resolve<any> {

    companies: any[];
    onCompaniesChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onCompaniesChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCompanies()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get Companies
     *
     * @returns {Promise<any>}
     */
    getCompanies(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/companies`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    this.companies = response;
                    this.onCompaniesChanged.next(this.companies);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get Users
     *
     * @param companyId
     * @returns {Promise<any>}
     */
    getUsers(companyId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/companies/${companyId}/users`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get Extensions
     *
     * @param companyId
     * @returns {Promise<any>}
     */
    getExtensions(companyId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/companies/${companyId}/extensions`;
            this._httpClient.get(requestUrl)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
