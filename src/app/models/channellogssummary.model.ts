export class ChannelLogsSummary {
    id: number;
    extension: string;
    userName: string;
    from: string;
    to: string;
    calls: number;
    callsTime: string;
    incomingCalls: number;
    incomingCallsTime: string;
    outgoingCalls: number;
    outgoingCallsTime: string;

    /**
     * Constructor
     *
     * @param channelLogsSummary
     */
    constructor(channelLogsSummary) {
        this.id = channelLogsSummary.id;
        this.extension = channelLogsSummary.extension;
        this.userName = channelLogsSummary.userName;
        this.from = channelLogsSummary.from;
        this.to = channelLogsSummary.to;
        this.calls = channelLogsSummary.calls;
        this.callsTime = channelLogsSummary.callsTime;
        this.incomingCalls = channelLogsSummary.incomingCalls;
        this.incomingCallsTime = channelLogsSummary.incomingCallsTime;
        this.outgoingCalls = channelLogsSummary.outgoingCalls;
        this.outgoingCallsTime = channelLogsSummary.outgoingCallsTime;
    }
}
