import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { QueuesService } from 'app/services/queues.service';
import { QueueLogsService } from 'app/services/queuelogs.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'queuelogs-list',
    templateUrl: './queuelogs.component.html',
    styleUrls: ['./queuelogs.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class QueueLogsComponent implements OnInit, OnDestroy {

    dataSource: QueueLogsDataSource | null;
    dataChannelSource: QueueChannelLogsDataSource | null;
    displayedColumns = ['callid', 'agent', 'event', 'data1', 'data2', 'data3', 'data4', 'data5', 'time'];
    displayedChannelColumns = ['date', 'time', 'callerName', 'calledNumber', 'from', 'to', 'queuedDuration', 'status'];

    selectedQueue: any;
    queues: any;

    startDate: string;
    startDateControl: FormControl;
    endDate: string;
    endDateControl: FormControl;
    maxStartDate: string;
    maxEndDate: string;
    minStartDate: string;
    minEndDate: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {QueueLogsService} _queueLogsService
     * @param {QueuesService} _queuesService
     */
    constructor(
        private _queueLogsService: QueueLogsService,
        private _queuesService: QueuesService
    ) {
        this.startDate = moment().format("YYYY-MM-DD");
        this.startDateControl = new FormControl(moment());
        this.endDate = moment().format("YYYY-MM-DD");
        this.endDateControl = new FormControl(moment());

        this.minStartDate = moment().subtract(3, 'months').format("YYYY-MM-DD");
        this.maxStartDate = this.endDate;
        this.minEndDate = this.startDate;
        this.maxEndDate = moment().format("YYYY-MM-DD");

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new QueueLogsDataSource(this._queueLogsService);
        this.dataChannelSource = new QueueChannelLogsDataSource(this._queueLogsService);
        this.getQueues();
    }

    getQueues(): void {
        this._queuesService.getQueues().then((queues) => {
            this.queues = queues;
            if (this.queues && this.queues.length > 0) {
                this.selectedQueue = queues[0].id;
                this._queueLogsService.getQueueLogs(this.selectedQueue, this.startDate, this.endDate);
                this._queueLogsService.getQueueChannelLogs(this.selectedQueue, this.startDate, this.endDate);
            }
        });
    }

    isQueueSelected(): boolean {
        return !_.isNil(this.selectedQueue);
    }

    refresh(): void {
        this._queueLogsService.getQueueLogs(this.selectedQueue, this.startDate, this.endDate);
        this._queueLogsService.getQueueChannelLogs(this.selectedQueue, this.startDate, this.endDate);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.queues = [];
        this.selectedQueue = null;
        this._queueLogsService.cleanup();
    }

    startDateChanged(event): void {
        this.startDate = moment(event.value).format("YYYY-MM-DD");
        this._queueLogsService.getQueueLogs(this.selectedQueue, this.startDate, this.endDate);
        this._queueLogsService.getQueueChannelLogs(this.selectedQueue, this.startDate, this.endDate);
        this.minEndDate = this.startDate;
    }

    endDateChanged(event): void {
        this.endDate = moment(event.value).format("YYYY-MM-DD");
        this._queueLogsService.getQueueLogs(this.selectedQueue, this.startDate, this.endDate);
        this._queueLogsService.getQueueChannelLogs(this.selectedQueue, this.startDate, this.endDate);
        this.maxStartDate = this.endDate;
    }

    isInProgress(status): boolean {
        return status === "IN_PROGRESS";
    }

    isNotAnswered(status): boolean {
        return status === "CALL_NOT_ANSWERED";
    }

    isAbandoned(status): boolean {
        return status === "CALL_ABANDONED";
    }

    isAnswered(status, outbound): boolean {
        return status === "CALL_ANSWERED" && !outbound;
    }

    isAnsweredOutgoing(status, outbound): boolean {
        return status === "CALL_ANSWERED" && outbound;
    }
}

export class QueueLogsDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueLogsService} _queueLogsService
     */
    constructor(
        private _queueLogsService: QueueLogsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueLogsService.onQueueLogsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

export class QueueChannelLogsDataSource extends DataSource<any>
{

    /**
     * Constructor
     *
     * @param {QueueLogsService} _queueLogsService
     */
    constructor(
        private _queueLogsService: QueueLogsService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._queueLogsService.onQueueChannelLogsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
