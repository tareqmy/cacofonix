import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'environments/environment';

import { AuthenticationService } from 'app/services/authentication.service';
import { CallCenterLive } from 'app/models/callcenterlive.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class CallCenterLiveService implements Resolve<any> {

    eventSource: EventSourcePolyfill;
    queueLives: any = [];
    callCenterLive: CallCenterLive;
    onQueueLivesChanged: BehaviorSubject<any>;
    interval: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onQueueLivesChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getCallCenterLive()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get callCenterLive
     *
     */
    getCallCenterLive() {
        this.createInterval();

        let jwtoken = this._authenticationService.currentTokenValue;
        if (jwtoken && jwtoken.token) {
            this.eventSource = new EventSourcePolyfill(`${environment.settings.backend}/api/sse/callcenter/live`,
                {
                    headers: {
                        Authorization: `Bearer ${jwtoken.token}`
                    }
                });

            // this.eventSource.onopen = (event: any) => console.log('open', event);

            this.eventSource.onmessage = (event: any) => {
                this.callCenterLive = new CallCenterLive(JSON.parse(event.data));
                this.queueLives = this.callCenterLive.queues;
                this.onQueueLivesChanged.next(this.queueLives);
            };

            // this.eventSource.onerror = (event: any) => console.log('error', event);
        }
    }

    createInterval() {
        this.deleteInterval();
        this.interval = setInterval(() => {
            //note: checked duration is increasing
            if (this.queueLives) {
                this.queueLives.forEach(queueLive => {
                    if (queueLive.queuedChannelsCount > 0) {
                        var aWait = parseInt(queueLive.avgWait);
                        var mWait = parseInt(queueLive.maxWait);
                        var qCount = parseInt(queueLive.queuedChannelsCount);
                        queueLive.avgWait = ((aWait * qCount) + qCount) / qCount;
                        queueLive.maxWait = mWait + 1;
                    }
                });
            }
        }, 1000);
    }

    deleteInterval() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    closeEvents() {
        this.queueLives = [];
        this.onQueueLivesChanged.next(null);
        this.eventSource.close();
        this.deleteInterval();
    }

    /**
     * Join callcenter
     *
     * @param phoneLineId
     * @returns {Promise<any>}
     */
    joinCallCenter(phoneLineId): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/join/${phoneLineId}`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Leave callcenter
     *
     * @returns {Promise<any>}
     */
    leaveCallCenter(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/leave`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Pause agent
     *
     * @returns {Promise<any>}
     */
    pauseAgent(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/pause`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Resume agent
     *
     * @returns {Promise<any>}
     */
    resumeAgent(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/resume`;
            this._httpClient.put(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get callcenter queues map
     *
     * @returns {Promise<any>}
     */
    getQueuesMap(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/queues`;
            this._httpClient.get(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get callcenter agents map
     *
     * @returns {Promise<any>}
     */
    getAgentsMap(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/agents`;
            this._httpClient.get(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get callcenter supervisors map
     *
     * @returns {Promise<any>}
     */
    getSupervisorsMap(): Promise<any> {
        return new Promise((resolve, reject) => {
            const requestUrl = `${environment.settings.backend}/api/callcenters/supervisors`;
            this._httpClient.get(requestUrl, httpOptions)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
