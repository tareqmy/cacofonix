import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { RoleGuard } from 'app/_guards/role-guard';

const routes = [
    {
        path: 'providers',
        loadChildren: './providers/providers.module#ProvidersModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN']
        }
    },
    {
        path: 'companies',
        loadChildren: './companies/companies.module#CompaniesModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN']
        }
    },
    {
        path: 'users',
        loadChildren: './users/users.module#UsersModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: 'extensions',
        loadChildren: './extensions/extensions.module#ExtensionsModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN', 'ROLE_USER']
        }
    },
    {
        path: 'phones',
        loadChildren: './phones/phones.module#PhonesModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN', 'ROLE_USER']
        }
    },
    {
        path: 'phonenumbers',
        loadChildren: './phonenumbers/phonenumbers.module#PhoneNumbersModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: 'queues',
        loadChildren: './queues/queues.module#QueuesModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: 'autoattendants',
        loadChildren: './autoattendants/autoattendants.module#AutoAttendantsModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: 'conferences',
        loadChildren: './conferences/conferences.module#ConferencesModule',
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: '**',
        redirectTo: 'pages/errors/error-404'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class SettingsModule {
}
