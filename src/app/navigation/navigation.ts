import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        children: [
            {
                id: 'dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/apps/dashboard'
            },
            {
                id: 'voicemailmessage',
                title: 'Voice Mail Messages',
                translate: 'NAV.VOICEMAILMESSAGES.TITLE',
                type: 'item',
                icon: 'voicemail',
                url: '/apps/voicemailmessages'
            },
            {
                id: 'callcenterlive',
                title: 'Call Center Live',
                translate: 'NAV.CALLCENTERLIVE.TITLE',
                type: 'item',
                icon: 'headset_mic',
                url: '/apps/callcenterlive'
            },
            {
                id: 'systemlive',
                title: 'System Live',
                translate: 'NAV.SYSTEMLIVE.TITLE',
                type: 'item',
                icon: 'event_seat',
                url: '/apps/systemlive'
            },
            {
                id: 'history',
                title: 'History',
                translate: 'NAV.HISTORY.TITLE',
                type: 'collapsable',
                icon: 'history',
                children: [
                    {
                        id: 'channellogs',
                        title: 'Channel Logs',
                        type: 'item',
                        icon: 'call',
                        url: '/apps/history/channellogs'
                    },
                    {
                        id: 'agentlogs',
                        title: 'Agent Logs',
                        type: 'item',
                        icon: 'person',
                        url: '/apps/history/agentlogs'
                    },
                    {
                        id: 'endpointlogs',
                        title: 'Endpoint Logs',
                        type: 'item',
                        icon: 'devices',
                        url: '/apps/history/endpointlogs'
                    },
                    {
                        id: 'queuelogs',
                        title: 'Queue Logs',
                        type: 'item',
                        icon: 'queue',
                        url: '/apps/history/queuelogs'
                    }
                ]
            },
            {
                id: 'report',
                title: 'Report',
                translate: 'NAV.REPORT.TITLE',
                type: 'collapsable',
                icon: 'timeline',
                children: [
                    {
                        id: 'agentreports',
                        title: 'Agent Reports',
                        type: 'item',
                        icon: 'person',
                        url: '/apps/report/agentreports'
                    },
                    {
                        id: 'queuereports',
                        title: 'Queue Reports',
                        type: 'item',
                        icon: 'queue',
                        url: '/apps/report/queuereports'
                    },
                    {
                        id: 'companyreports',
                        title: 'Company Reports',
                        type: 'item',
                        icon: 'business',
                        url: '/apps/report/companyreports'
                    }
                ]
            }
        ]
    },
    {
        id: 'settings',
        title: 'Settings',
        translate: 'NAV.SETTINGS',
        type: 'group',
        children: [
            {
                id: 'providers',
                title: 'Providers',
                translate: 'NAV.PROVIDERS.TITLE',
                type: 'item',
                icon: 'device_hub',
                url: '/settings/providers'
            },
            {
                id: 'companies',
                title: 'Companies',
                translate: 'NAV.COMPANIES.TITLE',
                type: 'item',
                icon: 'business',
                url: '/settings/companies'
            },
            {
                id: 'users',
                title: 'Users',
                translate: 'NAV.USERS.TITLE',
                type: 'item',
                icon: 'person',
                url: '/settings/users'
            },
            {
                id: 'extensions',
                title: 'Extensions',
                translate: 'NAV.EXTENSIONS.TITLE',
                type: 'item',
                icon: 'extension',
                url: '/settings/extensions'
            },
            {
                id: 'phones',
                title: 'Phones',
                translate: 'NAV.PHONES.TITLE',
                type: 'item',
                icon: 'phones',
                url: '/settings/phones'
            },
            {
                id: 'phonenumbers',
                title: 'Phone Numbers',
                translate: 'NAV.PHONENUMBERS.TITLE',
                type: 'item',
                icon: 'sim_card',
                url: '/settings/phonenumbers'
            },
            {
                id: 'queues',
                title: 'Queues',
                translate: 'NAV.QUEUES.TITLE',
                type: 'item',
                icon: 'queue',
                url: '/settings/queues'
            },
            {
                id: 'autoattendants',
                title: 'Auto Attendants',
                translate: 'NAV.AUTO_ATTENDANTS.TITLE',
                type: 'item',
                icon: 'subdirectory_arrow_right',
                url: '/settings/autoattendants'
            },
            {
                id: 'conferences',
                title: 'Conferences',
                translate: 'NAV.CONFERENCES.TITLE',
                type: 'item',
                icon: 'people',
                url: '/settings/conferences'
            }
        ]
    }
];

export const adminNavigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        children: [
            {
                id: 'dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/apps/dashboard'
            },
            {
                id: 'voicemailmessage',
                title: 'Voice Mail Messages',
                translate: 'NAV.VOICEMAILMESSAGES.TITLE',
                type: 'item',
                icon: 'voicemail',
                url: '/apps/voicemailmessages'
            },
            {
                id: 'callcenterlive',
                title: 'Call Center Live',
                translate: 'NAV.CALLCENTERLIVE.TITLE',
                type: 'item',
                icon: 'headset_mic',
                url: '/apps/callcenterlive'
            },
            {
                id: 'history',
                title: 'History',
                translate: 'NAV.HISTORY.TITLE',
                type: 'collapsable',
                icon: 'history',
                children: [
                    {
                        id: 'channellogs',
                        title: 'Channel Logs',
                        type: 'item',
                        icon: 'call',
                        url: '/apps/history/channellogs'
                    },
                    {
                        id: 'agentlogs',
                        title: 'Agent Logs',
                        type: 'item',
                        icon: 'person',
                        url: '/apps/history/agentlogs'
                    },
                    {
                        id: 'endpointlogs',
                        title: 'Endpoint Logs',
                        type: 'item',
                        icon: 'devices',
                        url: '/apps/history/endpointlogs'
                    },
                    {
                        id: 'queuelogs',
                        title: 'Queue Logs',
                        type: 'item',
                        icon: 'queue',
                        url: '/apps/history/queuelogs'
                    }
                ]
            },
            {
                id: 'report',
                title: 'Report',
                translate: 'NAV.REPORT.TITLE',
                type: 'collapsable',
                icon: 'timeline',
                children: [
                    {
                        id: 'agentreports',
                        title: 'Agent Reports',
                        type: 'item',
                        icon: 'person',
                        url: '/apps/report/agentreports'
                    },
                    {
                        id: 'queuereports',
                        title: 'Queue Reports',
                        type: 'item',
                        icon: 'queue',
                        url: '/apps/report/queuereports'
                    },
                    {
                        id: 'companyreports',
                        title: 'Company Reports',
                        type: 'item',
                        icon: 'business',
                        url: '/apps/report/companyreports'
                    }
                ]
            }
        ]
    },
    {
        id: 'settings',
        title: 'Settings',
        translate: 'NAV.SETTINGS',
        type: 'group',
        children: [
            {
                id: 'users',
                title: 'Users',
                translate: 'NAV.USERS.TITLE',
                type: 'item',
                icon: 'person',
                url: '/settings/users'
            },
            {
                id: 'extensions',
                title: 'Extensions',
                translate: 'NAV.EXTENSIONS.TITLE',
                type: 'item',
                icon: 'extension',
                url: '/settings/extensions'
            },
            {
                id: 'phones',
                title: 'Phones',
                translate: 'NAV.PHONES.TITLE',
                type: 'item',
                icon: 'phones',
                url: '/settings/phones'
            },
            {
                id: 'phonenumbers',
                title: 'Phone Numbers',
                translate: 'NAV.PHONENUMBERS.TITLE',
                type: 'item',
                icon: 'sim_card',
                url: '/settings/phonenumbers'
            },
            {
                id: 'queues',
                title: 'Queues',
                translate: 'NAV.QUEUES.TITLE',
                type: 'item',
                icon: 'queue',
                url: '/settings/queues'
            },
            {
                id: 'autoattendants',
                title: 'Auto Attendants',
                translate: 'NAV.AUTO_ATTENDANTS.TITLE',
                type: 'item',
                icon: 'subdirectory_arrow_right',
                url: '/settings/autoattendants'
            },
            {
                id: 'conferences',
                title: 'Conferences',
                translate: 'NAV.CONFERENCES.TITLE',
                type: 'item',
                icon: 'people',
                url: '/settings/conferences'
            }
        ]
    }
];

export const userNavigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        children: [
            {
                id: 'dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/apps/dashboard'
            },
            {
                id: 'voicemailmessage',
                title: 'Voice Mail Messages',
                translate: 'NAV.VOICEMAILMESSAGES.TITLE',
                type: 'item',
                icon: 'voicemail',
                url: '/apps/voicemailmessages'
            },
            {
                id: 'callcenterlive',
                title: 'Call Center Live',
                translate: 'NAV.CALLCENTERLIVE.TITLE',
                type: 'item',
                icon: 'headset_mic',
                url: '/apps/callcenterlive'
            },
            {
                id: 'history',
                title: 'History',
                translate: 'NAV.HISTORY.TITLE',
                type: 'collapsable',
                icon: 'history',
                children: [
                    {
                        id: 'channellogs',
                        title: 'Channel Logs',
                        type: 'item',
                        icon: 'call',
                        url: '/apps/history/channellogs'
                    },
                    {
                        id: 'agentlogs',
                        title: 'Agent Logs',
                        type: 'item',
                        icon: 'person',
                        url: '/apps/history/agentlogs'
                    },
                    {
                        id: 'endpointlogs',
                        title: 'Endpoint Logs',
                        type: 'item',
                        icon: 'devices',
                        url: '/apps/history/endpointlogs'
                    }
                ]
            },
            {
                id: 'report',
                title: 'Report',
                translate: 'NAV.REPORT.TITLE',
                type: 'collapsable',
                icon: 'timeline',
                children: [
                    {
                        id: 'agentreports',
                        title: 'Agent Reports',
                        type: 'item',
                        icon: 'person',
                        url: '/apps/report/agentreports'
                    }
                ]
            }
        ]
    },
    {
        id: 'settings',
        title: 'Settings',
        translate: 'NAV.SETTINGS',
        type: 'group',
        children: [
            {
                id: 'extensions',
                title: 'Extensions',
                translate: 'NAV.EXTENSIONS.TITLE',
                type: 'item',
                icon: 'extension',
                url: '/settings/extensions'
            },
            {
                id: 'phones',
                title: 'Phones',
                translate: 'NAV.PHONES.TITLE',
                type: 'item',
                icon: 'phones',
                url: '/settings/phones'
            }
        ]
    }
];
