export class Login {
    email: string;
    password: string;
    rememberMe: boolean;
}

export class JWToken {
    token: string;
}
