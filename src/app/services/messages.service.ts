import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MessagesService {

    messages: string[] = [];

    add(message: string) {
        this.messages.push(message);
        if (!environment['production']) {
            console.log(message);
        }
    }

    clear() {
        this.messages = [];
    }
}
