import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import { RoleGuard } from 'app/_guards/role-guard';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFileDownload, faTrash, faPlay, faStop, faPhone } from '@fortawesome/free-solid-svg-icons';

import { AudioContextModule } from 'angular-audio-context';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { ChannelLogsComponent } from './channellogs/channellogs.component';
import { ChannelLogsService } from 'app/services/channellogs.service';

import { AgentLogsComponent } from './agentlogs/agentlogs.component';
import { AgentLogsService } from 'app/services/agentlogs.service';

import { EndpointLogsComponent } from './endpointlogs/endpointlogs.component';
import { EndpointLogsService } from 'app/services/endpointlogs.service';

import { QueueLogsComponent } from './queuelogs/queuelogs.component';
import { QueueLogsService } from 'app/services/queuelogs.service';

const routes = [
    {
        path: 'channellogs',
        component: ChannelLogsComponent
    },
    {
        path: 'agentlogs',
        component: AgentLogsComponent
    },
    {
        path: 'endpointlogs',
        component: EndpointLogsComponent
    },
    {
        path: 'queuelogs',
        component: QueueLogsComponent,
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    }
];

@NgModule({
    declarations: [
        ChannelLogsComponent,
        AgentLogsComponent,
        EndpointLogsComponent,
        QueueLogsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        AudioContextModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        ChannelLogsService,
        AgentLogsService,
        EndpointLogsService,
        QueueLogsService
    ],
    entryComponents: [
    ]
})
export class HistoryModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faFileDownload);
        library.addIcons(faTrash);
        library.addIcons(faPlay);
        library.addIcons(faStop);
        library.addIcons(faPhone);
    }
}
