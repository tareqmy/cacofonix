import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import { RoleGuard } from 'app/_guards/role-guard';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule,
    MatDividerModule, MatExpansionModule
} from '@angular/material';

import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFileDownload, faTrash, faPlay, faStop, faPhone } from '@fortawesome/free-solid-svg-icons';

import { AudioContextModule } from 'angular-audio-context';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { AgentReportsComponent } from './agentreports/agentreports.component';
import { AgentReportsService } from 'app/services/agentreports.service';

import { QueueReportsComponent } from './queuereports/queuereports.component';
import { QueueReportsService } from 'app/services/queuereports.service';

import { CompanyReportsComponent } from './companyreports/companyreports.component';
import { CompanyReportsService } from 'app/services/companyreports.service';

const routes = [
    {
        path: 'agentreports',
        component: AgentReportsComponent
    },
    {
        path: 'queuereports',
        component: QueueReportsComponent,
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    },
    {
        path: 'companyreports',
        component: CompanyReportsComponent,
        canActivate: [RoleGuard],
        data: {
            expectedRoles: ['ROLE_SYSTEM_ADMIN', 'ROLE_GETAFIX_ADMIN', 'ROLE_ADMIN']
        }
    }
];

@NgModule({
    declarations: [
        AgentReportsComponent,
        QueueReportsComponent,
        CompanyReportsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,
        MatDividerModule,
        MatExpansionModule,

        AudioContextModule,

        ChartsModule,
        NgxChartsModule,

        FuseSharedModule,
        FuseConfirmDialogModule,

        FontAwesomeModule
    ],
    providers: [
        AgentReportsService,
        QueueReportsService,
        CompanyReportsService
    ],
    entryComponents: [
    ]
})
export class ReportModule {
    constructor(library: FaIconLibrary) {
        // Add an icon to the library for convenient access in other components
        library.addIcons(faFileDownload);
        library.addIcons(faTrash);
        library.addIcons(faPlay);
        library.addIcons(faStop);
        library.addIcons(faPhone);
    }
}
