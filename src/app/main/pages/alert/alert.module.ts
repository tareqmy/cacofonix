import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { AlertComponent } from './alert.component';

const routes = [
    {
        path: 'alert',
        component: AlertComponent
    }
];

@NgModule({
    declarations: [
        AlertComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        FuseSharedModule
    ]
})
export class AlertModule {
}
