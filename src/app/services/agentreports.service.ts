import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as moment from 'moment';

import { environment } from 'environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class AgentReportsService {

    routeParams: any;
    agentReports: any[];
    agentReportsCall: any[];
    onAgentReportsChanged: BehaviorSubject<any>;
    onAgentReportsCallChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onAgentReportsChanged = new BehaviorSubject({});
        this.onAgentReportsCallChanged = new BehaviorSubject({});
        this.cleanup();
    }

    /**
     * Get AgentReport
     *
     * @returns {Promise<any>}
     */
    getAgentReports(agentId: number, from: string, to: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = new HttpParams().set('from', from).set('to', to);
            let requestUrl = `${environment.settings.backend}/api/users/${agentId}/agentreports`;
            this._httpClient.get(requestUrl, { params })
                .subscribe((response: any) => {
                    this.agentReports = [];
                    // this.agentReports.push({ "label1": "Name", "value1": response.userName, "label2": "Zone", "value2": response.zoneId });
                    this.agentReports.push({ "label1": "Session Time", "value1": response.totalSessionTime, "label2": "Joined Time", "value2": response.totalJoinedTime });
                    this.agentReports.push({ "label1": "Paused Time", "value1": response.totalPausedTime, "label2": "UnJoined Time", "value2": response.totalUnJoinedTime });

                    this.agentReports.push({ "label1": "Sessions", "value1": response.sessions, "label2": "Joined", "value2": response.joined });
                    this.agentReports.push({ "label1": "Joined", "value1": response.percentJoined + " %", "label2": "Paused", "value2": response.percentPaused + " %" });
                    this.agentReports.push({ "label1": "Pauses", "value1": response.pauses, "label2": "Pauses Per Session", "value2": response.pausesPerSession });
                    this.agentReports.push({ "label1": "Average Session", "value1": response.averageSessionTime, "label2": "Average Joined", "value2": response.averageJoinedTime });
                    this.agentReports.push({ "label1": "Average Paused", "value1": response.averagePausedTime, "label2": "", "value2": "" });
                    this.onAgentReportsChanged.next(this.agentReports);

                    this.agentReportsCall = [];
                    this.agentReportsCall.push({ "label1": "Calls", "value1": response.totalCalls, "label2": "Queue Calls", "value2": response.totalQueueCalls });
                    this.agentReportsCall.push({ "label1": "Inbound Queue Calls", "value1": response.totalQueueCallsInbound, "label2": "Outbound Queue Calls", "value2": response.totalQueueCallsOutbound });
                    this.agentReportsCall.push({ "label1": "Queue Calls Answered", "value1": response.totalQueueCallsAnswered, "label2": "Queue Calls Unanswered", "value2": response.totalQueueCallsUnanswered });

                    this.agentReportsCall.push({ "label1": "ACD Calls Answered", "value1": response.totalACDCallsAnswered, "label2": "ACD Calls Failed", "value2": response.totalACDCallsFailed });
                    this.agentReportsCall.push({ "label1": "ACD Calls End By Caller", "value1": response.totalACDCallsEndByCaller, "label2": "ACD Calls End By Agent", "value2": response.totalACDCallsEndByAgent });

                    this.agentReportsCall.push({ "label1": "Call Time", "value1": response.totalCallTime, "label2": "Idle Time", "value2": response.totalIdleTime });
                    this.agentReportsCall.push({ "label1": "Queue Call Ring Time", "value1": response.totalQueueCallRingTime, "label2": "Queue Call Talk Time", "value2": response.totalQueueCallTalkTime });
                    this.onAgentReportsCallChanged.next(this.agentReportsCall);

                    resolve(response);
                }, reject);
        });
    }

    cleanup() {
        this.agentReports = [];
        this.agentReportsCall = [];
        this.onAgentReportsChanged.next(null);
        this.onAgentReportsCallChanged.next(null);
    }
}
