export class SystemLive {
    zoneId: string;
    channels: any[];
    endpoints: any[];

    /**
     * Constructor
     *
     * @param systemLive
     */
    constructor(systemLive) {
        this.zoneId = systemLive.zoneId;
        this.channels = systemLive.channels;
        this.endpoints = systemLive.endpoints;
    }
}
