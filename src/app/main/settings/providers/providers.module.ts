import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { ProviderComponent } from './provider/provider.component';
import { ProviderService } from 'app/services/provider.service';
import { ProvidersComponent } from './providers/providers.component';
import { ProvidersService } from 'app/services/providers.service';

const routes = [
    {
        path: ':id',
        component: ProviderComponent,
        resolve: {
            data: ProviderService
        }
    },
    {
        path: '',
        component: ProvidersComponent,
        resolve: {
            data: ProvidersService
        }
    },
];

@NgModule({
    declarations: [
        ProviderComponent,
        ProvidersComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        ProviderService,
        ProvidersService
    ],
    entryComponents: [
    ]
})
export class ProvidersModule { }
