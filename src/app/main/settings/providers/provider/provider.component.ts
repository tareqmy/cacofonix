import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { Provider } from 'app/models/provider.model';
import { ProviderService } from 'app/services/provider.service';
import { CommonService } from 'app/services/common.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
    selector: 'provider',
    templateUrl: './provider.component.html',
    styleUrls: ['./provider.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProviderComponent implements OnInit, OnDestroy {
    provider: Provider;
    pageType: string;
    providerForm: FormGroup;

    timeZones: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProviderService} _providerService
     * @param {CommonService} _commonService
     * @param {UtilityService} _utilityService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _providerService: ProviderService,
        private _commonService: CommonService,
        private _utilityService: UtilityService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.provider = new Provider({});

        this.timeZones = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update provider on changes
        this._providerService.onProviderChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(provider => {

                if (provider) {
                    this.provider = new Provider(provider);
                    this.pageType = 'edit';
                    this.providerForm = this.editProviderForm();
                }
                else {
                    this.pageType = 'new';
                    this.provider = new Provider({});
                    this.providerForm = this.createProviderForm();
                }
            });

        this.timeZones = this._utilityService.zonesValue;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.providerForm.controls; }

    /**
     * Create provider form
     *
     * @returns {FormGroup}
     */
    createProviderForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.provider.id],
            providerId: [this.provider.providerId, [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z][a-zA-Z0-9]+')]],
            host: [this.provider.host, [Validators.required, Validators.pattern('([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}')]]
        });
    }

    /**
     * Create provider form
     *
     * @returns {FormGroup}
     */
    editProviderForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.provider.id],
            providerId: [{ value: this.provider.providerId, disabled: true }, [Validators.required]],
            host: [this.provider.host, [Validators.required, Validators.pattern('([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}')]]
        });
    }

    /**
     * Save provider
     */
    saveProvider(): void {
        const data = this.providerForm.getRawValue();

        this._providerService.saveProvider(data)
            .then(() => {

                // Trigger the subscription with new data
                this._providerService.onProviderChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Provider saved');
            });
    }

    /**
     * Add provider
     */
    addProvider(): void {
        const data = this.providerForm.getRawValue();

        this._providerService.addProvider(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._providerService.onProviderChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Provider added');

                // Change the location with new one
                this._router.navigate(['/settings/providers/' + this.provider.id]);
            });
    }


    /**
     * Delete provider
     */
    deleteProvider(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.providerForm.getRawValue();

                this._providerService.deleteProvider(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._providerService.onProviderChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Provider deleted');

                        // Change the location to providers table
                        this._router.navigate(['/settings/providers']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
