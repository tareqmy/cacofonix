import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { Extension } from 'app/models/extension.model';
import { ExtensionService } from 'app/services/extension.service';
import { CommonService } from 'app/services/common.service';
import { CompaniesService } from 'app/services/companies.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
    selector: 'extension',
    templateUrl: './extension.component.html',
    styleUrls: ['./extension.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ExtensionComponent implements OnInit, OnDestroy {
    extension: Extension;
    pageType: string;
    isSuperAdmin: boolean;
    isUser: boolean;
    extensionForm: FormGroup;

    companies: any;
    users: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ExtensionService} _extensionService
     * @param {CommonService} _commonService
     * @param {CompaniesService} _companiesService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _matDialog
     * @param {Router} _router
     */
    constructor(
        private _extensionService: ExtensionService,
        private _commonService: CommonService,
        private _companiesService: CompaniesService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _matDialog: MatDialog,
        private _router: Router
    ) {
        // Set the default
        this.extension = new Extension({});

        this.companies = [];

        this.users = [];

        this.isSuperAdmin = this._authenticationService.isSuperAdmin();
        this.isUser = this._authenticationService.isUser();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        if (this.isSuperAdmin) {
            this._companiesService.getCompanies().then((companies) => {
                this.companies = companies;
            });
        }

        // Subscribe to update extension on changes
        this._extensionService.onExtensionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(extension => {

                if (extension) {
                    this.extension = new Extension(extension);
                    this.pageType = 'edit';
                    this.extensionForm = this.editExtensionForm();
                    if (!this.isUser) {
                        this._companiesService.getUsers(this.extension.companyId).then((users) => {
                            this.users = users;
                        });
                    }
                }
                else {
                    this.pageType = 'new';
                    this.extension = new Extension({});
                    this.extensionForm = this.createExtensionForm();
                }
            });


    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // convenience getter for easy access to form fields
    get f() { return this.extensionForm.controls; }

    /**
     * Create extension form
     *
     * @returns {FormGroup}
     */
    createExtensionForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.extension.id],
            baseExtension: [this.extension.baseExtension, [Validators.required, Validators.pattern('[0-9]{2}')]],
            ringDuration: [this.extension.ringDuration, [Validators.required, Validators.min(6), Validators.max(99)]],
            companyId: [this.extension.companyId, [Validators.required]],
            companyName: [this.extension.companyName],
            userId: [this.extension.userId],
            userName: [this.extension.userName],
            voiceMailId: [this.extension.voiceMailId]
        });
    }

    /**
     * Create extension form
     *
     * @returns {FormGroup}
     */
    editExtensionForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.extension.id],
            baseExtension: [{ value: this.extension.baseExtension, disabled: true }],
            ringDuration: [this.extension.ringDuration, [Validators.required, Validators.min(6), Validators.max(99)]],
            companyId: [{ value: this.extension.companyId, disabled: true }],
            companyName: [{ value: this.extension.companyName, disabled: true }],
            userId: [this.extension.userId],
            userName: [{ value: this.extension.userName, disabled: this.isUser }],
            voiceMailId: [this.extension.voiceMailId]
        });
    }

    getUsers(event): void {
        this._companiesService.getUsers(this.extension.companyId).then((users) => {
            this.users = users;
            this.f.userId.setValue(null);
        });
    }

    /**
     * Save extension
     */
    saveExtension(): void {
        const data = this.extensionForm.getRawValue();

        this._extensionService.saveExtension(data)
            .then(() => {

                // Trigger the subscription with new data
                this._extensionService.onExtensionChanged.next(data);

                // Show the success message
                this._commonService.showSnackBar('Extension saved');
            });
    }

    /**
     * Add extension
     */
    addExtension(): void {
        const data = this.extensionForm.getRawValue();

        this._extensionService.addExtension(data)
            .then((result) => {

                // Trigger the subscription with new data
                this._extensionService.onExtensionChanged.next(result);

                // Show the success message
                this._commonService.showSnackBar('Extension added');

                // Change the location with new one
                this._router.navigate(['/settings/extensions/' + this.extension.id]);
            });
    }


    /**
     * Delete extension
     */
    deleteExtension(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = this.extensionForm.getRawValue();

                this._extensionService.deleteExtension(data)
                    .then(() => {

                        // Trigger the subscription with new data
                        this._extensionService.onExtensionChanged.next(null);

                        // Show the success message
                        this._commonService.showSnackBar('Extension deleted');

                        // Change the location to extensions table
                        this._router.navigate(['/settings/extensions']);
                    });
            }
            this.confirmDialogRef = null;
        });

    }

}
