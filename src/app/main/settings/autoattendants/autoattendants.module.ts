import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GravatarModule } from 'ngx-gravatar';

import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule,
    MatToolbarModule, MatDialogModule, MatSelectModule, MatTooltipModule,
    MatSortModule, MatPaginatorModule, MatChipsModule, MatTabsModule, MatSnackBarModule
} from '@angular/material';

import { AudioContextModule } from 'angular-audio-context';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { AutoAttendantComponent } from './autoattendant/autoattendant.component';
import { AutoAttendantService } from 'app/services/autoattendant.service';
import { AutoAttendantsComponent } from './autoattendants/autoattendants.component';
import { AutoAttendantsService } from 'app/services/autoattendants.service';

const routes = [
    {
        path: ':id',
        component: AutoAttendantComponent,
        resolve: {
            data: AutoAttendantService
        }
    },
    {
        path: '',
        component: AutoAttendantsComponent,
        resolve: {
            data: AutoAttendantsService
        }
    },
];

@NgModule({
    declarations: [
        AutoAttendantsComponent,
        AutoAttendantComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        GravatarModule,

        MatButtonModule,
        MatCheckboxModule,
        // MatDatepickerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSortModule,
        MatPaginatorModule,
        MatChipsModule,
        MatTabsModule,
        MatSnackBarModule,

        AudioContextModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers: [
        AutoAttendantsService,
        AutoAttendantService
    ],
    entryComponents: [
    ]
})
export class AutoAttendantsModule { }
